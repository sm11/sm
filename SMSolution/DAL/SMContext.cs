using DAL.Models;

namespace DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SMContext : DbContext
    {
        public SMContext() : base("name=SMContext"){ }

        public DbSet<User> Users { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<QAnswer> QAnswers { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<TestGroup> TestGroups { get; set; }
        public DbSet<UserResult> UserResults { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }

        private static SMContext db = new SMContext();

        public static SMContext CreateInstance()
        {
            return db;
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Group>().HasMany(g => g.SubGroups).WithOptional(g => g.Parent).HasForeignKey(g => g.ParentId).WillCascadeOnDelete(false);
            modelBuilder.Entity<User>().HasMany(u => u.CreatedTests).WithRequired(t => t.Interviewer).HasForeignKey(t => t.InterviewerId).WillCascadeOnDelete(false);

            modelBuilder.Entity<Feedback>().HasKey(f => new { f.TestId, f.GroupId });
            //modelBuilder.Entity<Feedback>().HasRequired(f => f.TestGroup).WithOptional(g => g.Feedback).Map(cfg => cfg.MapKey("TestId", "GroupId"));
        }
    }
}
