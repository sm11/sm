﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Test : AbstractModel
    {
        public int TestId { get; set; }
        public string Name { get; set; }
        public int InterviewerId { get; set; }

        public virtual User Interviewer { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<TestGroup> Groups { get; set; }

        public override object[] GetKey()
        {
            return new object[] { this.TestId };
        }

        protected override void copyProperties(AbstractModel model)
        {
            Test test = (Test)model;

            this.Name = test.Name;
            this.InterviewerId = test.InterviewerId;
        }
    }
}
