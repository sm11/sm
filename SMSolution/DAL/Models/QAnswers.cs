﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class QAnswer : AbstractModel
    {
        public int QAnswerId { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }

        public virtual Question Question { get; set; }
        public virtual Answer Answer { get; set; }
        public virtual ICollection<UserResult> Results { get; set; }

        public override object[] GetKey()
        {
            return new object[] { this.QAnswerId };
        }

        protected override void copyProperties(AbstractModel model)
        {
            QAnswer answer = (QAnswer)model;

            this.QuestionId = answer.QuestionId;
            this.AnswerId = answer.AnswerId;
        }
    }
}
