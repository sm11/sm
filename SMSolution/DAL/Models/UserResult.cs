﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class UserResult : AbstractModel
    {
        [Key, Column(Order = 1)]
        public int UserId { get; set; }
        [Key, Column(Order = 2)]
        public int QAnswerId { get; set; }

        public virtual User User { get; set; }
        public virtual QAnswer QAnswer { get; set; }

        public override object[] GetKey()
        {
            return new object[] { this.UserId, this.QAnswerId };
        }

        protected override void copyProperties(AbstractModel model)
        {
            UserResult result = (UserResult)model;

            this.QAnswerId = result.QAnswerId;
            this.UserId = result.UserId;
        }
    }
}
