﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class TestGroup : AbstractModel
    {
        [Key, Column(Order = 1)]
        public int TestId { get; set; }
        [Key, Column(Order = 2)]
        public int GroupId { get; set; }
        public DateTime Expires { get; set; }

        public virtual Group Group { get; set; }
        public virtual Test Test { get; set; }
        public virtual Feedback Feedback { get; set; }

        public override object[] GetKey()
        {
            return new object[] { this.TestId, this.GroupId };
        }

        protected override void copyProperties(AbstractModel model)
        {
            var tGroup = (TestGroup)model;

            this.Expires = tGroup.Expires;
            this.GroupId = tGroup.GroupId;
            this.TestId = tGroup.TestId;

        }
    }
}
