﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Feedback : AbstractModel
    {
        [Key]
        [ForeignKey("TestGroup")]
        public int TestId { get; set; }
        [Key]
        [ForeignKey("TestGroup")]
        public int GroupId { get; set; }
        public string Text { set; get; }
        public virtual TestGroup TestGroup { set; get; }

        public override object[] GetKey()
        {
            return new object[] { TestId, GroupId };
        }

        protected override void copyProperties(AbstractModel model)
        {
            Feedback feedback = (Feedback) model;

            this.TestId = feedback.TestId;
            this.GroupId = feedback.GroupId;
            this.Text = feedback.Text;
        }
    }
}
