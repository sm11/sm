﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public abstract class AbstractModel
    {
        public abstract object[] GetKey(); 
        public void CopyValues(AbstractModel model)
        {
            //if (this.GetType() != model.GetType())
            //{
            //    throw new ArgumentException();
            //}

            this.copyProperties(model);
        }

        protected abstract void copyProperties(AbstractModel model);
    }
}
