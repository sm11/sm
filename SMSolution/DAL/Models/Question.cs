﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public enum QuestionType
    {
        SingleAnswer, 
        MultipleAnswer
    }

    public class Question : AbstractModel
    {
        public int QuestionId { get; set; }
        public string Text { get; set; }
        public int TestId { get; set; }
        public QuestionType Type { get; set; }

        public virtual Test Test { get; set; }
        public virtual ICollection<QAnswer> Answers { get; set; }

        public override object[] GetKey()
        {
            return new object[] { this.QuestionId };
        }

        protected override void copyProperties(AbstractModel model)
        {
            Question question = (Question)model;

            this.Text = question.Text;
            this.TestId = question.TestId;
            this.Type = question.Type;

        }
    }
}
