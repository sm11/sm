﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class User : AbstractModel
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int? GroupId { get; set; }
        public string Role { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }

        public virtual Group Group { get; set; }
        public virtual ICollection<Test> CreatedTests { get; set; }
        public virtual ICollection<UserResult> Results { get; set; } 

        public override object[] GetKey()
        {
            return new object[] { this.UserId };
        }

        protected override void copyProperties(AbstractModel model)
        {
            User user = (User)model;

            this.Login = user.Login;
            this.Password = user.Password;
            this.Email = user.Email;
            this.FullName = user.FullName;
            this.GroupId = user.GroupId;
            this.Password = user.Password;
            this.PhoneNumber = user.PhoneNumber;
            this.Role = user.Role;
        }
    }
}
