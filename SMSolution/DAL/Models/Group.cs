﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Group : AbstractModel
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int? ParentId { get; set; }

        public virtual Group Parent { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<TestGroup> Tests { get; set; }
        public virtual ICollection<Group> SubGroups { get; set; }

        public override object[] GetKey()
        {
            return new object[] { this.GroupId };
        }

        protected override void copyProperties(AbstractModel model)
        {
            Group group = (Group)model;

             this.GroupName = group.GroupName;
            this.ParentId = group.ParentId;
        }
    }
}
