﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Answer : AbstractModel
    {
        public int AnswerId { get; set; }
        public string Text { get; set; }

        public virtual ICollection<QAnswer> Questions { get; set; }

        public override object[] GetKey()
        {
            return new object[] { this.AnswerId };
        }

        protected override void copyProperties(AbstractModel model)
        {
            Answer answer = (Answer)model;
            this.Text = answer.Text;
        }
    }
}
