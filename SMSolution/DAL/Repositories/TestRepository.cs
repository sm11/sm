﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.Repositories
{
    public class TestRepository : Repository<Test>
    {
        public IEnumerable<QAnswer> GetQAnswersByQuestionId(int id)
        {
            return db.QAnswers.Where(q => q.QuestionId == id);
        } 
    }
}
