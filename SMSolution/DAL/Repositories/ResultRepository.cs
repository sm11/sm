﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.Repositories
{
    public class ResultRepository : Repository<UserResult>
    {
        public int GetNumberOfRespondentsForTest(int testId, int groupId)
        {
            //return db.UserResults.Where(r => r.QAnswer.Question.TestId == testId).Select(r => r.UserId).Distinct().Count();
            return db.UserResults.Where(r => r.QAnswer.Question.TestId == testId).Select(r => r.User).Where(u => u.GroupId == groupId).Distinct().Count();
        }

        public int GetNumberOfAnswersForQuestion(int qAnswernId)
        {
            return db.UserResults.Count(ur => ur.QAnswerId == qAnswernId);
        }

       
    }
}
