﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class GroupRepository : Repository<Group>
    {
        public override bool Delete(object[] key)
        {
            Group g = db.Groups.Find(key);
            if (g == null) { return false; }

            List<Group> subGroups = new List<Group>(g.SubGroups.AsEnumerable());

            foreach (Group subGroup in subGroups)
            {
                this.Delete(new object[] { subGroup.GroupId });
            }

            db.Groups.Remove(g);
            return true;
        }
    }
}
