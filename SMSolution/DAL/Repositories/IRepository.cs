﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public interface IRepository<T> where T : AbstractModel
    {
        IEnumerable<T> Find(Func<T, bool> predicate);
        T Get(object[] key);

        void Create(T item);
        void Update(T item);
        bool Delete(object[] key);

        void SaveChanges();
    }
}
