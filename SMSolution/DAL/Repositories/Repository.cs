﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;
using System.Data.Entity;

namespace DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : AbstractModel
    {
        protected SMContext db = new SMContext();

        public void Create(T item)
        {
            db.Set<T>().Add(item);
        }

        public virtual bool Delete(object[] key)
        {
            T item = db.Set<T>().Find(key);

            if (item != null)
            {
                db.Set<T>().Remove(item);
                return true;
            }

            return false;
        }

        public IEnumerable<T> Find(Func<T, bool> predicate)
        {
            return db.Set<T>().Where(predicate);
        }

        public T Get(object[] key)
        {
            return db.Set<T>().Find(key);
        }

        public void SaveChanges()
        {
            db.SaveChanges();    //Exception
        }

        public void Update(T item)
        {
            T dbItem = db.Set<T>().Find(item.GetKey());

            dbItem.CopyValues(item);

            db.Entry(dbItem).State = EntityState.Modified;
        }
    }
}
