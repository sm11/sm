namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateusergrouprelation : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "GroupId", "dbo.Groups");
            DropIndex("dbo.Users", new[] { "GroupId" });
            AlterColumn("dbo.Users", "GroupId", c => c.Int());
            CreateIndex("dbo.Users", "GroupId");
            AddForeignKey("dbo.Users", "GroupId", "dbo.Groups", "GroupId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "GroupId", "dbo.Groups");
            DropIndex("dbo.Users", new[] { "GroupId" });
            AlterColumn("dbo.Users", "GroupId", c => c.Int(nullable: false));
            CreateIndex("dbo.Users", "GroupId");
            AddForeignKey("dbo.Users", "GroupId", "dbo.Groups", "GroupId", cascadeDelete: true);
        }
    }
}
