namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateaftermerge : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Tests", new[] { "Interviewer_UserId" });
            DropColumn("dbo.Tests", "InterviewerId");
            RenameColumn(table: "dbo.Tests", name: "Interviewer_UserId", newName: "InterviewerId");
            AlterColumn("dbo.Tests", "InterviewerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Tests", "InterviewerId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tests", new[] { "InterviewerId" });
            AlterColumn("dbo.Tests", "InterviewerId", c => c.Int());
            RenameColumn(table: "dbo.Tests", name: "InterviewerId", newName: "Interviewer_UserId");
            AddColumn("dbo.Tests", "InterviewerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Tests", "Interviewer_UserId");
        }
    }
}
