namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class feedbackupdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Feedbacks", new[] { "TestId", "GroupId" }, "dbo.TestGroups");
            DropPrimaryKey("dbo.Feedbacks");
            AddPrimaryKey("dbo.Feedbacks", new[] { "TestId", "GroupId" });
            AddForeignKey("dbo.Feedbacks", new[] { "TestId", "GroupId" }, "dbo.TestGroups", new[] { "TestId", "GroupId" });
            DropColumn("dbo.Feedbacks", "FeedbackId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Feedbacks", "FeedbackId", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.Feedbacks", new[] { "TestId", "GroupId" }, "dbo.TestGroups");
            DropPrimaryKey("dbo.Feedbacks");
            AddPrimaryKey("dbo.Feedbacks", "FeedbackId");
            AddForeignKey("dbo.Feedbacks", new[] { "TestId", "GroupId" }, "dbo.TestGroups", new[] { "TestId", "GroupId" }, cascadeDelete: true);
        }
    }
}
