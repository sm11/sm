namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class feedbackmigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Feedbacks",
                c => new
                    {
                        FeedbackId = c.Int(nullable: false, identity: true),
                        TestId = c.Int(nullable: false),
                        GroupId = c.Int(nullable: false),
                        Text = c.String(),
                    })
                .PrimaryKey(t => t.FeedbackId)
                .ForeignKey("dbo.TestGroups", t => new { t.TestId, t.GroupId }, cascadeDelete: true)
                .Index(t => new { t.TestId, t.GroupId });
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Feedbacks", new[] { "TestId", "GroupId" }, "dbo.TestGroups");
            DropIndex("dbo.Feedbacks", new[] { "TestId", "GroupId" });
            DropTable("dbo.Feedbacks");
        }
    }
}
