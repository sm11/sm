﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using API.Models;
using BLL.DTO;
using BLL.Services;
using BLL.Validation;

namespace API.Controllers
{
    public class ResultController : ApiController
    { 
        private IUserResultService service = (IUserResultService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserResultService));

        [HttpPost]
        [Route("api/result/add")]
        public CUDResponseView AddResults(IEnumerable<UserResultViewModel> results)
        {
            IEnumerable<UserResultDTO> resultDtos = results.Select(r => new UserResultDTO()
            {
                UserId = r.UserId,
                QAnswerId = service.GetQAnswerId(r.AnswerId, r.QuestionId)
            });
            try
            {
                service.AddResults(resultDtos);
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpGet]
        [Route("api/result/get")]
        public IEnumerable<QuestionResultViewModel> Get(int testId, int groupId)
        {
            return service.GetTestResults(testId, groupId).Select(r => new QuestionResultViewModel(r));
        }
    }
}