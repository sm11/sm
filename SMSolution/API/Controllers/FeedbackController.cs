﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models;
using DAL.Models;
using BLL.Services;
using BLL.DTO;
using BLL.Validation;

namespace API.Controllers
{
    public class FeedbackController : ApiController
    {
        private IService<FeedbackDTO> service = (IService<FeedbackDTO>)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IService<FeedbackDTO>));

        [HttpGet]
        [Route("api/feedback/get")]
        public FeedbackViewModel Get(int testId, int groupId)
        {
            FeedbackDTO feedback = service.Find(fb => fb.GroupId == groupId && fb.TestId == testId).FirstOrDefault();

            if (feedback != null)
            {
                return new FeedbackViewModel(feedback);
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public CUDResponseView CreateItem(FeedbackViewModel model)
        {

            try
            {
                service.CreateItem(model.ToDTO());
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }
            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpPut]
        public CUDResponseView UpdateItem(FeedbackViewModel model)
        {
            try
            {
                service.Update(model.ToDTO());
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }
            return CUDResponseView.BuildSuccessResponse();
        }

    }
}
