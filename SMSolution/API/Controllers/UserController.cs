﻿using API.Models;
using BLL.DTO;
using BLL.Services;
using BLL.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Account;

namespace API.Controllers
{
    public class UserController : ApiController
    {
        private IUserService service = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));

        [HttpGet]
        [Route("api/user/login")]
        public LoginViewModel Login([FromUri]LoginInputViewModel model)
        {
            UserDTO result;

            result = service.Login(model.Login, model.Password);

            if (result == null) { return null; }

            if (model.RememberMe)
            {
                string token = TokenCollection.GenerateToken(result);

                return new LoginViewModel()
                {
                    Token = token,
                    User = new UserViewModel(result)
                };
            }

            return new LoginViewModel()
            {
                Token = null,
                User = new UserViewModel(result)
            };
        }

        [HttpGet]
        [Route("api/user/validateToken")]
        public UserViewModel ValidateToken(string token)
        {
            UserDTO user = TokenCollection.GetByToken(token);
            return new UserViewModel(user);
        }

        [HttpPost]
        [Route("api/user/register")]
        public RegisterResponseModel Register(UserDTO user)
        {

            try
            {
                service.Register(user);
            }
            catch (ValidationException ex)
            {
                return new RegisterResponseModel(CUDResponseView.BuildErrorResponse(ex.Message), null);
            }

            UserDTO regUser = service.Find(u => u.Login == user.Login).FirstOrDefault();

            return new RegisterResponseModel(CUDResponseView.BuildSuccessResponse(), new UserViewModel(regUser));
        }

        [HttpPut]
        public CUDResponseView UpdateUser(UserViewModel model)
        {
            UserDTO user = model.ToDTO();
            try
            {
                service.Update(user);
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpDelete]
        public CUDResponseView DeleteUser(int id)
        {
            try
            {
                service.Delete(new object[] { id });
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpGet]
        [Route("api/user/get_respondent")]
        public UserViewModel GetRespondent(int id)
        {
            ITestService testService = (ITestService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ITestService));
            ITestGroupService TGService = (ITestGroupService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ITestGroupService));
            UserDTO user = service.Get(new object[] { id });
            /*RespondentViewModel vm = new RespondentViewModel(user);          
            var testIds = TGService.Find(tg => tg.GroupId == user.GroupId).Select(tg => new { TestId = tg.TestId, IsPassed = DateTime.Now > tg.Expires });   
            vm.Tests = testIds.Select(test => new { Test = testService.Get(new object[] { test.TestId }), IsPassed = test.IsPassed }).Select(test => new RespondentTestViewModel(test.Test, test.IsPassed));
            return vm;*/ return new UserViewModel();
        }
    }
}
