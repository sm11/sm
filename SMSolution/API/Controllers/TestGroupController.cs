﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models;
using BLL.Services;
using BLL.Validation;
using BLL.DTO;

namespace API.Controllers
{
    public class TestGroupController : ApiController
    {
        private ITestGroupService service = (ITestGroupService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ITestGroupService));

        [HttpGet]
        public IEnumerable<TestGroupViewModel> GetAll()
        {
            return service.Find(tg => true).Select(tg => tg != null ? new TestGroupViewModel(tg) : null);
        }

        [HttpGet]
        [Route("api/testgroup/get_tests_by_groupId")]
        public IEnumerable<TestGroupViewModel> GetTests(int groupId)
        {
            return service.Find(tg => tg.GroupId == groupId).Select(t => t != null ? new TestGroupViewModel(t) : null);
        }

        [HttpGet]
        [Route("api/testgroup/get_tests_by_testId")]
        public IEnumerable<TestGroupViewModel> GetTestsByTestId(int testId)
        {
            return service.Find(tg => tg.TestId == testId).Select(t => t != null ? new TestGroupViewModel(t) : null);
        }

        [HttpGet]
        [Route("api/testgroup/get_groups_by_testId")]
        public IEnumerable<GroupDTOViewModel> GetGroupsByTestId(int testId)
        {
            return service.Find(tg => tg.TestId == testId).Select(t => {
                if (t == null || t.Group == null)
                {
                    return null;
                }

                return new GroupDTOViewModel(t.Group);
            });
        }

        [HttpPost]
        [Route("api/testgroup/add")]
        public CUDResponseView Add(TestGroupViewModel model)
        {
            try
            {
                service.CreateItem(model.ToDTO());
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }
            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpDelete]
        [Route("api/testgroup/delete")]
        public CUDResponseView Delete(int testId, int groupId)
        {
            try
            {
                service.Delete(new object[] { testId, groupId });
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }
            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpPut]
        [Route("api/testgroup/update")]
        public CUDResponseView Update(TestGroupViewModel model)
        {
            try
            {
                service.Update(model.ToDTO());
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }
            return CUDResponseView.BuildSuccessResponse();
        }


        [HttpDelete]
        [Route("api/testgroup/delete")]
        public CUDResponseView Delete(int id)
        {
            try
            {
                service.Delete(new object[] { id });
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }
            return CUDResponseView.BuildSuccessResponse();
        }
    }
}
