﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models;
using BLL.DTO;
using BLL.Services;
using BLL.Validation;

namespace API.Controllers
{
    public class InterviewerController : ApiController
    {
        private IService<InterviewerDTO> service = (IService<InterviewerDTO>)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IService<InterviewerDTO>));

        [HttpGet]
        [Route("api/interviewer/get")]
        public InterviewerViewModel Get(int id)
        {
            return new InterviewerViewModel(service.Get(new object[] {id}));
        }

    }
}
