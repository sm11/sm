﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models;
using BLL.Services;
using BLL.DTO;
using BLL.Validation;
using WebGrease.Css.Extensions;

namespace API.Controllers
{
    public class TestController : ApiController
    {
        private ITestService service = (ITestService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ITestService));

        [HttpGet]
        [Route("api/test/get")]
        public TestViewModel Get(int id)
        {
            return new TestViewModel(service.Get(new object[] { id }));
        }

        [HttpGet]
        [Route("api/test/getRespondentTests")]
        public IEnumerable<TestViewModel> GetRespondentTests(int userId)
        {
            //IUserService userService = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
            //UserDTO user = userService.Get(new object[] { userId });
            //ITestGroupService TGService = (ITestGroupService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ITestGroupService));
            //var testIds = TGService.Find(tg => tg.GroupId == user.GroupId).Select(tg => new { TestId = tg.TestId, IsPassed = DateTime.Now > tg.Expires });
            //return testIds.Select(test => new { Test = service.Get(new object[] { test.TestId }), IsPassed = test.IsPassed }).Select(test => new RespondentTestViewModel(test.Test, test.IsPassed));
            return service.GetRespondentTests(userId).Select(t => new RespondentTestViewModel(t));
        }

        [HttpGet]
        [Route("api/test/getInterviewerTests")]
        public IEnumerable<TestViewModel> GetInterviewerTests(int userId)
        {
            return service.Find(t => t.InterviewerId == userId).Select(t => new TestViewModel(t));
        }

        [HttpPut]
        [Route("api/test/update")]
        public CUDResponseView Update(TestViewModel model)
        {
            try
            {
                service.Update(model.ToDTO());

                IQuestionService questionService =
                    (IQuestionService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IQuestionService));
                IAnswerService answerService = (IAnswerService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IAnswerService));

                IEnumerable<int> questionsToDelete = questionService.Find(q => q.TestId == model.TestId).Select(q => q.QuestionId);

                questionService.DeleteQuestions(questionsToDelete.Where(q => !model.Questions.Select(quest => quest.QuestionId).Contains(q)));
                    

                foreach (var question in model.Questions)
                {
                    if (question.QuestionId < 0)
                    {
                        questionService.CreateItem(question.ToDTO());
                        question.QuestionId = questionService.Find(q => q.Text == question.Text).Last().QuestionId;
                    }
                    else
                    {
                        questionService.Update(question.ToDTO());
                    }
                    answerService.UpdateItems(question.Answers.Select(a => a.ToDTO()), question.QuestionId); 
                }

            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }
            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpPost]
        [Route("api/test/add")]
        public CUDResponseView Add(TestViewModel model)
        {
            try
            {
                service.CreateItem(model.ToDTO());

                IService<QuestionDTO> questionService =
                    (IService<QuestionDTO>) System.Web.Mvc.DependencyResolver.Current.GetService(typeof (IService<QuestionDTO>));
                IAnswerService answerService = (IAnswerService) System.Web.Mvc.DependencyResolver.Current.GetService(typeof (IAnswerService));
                int testId = service.Find(t => t.Name == model.Name).Last().TestId;
                foreach (var question in model.Questions)
                {
                    question.TestId = testId;
                    questionService.CreateItem(question.ToDTO());
                    int questionId = questionService.Find(q => q.TestId == testId).Last().QuestionId;
                    answerService.CreateItems(question.Answers.Select(a => a.ToDTO()), questionId);
                }

            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }
            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpDelete]
        [Route("api/test/delete")]
        public CUDResponseView Delete(int id)
        {
            try
            {
                service.Delete(new object[] { id });
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }
    }
}
