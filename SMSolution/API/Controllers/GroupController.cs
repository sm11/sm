﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using API.Account;
using API.Models;
using BLL.DTO;
using BLL.Services;
using BLL.Validation;
using System.Web;

namespace API.Controllers
{
    public class GroupController : ApiController
    {
        private IGroupService service =
            (IGroupService) System.Web.Mvc.DependencyResolver.Current.GetService(typeof (IGroupService));

        [HttpPost]
        [Route("api/group/add")]
        public GroupViewModel Add(GroupViewModel model)
        {
            try
            {
                return new GroupViewModel(service.CreateGroup(model.ToDTO(), model.CreatorId));
            }
            catch (ValidationException)
            {
                return null;
            }
        }

        [HttpGet]
        [Route("api/group/get_root_groups")]
        public IEnumerable<GroupViewModel> GetRootGroups()
        {
            IEnumerable<GroupDTO> hierarchy = service.GetRootGroups();
            return hierarchy.Select(g => new GroupViewModel(g));
        }

        [HttpGet]
        [Route("api/group/get_hierarchy")]
        public IEnumerable<GroupViewModel> GetHierarchy(int id)
        {
            IEnumerable<GroupDTO> hierarchy = service.GetHierarchy(id);
            return hierarchy.Select(g => new GroupViewModel(g));
        }

        [HttpGet]
        [Route("api/group/get")]
        public GroupViewModel Get(int id)
        {
            GroupViewModel group = new GroupViewModel(service.Get(new object[] {id}));
            group.Subgroups = service.GetSubgroups(id).Select(g => new GroupViewModel(g));
            return group;
        }

        [HttpGet]
        [Route("api/group/getall")]
        public IEnumerable<GroupViewModel> GetAll()
        {
            return service.GetGroups().Select(gr => new GroupViewModel(gr));
        }

        [HttpGet]
        [Route("api/group/getcreatortests")]
        public IEnumerable<GroupViewModel> GetByCreator(int userId)
        {

            IUserService userService =
                (IUserService) System.Web.Mvc.DependencyResolver.Current.GetService(typeof (IUserService));

            if (userService.Find(u => u.UserId == userId && u.Role == "creator").Count() == 0)
            {
                return null;
            }

            return
                service.GetGroups()
                    .Where(gr => gr.Users.Count(user => user.UserId == userId) > 0)
                    .SelectMany(gr => service.GetHierarchy(gr.GroupId))
                    .Select(gr => new GroupViewModel(gr));
        }

        [HttpPost]
        [Route("api/group/uploadFile")]
        public CUDResponseView UploadFile(int GroupId)
        {
            var httpRequest = HttpContext.Current.Request;
            string filePath = null;
            if (!Request.Content.IsMimeMultipartContent())
                return CUDResponseView.BuildErrorResponse("Invalid file format");
            var postedFile = httpRequest.Files[0];
            filePath = HttpContext.Current.Server.MapPath("~/" + postedFile.FileName);
            postedFile.SaveAs(filePath);

            try
            {
                IUserService service =
                    (IUserService) System.Web.Mvc.DependencyResolver.Current.GetService(typeof (IUserService));
                service.AddUsersFromExcel(filePath, GroupId);
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpPut]
        public CUDResponseView Update(GroupViewModel model)
        {
            try
            {
                service.Update(model.ToDTO());
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpDelete]
        public CUDResponseView Delete(int id)
        {
            try
            {
                IUserService userService = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
                userService.DeleteUsersByGroupId(id);
                service.Delete(new object[] {id});
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }
            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpPost]
        public CUDResponseView AddUsers()
        {
            var httpRequest = HttpContext.Current.Request;
            string filePath = null;
            if (!Request.Content.IsMimeMultipartContent())
                return CUDResponseView.BuildErrorResponse("Invalid file format");
            foreach (string file in httpRequest.Files)
            {
                var postedFile = httpRequest.Files[file];
                filePath = HttpContext.Current.Server.MapPath("~/" + postedFile.FileName);
                postedFile.SaveAs(filePath);
            }
            try
            {
                IUserService service =
                    (IUserService) System.Web.Mvc.DependencyResolver.Current.GetService(typeof (IUserService));
                service.AddUsersFromExcel(filePath, 5);
            }
            catch (ValidationException ex)
            {
                return CUDResponseView.BuildErrorResponse(ex.Message);
            }

            return CUDResponseView.BuildSuccessResponse();
        }

        [HttpGet]
        [Route("api/group/download_file")]
        public HttpResponseMessage DownloadFile()
        {
            var path = System.Web.HttpContext.Current.Server.MapPath("~/example/example.xlsx"); ;
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(path, FileMode.Open);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = Path.GetFileName(path);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentLength = stream.Length;
            return result;
        }
    }





}
