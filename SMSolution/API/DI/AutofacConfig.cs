﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Integration.Mvc;
using System.Web.Mvc;
using BLL.DI;

namespace API.DI
{
    class AutofacConfig
    {
        public static void Config()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterModule(new BLLModule());
            builder.RegisterControllers(typeof(WebApiApplication).Assembly);

            DependencyResolver.SetResolver(new AutofacDependencyResolver(builder.Build()));
        }
    }
}
