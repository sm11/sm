﻿using BLL.Services;
using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace API.Account
{
    static class TokenCollection
    {
        private static string key = Guid.NewGuid().ToString();

        public static string GenerateToken(UserDTO user)
        {
            return TripleEncryptor.Encrypt(user.Login, key);
        }

        public static UserDTO GetByToken(string token)
        {
            string login = TripleEncryptor.Decrypt(token, key);

            IService<UserDTO> loader = DependencyResolver.Current.GetService<IService<UserDTO>>();

            return loader.Find(u => true).First(user => user.Login == login);
        }

    }
}
