﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace API.Models
{
    public class FileGroupViewModel
    {
        public int GroupId { get; set; }
        public HttpPostedFileBase File { get; set; }
    }
}
