﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models
{
    public class GroupDTOViewModel
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int? ParentId { get; set; }

        public GroupDTOViewModel(GroupDTO group)
        {
            this.GroupId = group.GroupId;
            this.GroupName = group.GroupName;
            this.ParentId = group.ParentId;
        }

    }
}
