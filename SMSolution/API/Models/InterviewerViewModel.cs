﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL.DTO;

namespace API.Models
{
    public class InterviewerViewModel : UserViewModel
    {
        public InterviewerViewModel(UserDTO u) : base(u){}

        public InterviewerViewModel(InterviewerDTO interviewer) : base(interviewer)
        {
            CreatedTests = interviewer.CreatedTests.Select(t => new TestViewModel(t));
        }
        public IEnumerable<TestViewModel> CreatedTests { set; get; } 
    }
}