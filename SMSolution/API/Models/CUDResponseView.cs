﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models
{
    public class CUDResponseView
    {
        private CUDResponseView() { }
        private CUDResponseView(bool success, string message = null)
        {
            this.IsSuccess = success;
            this.ErrorMessage = message;
        }

        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }

        public static CUDResponseView BuildSuccessResponse()
        {
            return new CUDResponseView(true);
        }

        public static CUDResponseView BuildErrorResponse(string message)
        {
            return new CUDResponseView(false, message);
        }

    }
}
