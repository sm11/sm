﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace API.Models
{
    public class ExcelViewModel
    {
        public HttpPostedFile ExcelFile { set; get; }
        public int GroupId { set; get; }
    }
}