﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class FeedbackViewModel
    {
        public FeedbackViewModel() { }

        public FeedbackViewModel(FeedbackDTO dto)
        {
            if (dto == null)
            {
                return;
            } 

            this.TestId = dto.TestId;
            this.GroupId = dto.GroupId;
            this.Text = dto.Text;
        }

        public int TestId { get; set; }
        public int GroupId { get; set; }
        public string Text { get; set; }

        public FeedbackDTO ToDTO()
        {
            return new FeedbackDTO() { TestId = this.TestId, GroupId = this.GroupId, Text = this.Text };
        }
    }
}