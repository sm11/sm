﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL.DTO;

namespace API.Models
{
    public class AnswerViewModel
    {
        public int AnswerId { set; get; }
        public string Text { set; get; }

        public AnswerViewModel() { }

        public AnswerViewModel(AnswerDTO answer)
        {
            AnswerId = answer.AnswerId;
            Text = answer.Text;
        }

        public AnswerDTO ToDTO()
        {
            return new AnswerDTO()
            {
                AnswerId = this.AnswerId,
                Text = this.Text
            };
        }
    }
}