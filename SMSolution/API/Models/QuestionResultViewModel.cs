﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL.DTO;

namespace API.Models
{
    public class QuestionResultViewModel
    {
        public int QAnswerId { set; get; }
        public int Percent { set; get; }
        public string QuestionText { set; get; }
        public string AnswerText { set; get; }

        public QuestionResultViewModel(QuestionResultDTO questionResultDto)
        {
            this.QAnswerId = questionResultDto.QAnswerId;
            this.Percent = questionResultDto.Percent;
            this.AnswerText = questionResultDto.QAnswer.Answer.Text;
            this.QuestionText = questionResultDto.QAnswer.Question.Text;
        }
    }
}