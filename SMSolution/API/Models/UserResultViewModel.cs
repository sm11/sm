﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class UserResultViewModel
    {
        public int UserId { set; get; }
        public int AnswerId { set; get; }
        public int QuestionId { set; get; }
    }
}