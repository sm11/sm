﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL.DTO;

namespace API.Models
{
    public class TestViewModel
    {
        public int TestId { get; set; }
        public string Name { get; set; }
        public int InterviewerId { get; set; }
        public IEnumerable<QuestionViewModel> Questions { set; get; }

        public TestViewModel() { }
        public TestViewModel(TestDTO item)
        {
            TestId = item.TestId;
            Name = item.Name;
            InterviewerId = item.InterviewerId;
            Questions = item.Questions.Select(q => new QuestionViewModel(q));
        }

        public TestDTO ToDTO()
        {
            return new TestDTO()
            {
                TestId = this.TestId,
                Name = this.Name,
                InterviewerId = this.InterviewerId

            };
        }
    }
}