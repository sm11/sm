﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL.DTO;
using DAL.Models;

namespace API.Models
{
    public class QuestionViewModel
    {
        public int QuestionId { get; set; }
        public string Text { get; set; }
        public int TestId { get; set; }
        public QuestionType Type { get; set; }

        public IEnumerable<AnswerViewModel> Answers { set; get; } 

        public QuestionViewModel() { }

        public QuestionViewModel(QuestionDTO question)
        {
            QuestionId = question.QuestionId;
            Text = question.Text;
            TestId = question.TestId;
            Type = question.Type;
            Answers = question.Answers.Select(a => new AnswerViewModel(a.Answer));
        }

        public QuestionDTO ToDTO()
        {
            return new QuestionDTO()
            {
                QuestionId = this.QuestionId,
                Text = this.Text,
                TestId = this.TestId,
                Type = this.Type,
            };
        }
    }
}