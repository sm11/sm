﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models
{
    public class RegisterResponseModel
    {
        public CUDResponseView CUDResult { get; set; }
        public UserViewModel User { get; set; }

        public RegisterResponseModel(CUDResponseView model, UserViewModel user)
        {
            this.CUDResult = model;
            this.User = user;
        }

    }
}
