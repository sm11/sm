﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL.DTO;

namespace API.Models
{
    public class GroupViewModel
    {
        public GroupViewModel() { }
        public GroupViewModel(GroupDTO item)
        {
            if (item != null)
            {
                GroupId = item.GroupId;
                GroupName = item.GroupName;
                ParentId = item.ParentId;
                Users = item.Users.Select(u => new UserViewModel(u));
                Parent = new GroupViewModel(item.Parent);
            }

        }

        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int? ParentId { get; set; }
        public int? CreatorId { get; set; }
        public IEnumerable<UserViewModel> Users { set; get; } 
        public GroupViewModel Parent { set; get; }
        public IEnumerable<GroupViewModel> Subgroups { set; get; } 

        public GroupDTO ToDTO()
        {
            return new GroupDTO()
            {
                GroupId = this.GroupId,
                GroupName = this.GroupName,
                ParentId = this.ParentId
            };
        }
    }
}