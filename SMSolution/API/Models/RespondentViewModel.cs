﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL.DTO;

namespace API.Models
{
    public class RespondentViewModel : UserViewModel
    {
        public IEnumerable<RespondentTestViewModel> Tests { set; get; }
        public RespondentViewModel() { }
        public RespondentViewModel(UserDTO user) : base(user) { }

    }
}