﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using BLL.DTO;
using BLL.Services;

namespace API.Models
{
    public class TestGroupViewModel
    {
        public int TestId { get; set; }
        public int GroupId { get; set; }
        public DateTime Expires { get; set; }
        public string GroupName { get; set; }
        public string TestName { get; set; }

        public TestGroupViewModel() { }

        public TestGroupViewModel(TestGroupDTO testGroup)
        {
            TestId = testGroup.TestId;
            GroupId = testGroup.GroupId;
            Expires = testGroup.Expires;
            GroupName = testGroup.Group.GroupName;
            TestName = testGroup.Test.Name;
        }

        public TestGroupDTO ToDTO()
        {
            return new TestGroupDTO()
            {
                TestId = this.TestId,
                GroupId = this.GroupId,
                Expires = this.Expires
            };
        }

    }
}