﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models
{
    public class UserViewModel
    {
        public UserViewModel() { }
        public UserViewModel(UserDTO item)
        {
            Email = item.Email;
            FullName = item.FullName;
            GroupId = item.GroupId;
            Login = item.Login;
            Password = item.Password;
            PhoneNumber = item.PhoneNumber;
            Role = item.Role;
            UserId = item.UserId;
        }

        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int? GroupId { get; set; }
        public string Role { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }

        public UserDTO ToDTO()
        {
            return new UserDTO()
            {
                Email = this.Email,
                FullName = this.FullName,
                GroupId = this.GroupId,
                Login = this.Login,
                Password = this.Password,
                PhoneNumber = this.PhoneNumber,
                Role = this.Role,
                UserId = this.UserId
            };
        }
    }
}
