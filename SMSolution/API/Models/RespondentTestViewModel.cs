﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Models
{
    public class RespondentTestViewModel : TestViewModel
    {
        public bool IsPassed { get; set; }
        public string FeedbackMessage { get; set; }
        public RespondentTestViewModel() { }

        public RespondentTestViewModel(RespondentTestDTO respondentTest) : base(respondentTest)
        {
            this.IsPassed = respondentTest.IsPassed;
            this.FeedbackMessage = respondentTest.FeedbackMessage;
        } 
    }
}
