﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using BLL.DTO;
using DAL.Models;
using DAL.Repositories;

namespace BLL.Validation.Validators
{
    class TestValidator : Validator<TestDTO>
    {
        private IRepository<Test> repo = AutofacResolver.Container.Resolve<IRepository<Test>>();

        protected override bool itemExists(TestDTO item)
        {
            return this.itemExists(new object[] { item.TestId });
        }

        protected override bool itemExists(object[] key)
        {
            return repo.Get(key) != null;
        }

        protected override bool validateProperties(TestDTO item)
        {
            if (String.IsNullOrWhiteSpace(item.Name))
            {
                return false;
            }

            return true;
        }
    }
}
