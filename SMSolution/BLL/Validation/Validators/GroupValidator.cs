﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using DAL.Repositories;
using DAL.Models;
using BLL.DI;
using Autofac;

namespace BLL.Validation.Validators
{
    class GroupValidator : Validator<GroupDTO>
    {
        private IRepository<Group> repo = AutofacResolver.Container.Resolve<IRepository<Group>>();

        protected override bool itemExists(GroupDTO item)
        {
            return this.itemExists(new object[] { item.GroupId });
        }

        protected override bool itemExists(object[] key)
        {
            return repo.Get(key) != null;
        }

        protected override bool validateProperties(GroupDTO item)
        {
            if (String.IsNullOrWhiteSpace(item.GroupName)) {
                return false;
            }

            return true;
        }
    }
}
