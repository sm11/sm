using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using BLL.DTO;
using DAL.Models;
using DAL.Repositories;

namespace BLL.Validation.Validators
{
    class TestGroupValidator : Validator<TestGroupDTO>, IValidator<TestGroupDTO>
    {
        private IRepository<TestGroup> repo = AutofacResolver.Container.Resolve<IRepository<TestGroup>>();

        protected override bool itemExists(TestGroupDTO item)
        {
            return this.itemExists(new object[] { item.TestId, item.GroupId });
        }

        protected override bool itemExists(object[] key)
        {
            return repo.Get(key) != null;
        }

        protected override bool validateProperties(TestGroupDTO item)
        {
            if (item.TestId <= 0 || item.GroupId <= 0)
            {
                return false;
            }

            if (item.Expires == default(DateTime))
            {
                return false;
            }
            return true;

        }
    }
}
