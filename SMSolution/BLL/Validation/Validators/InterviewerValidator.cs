﻿
using System;
using System.Text.RegularExpressions;
using Autofac;
using BLL.DI;
using BLL.DTO;
using DAL.Models;
using DAL.Repositories;

namespace BLL.Validation.Validators
{
    class InterviewerValidator : Validator<InterviewerDTO>
    {
        private IRepository<User> repo = AutofacResolver.Container.Resolve<IRepository<User>>();

        protected override bool itemExists(InterviewerDTO item)
        {
            return repo.Get(new object[] { item.UserId }) != null;
        }

        protected override bool itemExists(object[] key)
        {
            return repo.Get(key) != null;
        }

        protected override bool validateProperties(InterviewerDTO item)
        {
            if (String.IsNullOrWhiteSpace(item.Email) || String.IsNullOrWhiteSpace(item.FullName) || String.IsNullOrWhiteSpace(item.Login) || String.IsNullOrWhiteSpace(item.Password) || String.IsNullOrWhiteSpace(item.Role))
            {
                return false;
            }

            Regex reg = new Regex(@"\w+@\w+\.\w+");
            if (!reg.IsMatch(item.Email))
            {
                return false;
            }

            return true;
        }
    }
}
