﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using BLL.DTO;
using DAL.Models;
using DAL.Repositories;

namespace BLL.Validation.Validators
{
    class ResultValidator : Validator<UserResultDTO>
    {
        private IRepository<UserResult> repo = AutofacResolver.Container.Resolve<IRepository<UserResult>>();

        protected override bool itemExists(UserResultDTO item)
        {
            return this.itemExists(new object[] { item.UserId, item.QAnswerId });
        }

        protected override bool itemExists(object[] key)
        {
            return repo.Get(key) != null;
        }

        protected override bool validateProperties(UserResultDTO item)
        {
            if (item.QAnswerId == 0 || item.UserId == 0)
            {
                return false;
            }

            return true;
        }
    }
}
