﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using BLL.DTO;
using DAL.Models;
using DAL.Repositories;

namespace BLL.Validation.Validators
{
    internal class AnswerValidator : Validator<AnswerDTO>
    {
        private IRepository<Answer> repo = AutofacResolver.Container.Resolve<IRepository<Answer>>();

        protected override bool itemExists(AnswerDTO item)
        {
            return this.itemExists(new object[] {item.AnswerId});
        }

        protected override bool itemExists(object[] key)
        {
            return repo.Get(key) != null;
        }

        protected override bool validateProperties(AnswerDTO item)
        {

            if (String.IsNullOrWhiteSpace(item.Text))
            {
                return false;
            }


            return true;
        }
    }
}
