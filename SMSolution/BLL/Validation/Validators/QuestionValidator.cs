﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using BLL.DTO;
using DAL.Models;
using DAL.Repositories;

namespace BLL.Validation.Validators
{
    internal class QuestionValidator : Validator<QuestionDTO>
    {
        private IRepository<Question> repo = AutofacResolver.Container.Resolve<IRepository<Question>>();

        protected override bool itemExists(QuestionDTO item)
        {
            return this.itemExists(new object[] {item.QuestionId});
        }

        protected override bool itemExists(object[] key)
        {
            return repo.Get(key) != null;
        }

        protected override bool validateProperties(QuestionDTO item)
        {
         
            if (String.IsNullOrWhiteSpace(item.Text))
            {
                return false;
            }
            

            return true;
        }
    }
}
