﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using BLL.DTO;
using DAL.Models;
using DAL.Repositories;

namespace BLL.Validation.Validators
{
    internal class QAnswerValidator : Validator<QAnswerDTO>
    {
        private IRepository<QAnswer> repo = AutofacResolver.Container.Resolve<IRepository<QAnswer>>();

        protected override bool itemExists(QAnswerDTO item)
        {
            return this.itemExists(new object[] {item.QAnswerId});
        }

        protected override bool itemExists(object[] key)
        {
            return repo.Get(key) != null;
        }

        protected override bool validateProperties(QAnswerDTO item)
        {
            IRepository<Question> testRepo = AutofacResolver.Container.Resolve<IRepository<Question>>();

            if (testRepo.Get(new object[] {item.QuestionId}) == null)
            {
                return false;
            }

            IRepository<Answer> groupRepo = AutofacResolver.Container.Resolve<IRepository<Answer>>();

            if (groupRepo.Get(new object[] {item.AnswerId}) == null)
            {
                return false;
            }

            return true;
        }
    }
}
