﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.DI;
using DAL.Repositories;
using DAL.Models;
using Autofac;

namespace BLL.Validation.Validators
{
    class UserValidator : Validator<UserDTO>
    {
        private IRepository<User> repo = AutofacResolver.Container.Resolve<IRepository<User>>();

        protected override bool itemExists(UserDTO item)
        {
            return repo.Get(new object[] { item.UserId }) != null;
        }

        protected override bool itemExists(object[] key)
        {
            return repo.Get(key) != null;
        }

        protected override bool validateProperties(UserDTO item)
        {
            if (String.IsNullOrWhiteSpace(item.Email)) {
                throw new ValidationException("Email can't be empty");
            }

            if ( String.IsNullOrWhiteSpace(item.FullName))
            {
                throw new ValidationException("Full name can't be empty");
            }

            if (String.IsNullOrWhiteSpace(item.Password))
            {
                throw new ValidationException("Password can't be empty");
            }

            if (String.IsNullOrWhiteSpace(item.Role))
            {
                throw new ValidationException("Role can't be empty");
            }
            if (String.IsNullOrWhiteSpace(item.Login))
            {
                throw new ValidationException("Login can't be empty");
            }

            Regex reg = new Regex(@"\w+@\w+\.\w+");
            if (!reg.IsMatch(item.Email)) {
                throw new ValidationException("Inavalid email address.");
            }

            reg = new Regex(@"\+38\s\(0\d\d\)\s\d{3}\s\d{2}\s\d{2}");
            if (!reg.IsMatch(item.PhoneNumber))
            {
                throw new ValidationException("Invalid phone number");
            }
            

            return true;
        }

        public  ValidationStatus ValidateEntityForCreate(UserDTO item)
        {
            if (this.itemExists(item))
            {
                return ValidationStatus.BuildErrorResult(message: "Item already exists");
            }

            if (!this.validateProperties(item))
            {
                return ValidationStatus.BuildErrorResult(message: "Invalid item properties");
            }
            if (repo.Find(user => user.Login == item.Login).FirstOrDefault() != null)
            {
                throw new ValidationException("Login is already taken.");
            }

            return ValidationStatus.BuildSuccessResult();
        }

    }
}
