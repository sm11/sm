﻿using Autofac;
using BLL.DI;
using BLL.DTO;
using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Validation.Validators
{
    class FeedbackValidator : Validator<FeedbackDTO>
    {
        private IRepository<Feedback> repo = AutofacResolver.Container.Resolve<IRepository<Feedback>>();

        protected override bool itemExists(FeedbackDTO item)
        {
            return this.itemExists(new object[] { item.TestId, item.GroupId });
        }

        protected override bool itemExists(object[] key)
        {
            return repo.Get(key) != null;
        }

        protected override bool validateProperties(FeedbackDTO item)
        {
            if (item.TestId <= 0 || item.GroupId <= 0)
            {
                return false;
            }

            if (String.IsNullOrWhiteSpace(item.Text))
            {
                return false;
            }

            IRepository<Test> testRepo = AutofacResolver.Container.Resolve<IRepository<Test>>();

            if (testRepo.Get(new object[] { item.TestId }) == null)
            {
                return false;
            }

            IRepository<Group> groupRepo = AutofacResolver.Container.Resolve<IRepository<Group>>();

            if (groupRepo.Get(new object[] { item.GroupId }) == null)
            {
                return false;
            }

            return true;
        }
    }
}
