﻿using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Validation
{
    abstract class Validator<TDTO> : IValidator<TDTO> where TDTO : class
    {
        protected abstract bool itemExists(object[] key);
        protected abstract bool itemExists(TDTO item);

        protected abstract bool validateProperties(TDTO item);

        public virtual ValidationStatus ValidateEntityForCreate(TDTO item)
        {
            if (this.itemExists(item))
            {
                return ValidationStatus.BuildErrorResult(message: "Item already exists");
            }

            if (!this.validateProperties(item))
            {
                return ValidationStatus.BuildErrorResult(message: "Invalid item properties");
            }

            return ValidationStatus.BuildSuccessResult();
        }

        public ValidationStatus ValidateEntityForDelete(object[] key)
        {
            if (!this.itemExists(key))
            {
                return ValidationStatus.BuildErrorResult(message: "Item doesn't exists.");
            }

            return ValidationStatus.BuildSuccessResult();
        }

        public ValidationStatus ValidateEntityForUpdate(TDTO item)
        {
            if (!this.itemExists(item))
            {
                return ValidationStatus.BuildErrorResult(message: "Item doesn't exists.");
            }

            if (!this.validateProperties(item))
            {
                return ValidationStatus.BuildErrorResult(message: "Invalid item properties");
            }

            return ValidationStatus.BuildSuccessResult();
        }
    }
}
