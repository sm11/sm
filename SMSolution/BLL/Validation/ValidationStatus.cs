﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Validation
{
    public class ValidationStatus
    {
        public bool IsSuccess { get; set; }
        public string ErrorType { get; set; }
        public string ErrorMessage { get; set; }

        public static ValidationStatus BuildSuccessResult()
        {
            return new ValidationStatus() { IsSuccess = true };
        }

        public static ValidationStatus BuildErrorResult(string type = null, string message = null)
        {
            return new ValidationStatus() { IsSuccess = false, ErrorType = type, ErrorMessage = message };
        }
    }
}
