﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Validation
{
    interface IValidator<TDTO>
    {
        ValidationStatus ValidateEntityForCreate(TDTO item);
        ValidationStatus ValidateEntityForUpdate(TDTO item);
        ValidationStatus ValidateEntityForDelete(object[] key);
    }
}
