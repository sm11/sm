﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using DAL.DI;
using BLL.Validation.Validators;
using BLL.Validation;
using BLL.DTO;

namespace BLL.DI
{
    static class AutofacResolver
    {
        private static IContainer container = null;

        public static IContainer Container
        {
            get
            {
                if (container == null)
                {
                    container = configContainer();
                }

                return container;
            }
        }

        private static IContainer configContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterModule(new DALModule());

            builder.RegisterType<UserValidator>().As<IValidator<UserDTO>>();
            builder.RegisterType<GroupValidator>().As<IValidator<GroupDTO>>();
            builder.RegisterType<TestValidator>().As<IValidator<TestDTO>>();
            builder.RegisterType<InterviewerValidator>().As<IValidator<InterviewerDTO>>();
            builder.RegisterType<TestGroupValidator>().As<IValidator<TestGroupDTO>>();
            builder.RegisterType<ResultValidator>().As<IValidator<UserResultDTO>>();
            builder.RegisterType<TestGroupValidator>().As<IValidator<TestGroupDTO>>();
            builder.RegisterType<FeedbackValidator>().As<IValidator<FeedbackDTO>>();
            builder.RegisterType<QuestionValidator>().As<IValidator<QuestionDTO>>();
            builder.RegisterType<AnswerValidator>().As<IValidator<AnswerDTO>>();
            builder.RegisterType<QAnswerValidator>().As<IValidator<QAnswerDTO>>();

            return builder.Build();
        }
    }
}
