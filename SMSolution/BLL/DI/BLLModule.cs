﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.Services.Entity;
using BLL.Services;
using BLL.DTO;
using DAL.Models;

namespace BLL.DI
{
    public class BLLModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //user service
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<UserService>().As <IService<UserDTO>>();

            //groupService
            builder.RegisterType<GroupService>().As<IService<GroupDTO>>();
            builder.RegisterType<GroupService>().As<IGroupService>();

            //test service
            builder.RegisterType<TestService>().As<IService<TestDTO>>();
            builder.RegisterType<TestService>().As<ITestService>();

            // test group service
            builder.RegisterType<TestGroupService>().As<ITestGroupService>();
            builder.RegisterType<TestGroupService>().As<IService<TestGroupDTO>>();

            // user result service
            builder.RegisterType<UserResultService>().As<IUserResultService>();
            builder.RegisterType<UserResultService>().As<IService<UserResultDTO>>();

            //interviewer service
            builder.RegisterType<InterviewerService>().As<IService<InterviewerDTO>>();

            //feedback service
            builder.RegisterType<FeedbackService>().As<IService<FeedbackDTO>>();

            //question service
            builder.RegisterType<QuestionService>().As<IService<QuestionDTO>>();
            builder.RegisterType<QuestionService>().As<IQuestionService>();

            //answer service
            builder.RegisterType<AnswerService>().As<IAnswerService>();
            builder.RegisterType<AnswerService>().As<IService<AnswerDTO>>();

            base.Load(builder);
        }
    }
}
