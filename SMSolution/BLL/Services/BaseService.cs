﻿using BLL.DI;
using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using DAL.Repositories;
using DAL.Models;
using BLL.Validation;

namespace BLL.Services
{
    abstract class BaseService<TDomain, TDTO> : IService<TDTO> where TDomain: AbstractModel where TDTO : class
    {
        protected IRepository<TDomain> repo = AutofacResolver.Container.Resolve<IRepository<TDomain>>();
        protected IValidator<TDTO> validator = AutofacResolver.Container.Resolve<IValidator<TDTO>>();

        protected abstract TDTO convertToDTO(TDomain domain);
        protected abstract TDomain convertToDomain(TDTO dto);

        public BaseService() { }

        public virtual void CreateItem(TDTO item)
        {
            ValidationStatus valRes = validator.ValidateEntityForCreate(item);
            if (!valRes.IsSuccess)
            {
                throw new ValidationException(valRes.ErrorMessage);
            }

            repo.Create(this.convertToDomain(item));
            repo.SaveChanges();
        }

        public virtual void Delete(object[] key)
        {
            ValidationStatus valRes = validator.ValidateEntityForDelete(key);
            if (!valRes.IsSuccess)
            {
                throw new ValidationException(valRes.ErrorMessage);
            }

            repo.Delete(key);
            repo.SaveChanges();
        }

        public IEnumerable<TDTO> Find(Func<TDTO, bool> predicate)
        {
            return repo.Find(domain => predicate(this.convertToDTO(domain))).Select(domain => this.convertToDTO(domain));
        }

        public TDTO Get(object[] key)
        {
            return this.convertToDTO(repo.Get(key));
        }

        public virtual void Update(TDTO item)
        {
            ValidationStatus valRes = validator.ValidateEntityForUpdate(item);
            if (!valRes.IsSuccess)
            {
                throw new ValidationException(valRes.ErrorMessage);
            }

            repo.Update(this.convertToDomain(item));
            repo.SaveChanges();
        }
    }
}
