﻿using System.IO;
using BLL.DTO;

namespace BLL.Services
{
    public interface IUserService : IService<UserDTO>
    {
        UserDTO Login(string login, string password);
        void Register(UserDTO user);
        void AddUsersFromExcel(string fileName, int groupId);
        void DeleteUsersByGroupId(int userId);
    }
}
