﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using DevOne.Security.Cryptography.BCrypt;

namespace BLL.Services.Entity
{
    class MailService
    {
        private const string ourEmail = "psychology.social.manager@gmail.com";

        public void SendEmail(UserDTO user, string password)
        {
            MailMessage mail = new MailMessage(ourEmail, user.Email);
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("psychology.social.manager@gmail.com", "sm123456");
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
           
            mail.Subject = "Social Manager Registration";
            mail.Body = "Welcome, " + user.FullName + "! You were registered in a Social Manager System as a member of group.\n" +
                        "Your login: " + user.Login +
                        "\nYour password: " + password;
            
            client.Send(mail);
            mail.Dispose();
        }
    }
}
