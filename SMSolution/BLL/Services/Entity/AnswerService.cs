﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using BLL.DTO;
using BLL.Validation;
using DAL.Models;
using DAL.Repositories;

namespace BLL.Services.Entity
{
    internal class AnswerService : BaseService<Answer, AnswerDTO>, IAnswerService
    {
        protected override AnswerDTO convertToDTO(Answer answer)
        {
            return new AnswerDTO() {AnswerId = answer.AnswerId, Text = answer.Text};
        }

        protected override Answer convertToDomain(AnswerDTO answer)
        {
            return new Answer() {AnswerId = answer.AnswerId, Text = answer.Text};
        }

        public void CreateItems(IEnumerable<AnswerDTO> answers, int questionId)
        {
            IRepository<QAnswer> qanswerRepo = AutofacResolver.Container.Resolve<IRepository<QAnswer>>();
            IValidator<QAnswerDTO> qanswerValidator = AutofacResolver.Container.Resolve<IValidator<QAnswerDTO>>();
            foreach (var answer in answers)
            {
                Answer old = repo.Find(a => a.Text == answer.Text).FirstOrDefault();
                if (old != default(Answer))
                {
                    QAnswerDTO newQAnswer = new QAnswerDTO()
                    {
                        QuestionId = questionId,
                        AnswerId = old.AnswerId
                    };

                    ValidationStatus valRes = qanswerValidator.ValidateEntityForCreate(newQAnswer);
                    if (!valRes.IsSuccess)
                    {
                        throw new ValidationException(valRes.ErrorMessage);
                    }

                    qanswerRepo.Create((QAnswer)newQAnswer);
                    qanswerRepo.SaveChanges();
                }
                else
                {

                    /*
                    if (repo.Get(new object[] {answer.AnswerId}) != null && oldQAnswers != null
                        && oldQAnswers.Count(qa => qa.AnswerId == answer.AnswerId) <= 1)
                    {
                        valRes = validator.ValidateEntityForUpdate(answer);
                        if (!valRes.IsSuccess)
                        {
                            throw new ValidationException(valRes.ErrorMessage);
                        }
                        repo.Update(convertToDomain(answer));    
                    }
                    else
                    {*/
                    ValidationStatus valRes;
                    valRes = validator.ValidateEntityForCreate(answer);
                    if (!valRes.IsSuccess)
                    {
                       if (valRes.ErrorMessage != "Item already exists")
                                throw new ValidationException(valRes.ErrorMessage);
                    }
                    repo.Create(convertToDomain(answer));
                    repo.SaveChanges();

                    int answId = repo.Find(a => a.Text == answer.Text).Last().AnswerId;
                    QAnswerDTO newQAnswer = new QAnswerDTO()
                    {
                        QuestionId = questionId,
                        AnswerId = answId
                    };

                    valRes = qanswerValidator.ValidateEntityForCreate(newQAnswer);
                    if (!valRes.IsSuccess)
                    {
                        throw new ValidationException(valRes.ErrorMessage);
                    }
                    
                    qanswerRepo.Create((QAnswer)newQAnswer);
                    qanswerRepo.SaveChanges();
                }
            }
        }

        public void UpdateItems(IEnumerable<AnswerDTO> answers, int questionId)
        {
            IRepository<QAnswer> qanswerRepo = AutofacResolver.Container.Resolve<IRepository<QAnswer>>();
            IList<QAnswer> oldQAnswerDtos = qanswerRepo.Find(qa => qa.QuestionId == questionId).ToList();
            foreach (var qa in oldQAnswerDtos)
            {
                qanswerRepo.Delete(new object[] {qa.QAnswerId});
            }
            qanswerRepo.SaveChanges();
            CreateItems(answers, questionId);
        }
    }
}
