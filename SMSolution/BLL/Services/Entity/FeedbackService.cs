﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using DAL.Models;

namespace BLL.Services.Entity
{
    internal class FeedbackService : BaseService<Feedback, FeedbackDTO>
    {
        protected override FeedbackDTO convertToDTO(Feedback feedback)
        {
            return new FeedbackDTO() { TestId = feedback.TestId, GroupId = feedback.GroupId, Text = feedback.Text };
        }

        protected override Feedback convertToDomain(FeedbackDTO feedback)
        {
            return new Feedback() { TestId = feedback.TestId, GroupId = feedback.GroupId, Text = feedback.Text };
        }
    }
}
