﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using DAL.Models;

namespace BLL.Services.Entity
{
    class QuestionService : BaseService<Question, QuestionDTO>, IQuestionService
    {
        protected override QuestionDTO convertToDTO(Question question)
        {
            return new QuestionDTO()
            { TestId = question.TestId, QuestionId = question.QuestionId, Text = question.Text, Type =  question.Type};
        }

        protected override Question convertToDomain(QuestionDTO question)
        {
            return new Question()
            { TestId = question.TestId, QuestionId = question.QuestionId, Text = question.Text, Type = question.Type};
        }

        public void DeleteQuestions(IEnumerable<int> questions)
        {
            foreach (var id in questions)
            {
                repo.Delete(new object[] {id});
            }
            repo.SaveChanges();
        }
    }
}
