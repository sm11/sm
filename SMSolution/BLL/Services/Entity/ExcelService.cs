﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using GemBox.Spreadsheet;

namespace BLL.Services.Entity
{
    class ExcelService
    {
        private ExcelFile excelFile = null;
        private string file = null;

        static ExcelService()
        {
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
        }

        public IEnumerable<UserDTO> ParseExcelUsers(string fileName, int groupId)
        {
            excelFile = ExcelFile.Load(fileName);
            ExcelWorksheet excelWorksheet = excelFile.Worksheets[0];

            this.file = fileName;

            foreach(ExcelRow row in excelWorksheet.Rows)
            {
                if (row.Cells[2].StringValue.Equals("Email"))
                {
                    continue;
                }

                string login = row.Cells[0].StringValue;

                if (login == null)
                { 
                    File.Delete(fileName);
                    yield break;
                }

                string fullName = row.Cells[1].StringValue;
                string email = row.Cells[2].StringValue;
                string phone = row.Cells[3].StringValue;
                string password = row.Cells[4].StringValue;
                string role = row.Cells[5].StringValue;

                if (role != "interviewer")
                    role = "respondent";

                if (phone[0] != '+')
                    phone += '+' + phone;

                UserDTO user = new UserDTO()
                {
                    Login = login,
                    FullName = fullName,
                    Email = email,
                    PhoneNumber = phone,
                    Password = password,
                    Role = role,
                    GroupId = groupId
                };

                yield return user;
            }
        }

        public void CloseProcess()
        {
            if(file!=null)
                File.Delete(file);
        }
    }
}
