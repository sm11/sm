﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using DAL.Models;

namespace BLL.Services.Entity
{
    class InterviewerService : BaseService<User, InterviewerDTO>
    {
        protected override User convertToDomain(InterviewerDTO dto)
        {
            return (User)dto;
        }

        protected override InterviewerDTO convertToDTO(User domain)
        {
            return (InterviewerDTO)domain;
        }
    }
}
