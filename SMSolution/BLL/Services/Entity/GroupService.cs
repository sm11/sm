﻿using DAL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using BLL.DTO;
using DAL.Repositories;

namespace BLL.Services.Entity
{
    class GroupService : BaseService<Group, GroupDTO>, IGroupService
    { 
        protected override Group convertToDomain(GroupDTO dto)
        {
            return (Group)dto;
        }

        protected override GroupDTO convertToDTO(Group domain)
        {
            return (GroupDTO)domain;
        }

        public GroupDTO CreateGroup(GroupDTO group, int? creatorId)
        {
            CreateItem(group);
            Group record = repo.Find(g => g.GroupName == group.GroupName).Last();
            IRepository<User> userRepo = AutofacResolver.Container.Resolve<IRepository<User>>();
            if (creatorId != null)
            {
                User creator = userRepo.Get(new object[] { creatorId });
                if (creator.GroupId == null)
                {
                    creator.GroupId = record.GroupId;
                    userRepo.Update(creator);
                    userRepo.SaveChanges();
                }
            }
            return (GroupDTO)record;
        }

        public IEnumerable<GroupDTO> GetGroups()
        {
            return repo.Find(gr => true).Select(g => convertToDTO(g));
        } 

        public IEnumerable<GroupDTO> GetRootGroups()
        {
            return repo.Find(u => true).Where(gr => gr.ParentId == null).Select(g => convertToDTO(g));
        }

        public IEnumerable<GroupDTO> GetHierarchy(int id)
        {
            IEnumerable<GroupDTO> groups = repo.Find(gr => true).Select(gr => convertToDTO(gr));
            List<GroupDTO> hierarchy = new List<GroupDTO>();
            hierarchy.Add(groups.FirstOrDefault(g => g.GroupId == id));
            IEnumerable<GroupDTO> children = groups.Where(g => g.ParentId == id);
            foreach (GroupDTO g in children)
            {
                hierarchy.AddRange(GetHierarchy(g.GroupId));
            }
            
            return hierarchy;
        }

        public IEnumerable<GroupDTO> GetSubgroups(int groupId)
        {
            return repo.Find(g => true).Where(g => g.ParentId == groupId).Select(g => (GroupDTO) g);
        } 
    }
}
