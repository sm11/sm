﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Validation;
using DAL.Models;
using DAL.Repositories;

namespace BLL.Services.Entity
{
    internal class UserResultService : BaseService<UserResult, UserResultDTO>, IUserResultService
    {
        private new ResultRepository repo = new ResultRepository();

        protected override UserResult convertToDomain(UserResultDTO dto)
        {
            return (UserResult) dto;
        }

        protected override UserResultDTO convertToDTO(UserResult domain)
        {
            return (UserResultDTO) domain;
        }

        public int GetQAnswerId(int answerId, int questionId)
        {
            Repository<QAnswer> repo = new Repository<QAnswer>();
            return repo.Find(qa => qa.AnswerId == answerId && qa.QuestionId == questionId).First(q => true).QAnswerId;
        }

        public void AddResults(IEnumerable<UserResultDTO> userResults)
        {
            foreach (UserResultDTO item in userResults)
            {
                ValidationStatus valRes = validator.ValidateEntityForCreate(item);
                if (!valRes.IsSuccess)
                {
                    throw new ValidationException(valRes.ErrorMessage);
                }

                repo.Create(this.convertToDomain(item));
                repo.SaveChanges();
            }
        }

        public IEnumerable<QuestionResultDTO> GetTestResults(int testId, int groupId)
        {
            IList<QuestionResultDTO> results = new List<QuestionResultDTO>();
            int respondentsNumber = repo.GetNumberOfRespondentsForTest(testId, groupId);
            IEnumerable<UserResult> userResults = repo.Find(ur =>
                        ur.QAnswer.Question.TestId == testId && ur.User.Group.GroupId == groupId);
            foreach (UserResult res in userResults)
            {
                //int answersNumber = repo.GetNumberOfAnswersForQuestion(res.QAnswerId);
                double answersNumber = userResults.Count(ur => ur.QAnswerId == res.QAnswerId);
                if (results.FirstOrDefault(r => r.QAnswerId == res.QAnswerId) == default(QuestionResultDTO))
                {
                    results.Add(new QuestionResultDTO()
                    {
                        QAnswer = (QAnswerDTO)res.QAnswer,
                        QAnswerId = res.QAnswerId,
                        Percent = (int)(answersNumber/respondentsNumber*100)
                    });
                }
            }
            
            return results;
        }

        
    }

}
