﻿using BLL.DTO;
using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using DevOne.Security.Cryptography.BCrypt;
using BLL.Validation;

namespace BLL.Services.Entity
{
    class UserService : BaseService<User, UserDTO>, IUserService
    {
        public UserDTO Login(string login, string password)
        {
            UserDTO user = (UserDTO)repo.Find(u => u.Login == login).FirstOrDefault();

            if (user == null || !BCryptHelper.CheckPassword(password, user.Password))
            {
                return null;
            }

            return user;
        }

        public void Register(UserDTO user)
        {
            ValidationStatus validity = validator.ValidateEntityForCreate(user);

            if (!validity.IsSuccess)
            {
                throw new ValidationException(validity.ErrorMessage);
            }

            user.Password = BCryptHelper.HashPassword(user.Password, BCryptHelper.GenerateSalt());

            repo.Create((User)user);
            repo.SaveChanges();
        }

        public override void CreateItem(UserDTO item)
        {
            this.Register(item);
        }

        public override void Update(UserDTO item)
        {
            item.Password = BCryptHelper.HashPassword(item.Password, BCryptHelper.GenerateSalt());
            base.Update(item);
        }

        public void AddUsersFromExcel(string fileName, int groupId)
        {
            ExcelService excelService = new ExcelService();
            IEnumerable<UserDTO> existing = repo.Find(u => u.GroupId == groupId && u.Role == "respondent").Select(u => convertToDTO(u));

            List<UserDTO> users = excelService.ParseExcelUsers(fileName, groupId).ToList();

            List<string> passwords = users.Select(u => u.Password).ToList();
            foreach (var u in users)
            {
                try
                {
                    if (!existing.Select(e => e.Login).Contains(u.Login))
                        CreateItem(u);
                    else
                        foreach (var exist in existing)
                        {
                            if (exist.Login == u.Login)
                            {
                                u.UserId = exist.UserId;
                                ValidationStatus valStat = validator.ValidateEntityForUpdate(u);
                                if (!valStat.IsSuccess)
                                {
                                    throw new ValidationException(valStat.ErrorMessage);
                                }
                                repo.Update(convertToDomain(u));
                            }
                        }
                    repo.SaveChanges();
                }
                catch (ValidationException e)
                {
                    excelService.CloseProcess();
                    throw new ValidationException(e.Message);
                }
                
            }
            for(int i = 0; i < users.Count(); i++)
            {
                try
                {
                    var emailService = new MailService();
                    emailService.SendEmail(users[i], passwords[i]);
                }
                catch (SmtpException)
                {
                    excelService.CloseProcess();
                    throw new ValidationException("Unable to send an email");
                }
            }
        }

        public void DeleteUsersByGroupId(int groupId)
        {
            IEnumerable<int> idsToDelete = repo.Find(u => u.GroupId == groupId).Select(u => u.UserId);
            foreach (int id in idsToDelete)
            {
                repo.Delete(new object[] { id });
            }
            repo.SaveChanges();
        }

        protected override User convertToDomain(UserDTO dto)
        {
            return (User)dto;
        }

        protected override UserDTO convertToDTO(User domain)
        {
            return (UserDTO)domain;
        }
    }
}
