﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using BLL.DTO;
using DAL.Models;
using DAL.Repositories;

namespace BLL.Services.Entity
{
    class TestService : BaseService<Test, TestDTO>, ITestService
    {
        protected override Test convertToDomain(TestDTO dto)
        {
            return (Test)dto;
        }

        protected override TestDTO convertToDTO(Test domain)
        {
            return (TestDTO)domain;
        }

        public IEnumerable<RespondentTestDTO> GetRespondentTests(int userId)
        {
            IRepository<User> userRepo = AutofacResolver.Container.Resolve<IRepository<User>>();

            User user = userRepo.Get(new object[] { userId });

            IRepository<TestGroup> tgRepo = AutofacResolver.Container.Resolve<IRepository<TestGroup>>();
            var testIds = tgRepo.Find(tg => tg.GroupId == user.GroupId)
                .Select(tg => new
                {
                    TestId = tg.TestId,
                    IsPassed = DateTime.Now > tg.Expires ||
                        user.Results.Any(r => r.QAnswer.Question.TestId == tg.TestId),
                    Feedback = tg.Feedback != null ? tg.Feedback.Text : null
                
                });

            return testIds.Select(test => new { Test = repo.Get(new object[] { test.TestId }), IsPassed = test.IsPassed, Feedback = test.Feedback }).Select(test => new RespondentTestDTO(test.Test, test.IsPassed, test.Feedback));
        } 
    }
}
