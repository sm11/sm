﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using DAL.Models;

namespace BLL.Services.Entity
{
    class TestGroupService : BaseService<TestGroup, TestGroupDTO>, ITestGroupService
    {
        protected override TestGroup convertToDomain(TestGroupDTO dto)
        {
            return (TestGroup)dto;
        }

        protected override TestGroupDTO convertToDTO(TestGroup domain)
        {
            return (TestGroupDTO)domain;
        }
    }
}
