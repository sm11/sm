﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;

namespace BLL.Services
{
    public interface IGroupService : IService<GroupDTO>
    {
        IEnumerable<GroupDTO> GetHierarchy(int id);
        IEnumerable<GroupDTO> GetRootGroups();
        IEnumerable<GroupDTO> GetGroups();
        IEnumerable<GroupDTO> GetSubgroups(int id);
        GroupDTO CreateGroup(GroupDTO group, int? creatorId);
    }
}
