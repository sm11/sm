﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;

namespace BLL.Services
{
    public interface IAnswerService : IService<AnswerDTO>
    {
        void CreateItems(IEnumerable<AnswerDTO> answers, int questionId);
        void UpdateItems(IEnumerable<AnswerDTO> answers, int questionId);

    }
}
