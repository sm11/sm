﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public interface IService<T> where T : class
    {
        IEnumerable<T> Find(Func<T, bool> predicate);
        T Get(object[] key);

        void CreateItem(T item);
        void Update(T item);
        void Delete(object[] key); 
    }
}
