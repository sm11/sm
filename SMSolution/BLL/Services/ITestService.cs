﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;

namespace BLL.Services
{
    public interface ITestService : IService<TestDTO>
    {
        IEnumerable<RespondentTestDTO> GetRespondentTests(int userId);
    }
}
