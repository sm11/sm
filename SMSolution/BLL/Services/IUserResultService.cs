﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;

namespace BLL.Services
{
    public interface IUserResultService : IService<UserResultDTO>
    {
        int GetQAnswerId(int answerId, int questionId);
        IEnumerable<QuestionResultDTO> GetTestResults(int testId, int groupId);
        void AddResults(IEnumerable<UserResultDTO> userResults);
    }
}
