﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using DAL.Models;
using DAL.Repositories;

namespace BLL.DTO
{
    public class UserResultDTO
    {
        private UserResult result;
        public int UserId { get; set; }
        public int QAnswerId { get; set; }

        private void loadUserResult()
        {
            IRepository<UserResult> repo = AutofacResolver.Container.Resolve<IRepository<UserResult>>();
            this.result = repo.Get(new object[] { this.UserId, QAnswerId });
        }

        public virtual UserDTO User
        {
            get
            {
                if (result == null || result.UserId != this.UserId || result.QAnswerId != this.QAnswerId)
                {
                    this.loadUserResult();

                    // no entity with such id in db
                    if (result == null)
                    {
                        return null;
                    }
                }

                return (UserDTO) result.User;
            }
        }

        public virtual QAnswerDTO QAnswer
        {
            get
            {
                if (result == null || result.UserId != this.UserId)
                {
                    this.loadUserResult();

                    // no entity with such id in db
                    if (result == null)
                    {
                        return null;
                    }
                }

                return (QAnswerDTO) result.QAnswer;
            }
        }

        public static explicit operator UserResultDTO(UserResult item)
        {
            if (item == null) { return null; }

            return new UserResultDTO()
            {
                UserId = item.UserId,
                QAnswerId = item.QAnswerId
            };
        }

        public static explicit operator UserResult(UserResultDTO item)
        {
            if (item == null) { return null; }

            return new UserResult()
            {
                QAnswerId = item.QAnswerId,
                UserId = item.UserId
            };
        }
    }
}
