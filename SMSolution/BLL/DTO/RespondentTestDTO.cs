﻿using DAL.Models;

namespace BLL.DTO
{
    public class RespondentTestDTO : TestDTO
    {
        public bool IsPassed { get; set; }
        public string FeedbackMessage { get; set; }

        public RespondentTestDTO(Test item, bool isPassed, string feedback)
        {
            this.TestId = item.TestId;
            this.InterviewerId = item.InterviewerId;
            this.Name = item.Name;
            this.IsPassed = isPassed;
            this.FeedbackMessage = feedback;
        } 
    }
}
