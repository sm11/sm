﻿using System;
using System.Collections.Generic;
using System.Linq; 
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using DAL.Models;
using DAL.Repositories;

namespace BLL.DTO
{
    public class TestGroupDTO
    {
        private TestGroup testGroup;
        public int TestId { get; set; }
        public int GroupId { get; set; }
        public DateTime Expires { get; set; }

        private void loadTestGroup()
        {
            IRepository<TestGroup> repo = AutofacResolver.Container.Resolve<IRepository<TestGroup>>();

            this.testGroup = repo.Get(new object[] { this.TestId, this.GroupId });
        }

        public virtual TestDTO Test
        {
            get
            {
                if (testGroup == null || testGroup.TestId != this.TestId || testGroup.GroupId != this.GroupId)
                {
                    this.loadTestGroup();

                    // no entity with such id in db
                    if (testGroup == null)
                    {
                        return null;
                    }
                }

                return (TestDTO)testGroup.Test;
            }
        }

        public virtual GroupDTO Group
        {
            get
            {
                if (testGroup == null || testGroup.TestId != this.TestId || testGroup.GroupId != this.GroupId)
                {
                    this.loadTestGroup();

                    // no entity with such id in db
                    if (testGroup == null)
                    {
                        return null;
                    }
                }

                return (GroupDTO)testGroup.Group;
            }
        }

        public static explicit operator TestGroupDTO(TestGroup testGroup)
        {
            if (testGroup == null) { return null; }

            return new TestGroupDTO()
            {
                TestId = testGroup.TestId,
                GroupId = testGroup.GroupId,
                Expires = testGroup.Expires
            };
        }

        public static explicit operator TestGroup(TestGroupDTO testGroup)
        {
            if (testGroup == null) { return null; }

            return new TestGroup()
            {
                TestId = testGroup.TestId,
                GroupId = testGroup.GroupId,
                Expires = testGroup.Expires
            };
        }
    }
}
