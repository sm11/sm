﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using DAL.Models;
using DAL.Repositories;

namespace BLL.DTO
{
    public class AnswerDTO
    {
        private Answer answer;
        public int AnswerId { get; set; }
        public string Text { get; set; }

        public virtual IEnumerable<QAnswerDTO> Questions
        {
            get
            {
                if (answer == null || answer.AnswerId != this.AnswerId)
                {
                    this.loadAnswer();

                    // entity not found in db
                    if (answer == null)
                    {
                        return null;
                    }
                }
                return answer.Questions.Select(q => (QAnswerDTO)q);
            }
        }

        private void loadAnswer()
        {
            IRepository<Answer> repo = AutofacResolver.Container.Resolve<IRepository<Answer>>();

            this.answer = repo.Get(new object[] { this.AnswerId });
        }


        public static explicit operator AnswerDTO(Answer qAnswer)
        {
            if (qAnswer == null) { return null; }

            return new AnswerDTO()
            {
                Text = qAnswer.Text,
                AnswerId = qAnswer.AnswerId
            };
        }

        public static explicit operator Answer(AnswerDTO qAnswer)
        {
            if (qAnswer == null) { return null; }

            return new Answer()
            {
                Text = qAnswer.Text,
                AnswerId = qAnswer.AnswerId
            };
        }
    }
}
