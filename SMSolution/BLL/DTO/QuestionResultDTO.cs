﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class QuestionResultDTO
    {
        //private QAnswerDTO qAnswer;
        public int QAnswerId { set; get; }
        public int Percent { set; get; }
        public QAnswerDTO QAnswer { set; get; }
    }
}
