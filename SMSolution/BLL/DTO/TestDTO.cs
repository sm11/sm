﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using DAL.Models;
using DAL.Repositories;

namespace BLL.DTO
{
    public class TestDTO
    {
        private Test test;

        public int TestId { get; set; }
        public string Name { get; set; }
        public int InterviewerId { get; set; }

        public virtual UserDTO Interviewer {
            get
            {
                if (test == null || test.TestId != this.TestId)
                {
                    this.loadTest();

                    // entity not found in db
                    if (test == null)
                    {
                        return null;
                    }
                }
                return (UserDTO)test.Interviewer;
            }
        }        

        public IEnumerable<QuestionDTO> Questions
        {
            get
            {
                if (test == null || test.TestId != this.TestId)
                {
                    this.loadTest();

                    // entity not found in db
                    if (test == null)
                    {
                        return null;
                    }
                }
                return test.Questions.Select(q => (QuestionDTO)q);
            }
        }

        public IEnumerable<TestGroupDTO> Groups
        {
            get
            {
                if (test == null || test.TestId != this.TestId)
                {
                    this.loadTest();

                    // entity not found in db
                    if (test == null)
                    {
                        return null;
                    }
                }

                return test.Groups.Select(t => (TestGroupDTO)t);
            }
        }

        private void loadTest()
        {
            IRepository<Test> repo = AutofacResolver.Container.Resolve<IRepository<Test>>();

            this.test = repo.Get(new object[] { this.TestId });
        }

        public static explicit operator TestDTO(Test test)
        {
            if (test == null) { return null; }

            return new TestDTO()
            {
                TestId = test.TestId,
                Name = test.Name,
                InterviewerId = test.InterviewerId
            };
        }

        public static explicit operator Test(TestDTO test)
        {
            if (test == null) { return null; }

            return new Test()
            {
                TestId = test.TestId,
                Name = test.Name,
                InterviewerId = test.InterviewerId
            };
        }
    }
}
