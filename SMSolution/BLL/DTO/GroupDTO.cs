﻿using BLL.DI;
using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace BLL.DTO
{
    public class GroupDTO
    {
        private Group group = null;

        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int? ParentId { get; set; }

        public GroupDTO Parent
        {
            get
            {
                if (group == null || group.GroupId == this.GroupId)
                {
                    this.loadGroup();

                    // entity not found in db
                    if (group == null)
                    {
                        return null;
                    }
                }
                return (GroupDTO)group.Parent;
            }
        }

        public IEnumerable<UserDTO> Users
        {
            get
            {
                if (group == null || group.GroupId != this.GroupId)
                {
                    this.loadGroup();

                    // entity not found in db
                    if (group == null)
                    {
                        return null;
                    }
                }

                return group.Users != null ? group.Users.Select(u => (UserDTO)u) : new List<UserDTO>();
            }
        }

        private void loadGroup()
        {
            IRepository<Group> repo = AutofacResolver.Container.Resolve<IRepository<Group>>();

            this.group = repo.Get(new object[] { this.GroupId });
        }

        public static explicit operator GroupDTO(Group group)
        {
            if (group == null) { return null; }

            return new GroupDTO()
            {
                GroupId = group.GroupId,
                GroupName = group.GroupName,
                ParentId = group.ParentId
            };
        }

        public static explicit operator Group(GroupDTO group)
        {
            if (group == null) { return null; }

            return new Group()
            {
                GroupId = group.GroupId,
                GroupName = group.GroupName,
                ParentId = group.ParentId
            };
        }
    }
}
