﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace BLL.DTO
{
    public class FeedbackDTO
    {
        private Feedback feedback;

        public int TestId { get; set; }
        public int GroupId { get; set; }
        public string Text { get; set; }

        //public TestGroupDTO TestGroup { }
    }
}
