﻿using BLL.DI;
using DAL.Models;
using DAL.Repositories;
using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class UserDTO
    {
        private User user;

        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int? GroupId { get; set; }
        public string Role { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }

        public GroupDTO Group
        {
            get
            {
                if (user == null || user.UserId != this.UserId)
                {
                    this.loadUser();

                    // no entity with such id in db
                    if (user == null)
                    {
                        return null;
                    }
                }

                return (GroupDTO)user.Group;
            }
        }

        private void loadUser()
        {
            IRepository<User> repo = AutofacResolver.Container.Resolve<IRepository<User>>();
            this.user = repo.Get(new object[] { this.UserId });
        }

        public static explicit operator UserDTO(User item)
        {
            if (item == null) { return null; }

            return new UserDTO()
            {
                Email = item.Email,
                FullName = item.FullName,
                GroupId = item.GroupId,
                Login = item.Login,
                Password = item.Password,
                PhoneNumber = item.PhoneNumber,
                Role = item.Role,
                UserId = item.UserId
            };
        }

        public static explicit operator User(UserDTO item)
        {
            if (item == null) { return null; }

            return new User()
            {
                Email = item.Email,
                FullName = item.FullName,
                GroupId = item.GroupId,
                Login = item.Login,
                Password = item.Password,
                PhoneNumber = item.PhoneNumber,
                Role = item.Role,
                UserId = item.UserId
            };
        }
    }
}
