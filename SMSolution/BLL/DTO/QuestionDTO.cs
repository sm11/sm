﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using DAL.Models;
using DAL.Repositories;

namespace BLL.DTO
{
    public class QuestionDTO
    {
        private Question question;
        public int QuestionId { get; set; }
        public string Text { get; set; }
        public int TestId { get; set; }
        public QuestionType Type { get; set; }

        private void loadQuestion()
        {
            IRepository<Question> repo = AutofacResolver.Container.Resolve<IRepository<Question>>();

            this.question = repo.Get(new object[] { this.QuestionId });
        }

        public virtual TestDTO Test
        {
            get
            {
                if (question == null || question.QuestionId != this.QuestionId)
                {
                    this.loadQuestion();

                    // no entity with such id in db
                    if (question == null)
                    {
                        return null;
                    }
                }

                return (TestDTO)question.Test;
            }
        }


        public static explicit operator QuestionDTO(Question question)
        {
            if (question == null) { return null; }

            return new QuestionDTO()
            {
                TestId = question.TestId,
                QuestionId = question.QuestionId,
                Text = question.Text,
                Type = question.Type
            };
        }

        public static explicit operator Question(QuestionDTO question)
        {
            if (question == null) { return null; }

            return new Question()
            {
                TestId = question.TestId,
                QuestionId = question.QuestionId,
                Text = question.Text,
                Type = question.Type
            };
        }

        public virtual IEnumerable<QAnswerDTO> Answers {
            get
            {
                if (question == null || question.QuestionId != this.QuestionId)
                {
                    this.loadQuestion();

                    // entity not found in db
                    if (question == null)
                    {
                        return null;
                    }
                }
                TestRepository repo = new TestRepository();
                return repo.GetQAnswersByQuestionId(QuestionId).Select(q => (QAnswerDTO)q);
            }
        }
    }
}
