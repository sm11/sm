﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using DAL.Models;
using DAL.Repositories;

namespace BLL.DTO
{
    public class InterviewerDTO : UserDTO
    {
        private User interviewer;
        public IEnumerable<TestDTO> CreatedTests
        {
            get
            {
                if (interviewer == null || interviewer.UserId != this.UserId)
                {
                    this.loadUser();

                    // entity not found in db
                    if (interviewer == null)
                    {
                        return null;
                    } 
                }
                return interviewer.CreatedTests.Select(q => (TestDTO)q);
            }
        }

        private void loadUser()
        {
            IRepository<User> repo = AutofacResolver.Container.Resolve<IRepository<User>>();
            this.interviewer = repo.Get(new object[] { this.UserId });
        }

        public static explicit operator InterviewerDTO(User item)
        {
            if (item == null) { return null; }

            return new InterviewerDTO()
            {
                Email = item.Email,
                FullName = item.FullName,
                GroupId = item.GroupId,
                Login = item.Login,
                Password = item.Password,
                PhoneNumber = item.PhoneNumber,
                Role = item.Role,
                UserId = item.UserId
            };
        }

        public static explicit operator User(InterviewerDTO item)
        {
            if (item == null) { return null; }

            return (User) ((UserDTO) item);
        }

    }
}
