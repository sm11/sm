﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BLL.DI;
using DAL.Models;
using DAL.Repositories;

namespace BLL.DTO
{
    public class QAnswerDTO
    {
        private QAnswer qAnswer;
        public int QAnswerId { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }

        public virtual IEnumerable<UserResultDTO> Result
        {
            get
            {
                if (qAnswer == null || qAnswer.QAnswerId != this.QAnswerId)
                {
                    this.loadQAnswer();

                    // no entity with such id in db
                    if (qAnswer == null)
                    {
                        return null;
                    }
                }

                return qAnswer.Results.Select(res => (UserResultDTO)res);
            }
        }

        private void loadQAnswer()
        {
            IRepository<QAnswer> repo = AutofacResolver.Container.Resolve<IRepository<QAnswer>>();

            this.qAnswer = repo.Get(new object[] { this.QAnswerId });
        }

        public virtual QuestionDTO Question
        {
            get
            {
                if (qAnswer == null || qAnswer.QuestionId != this.QuestionId)
                {
                    this.loadQAnswer();

                    // no entity with such id in db
                    if (qAnswer == null)
                    {
                        return null;
                    }
                }

                return (QuestionDTO)qAnswer.Question;
            }
        }

        public virtual AnswerDTO Answer
        {
            get
            {
                if (qAnswer == null || qAnswer.QuestionId != this.QuestionId)
                {
                    this.loadQAnswer();

                    // no entity with such id in db
                    if (qAnswer == null)
                    {
                        return null;
                    }
                }

                return (AnswerDTO)qAnswer.Answer;
            }
        }


        public static explicit operator QAnswerDTO(QAnswer qAnswer)
        {
            if (qAnswer == null) { return null; }

            return new QAnswerDTO()
            {
                QAnswerId = qAnswer.QAnswerId,
                QuestionId = qAnswer.QuestionId,
                AnswerId = qAnswer.AnswerId
            };
        }

        public static explicit operator QAnswer(QAnswerDTO qAnswer)
        {
            if (qAnswer == null) { return null; }

            return new QAnswer()
            {
                QAnswerId = qAnswer.QAnswerId,
                QuestionId = qAnswer.QuestionId,
                AnswerId = qAnswer.AnswerId
            };
        }
    }
}
