"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var http_1 = require("@angular/http");
// components
var app_component_1 = require('./app.component');
var create_organization_component_1 = require("./create.organization.component");
var login_component_1 = require("./login.component");
var register_component_1 = require("./register.component");
var test_passing_component_1 = require("./test.passing.component");
var create_test_component_1 = require("./create.test.component");
var interviewers_component_1 = require("./interviewers.component");
var assign_test_component_1 = require("./assign.test.component");
var respondent_component_1 = require("./respondent.component");
var report_component_1 = require("./report.component");
var profile_editing_component_1 = require("./profile.editing.component");
var group_list_component_1 = require("./group-list.component");
var help_component_1 = require("./help.component");
//services
var user_service_1 = require("./services/user.service");
var test_group_service_1 = require("./services/test.group.service");
var test_service_1 = require("./services/test.service");
var test_respondent_service_1 = require("./services/test.respondent.service");
var group_service_1 = require("./services/group.service");
var test_passing_service_1 = require("./services/test.passing.service");
var edit_test_service_1 = require("./services/edit.test.service");
// routing
var app_routing_module_1 = require("./app-routing.module");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, app_routing_module_1.AppRoutingModule, http_1.HttpModule],
            declarations: [
                app_component_1.AppComponent, create_organization_component_1.CreateOrgComponent, login_component_1.LoginComponent, register_component_1.RegisterComponent, interviewers_component_1.InterviewersComponent,
                assign_test_component_1.AssignTestComponent, respondent_component_1.RespondentComponent, test_passing_component_1.TestPassingComponent, report_component_1.ReportComponent, profile_editing_component_1.ProfileEditingComponent,
                group_list_component_1.GroupListComponent, create_test_component_1.CreateTestComponent, help_component_1.HelpComponent
            ],
            providers: [user_service_1.UserService, test_group_service_1.TestGroupService, test_service_1.TestService, test_respondent_service_1.TestRespondentService, group_service_1.GroupService,
                test_passing_service_1.TestPassingService, edit_test_service_1.EditTestService
            ],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map