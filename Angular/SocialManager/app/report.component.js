"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var Test_1 = require("./Models/Test");
var Feedback_1 = require("./Models/Feedback");
var test_question_service_1 = require("./Services/test.question.service");
var test_group_service_1 = require("./Services/test.group.service");
var test_passing_service_1 = require("./Services/test.passing.service");
var feedback_service_1 = require("./Services/feedback.service");
var ErrorHandler_1 = require("./Library/ErrorHandler");
var ReportComponent = (function () {
    function ReportComponent(router, testQuestionService, testGroupService, TestPassingService, feedbackService) {
        this.router = router;
        this.testQuestionService = testQuestionService;
        this.testGroupService = testGroupService;
        this.TestPassingService = TestPassingService;
        this.feedbackService = feedbackService;
        this.testModel = null;
        this.questionResults = null;
        this.selectedQuestion = null;
        this.groups = [];
        this.maxAnswers = 0;
        this.feedback = new Feedback_1.Feedback();
        this.fbExists = true;
    }
    ReportComponent.prototype.getPercent = function (question, answer) {
        if (this.questionResults == null || this.selectedQuestion == null || this.selectedQuestion.text != question) {
            return "";
        }
        var qResult = this.questionResults.find(function (res) { return res.QuestionText == question && res.AnswerText == answer; });
        return qResult != null ? qResult.Percent.toString() : '-';
    };
    ReportComponent.prototype.getArray = function (length) {
        if (length <= 0) {
            return [];
        }
        var arr = [];
        arr.length = length;
        return arr;
    };
    ReportComponent.prototype.getSelectedGroupId = function () {
        var _this = this;
        var group = this.groups.filter(function (g) { return g.Name == _this.selectedGroupName; })[0];
        if (group != null) {
            return group.Id;
        }
        return -1;
    };
    ReportComponent.prototype.getSelectedGroup = function (groupName) {
        return this.groups.filter(function (g) { return g.Name == groupName; })[0];
    };
    ReportComponent.prototype.jQueryMarkup = function () {
        console.log("$ markup");
        $(".recall").click(function () {
            if ($(this).css('height') == '65px') {
                $(this).css({ 'height': "auto" });
            }
            else {
                $(this).css({ 'height': "65px" });
            }
        });
        $("td.q").click(function () {
            $(".resultTable").find("td.q").css({ 'background': "none" });
            $(this).css({ 'background': "linear-gradient(to right, #dbf29d, #fff)" });
        });
    };
    ReportComponent.prototype.ngOnInit = function () {
        var _this = this;
        var jsonTest = localStorage.getItem("selectedTest");
        var parsedTest = JSON.parse(jsonTest);
        this.test = new Test_1.Test(parsedTest.testId, parsedTest.name, parsedTest.interviewerId);
        if (this.test == null) {
            this.router.navigate(['/interviewers']);
        }
        this.TestPassingService.getTestParsed(this.test.TestId).then(function (test) {
            _this.testModel = test;
            var questions = _this.testModel.questions;
            var maxAnswers = 0;
            for (var i = 0; i < questions.length; i++) {
                if (maxAnswers < questions[0].answers.length) {
                    maxAnswers = questions[0].answers.length;
                }
            }
            _this.maxAnswers = maxAnswers;
        });
        this.testGroupService.getFinalGroupsByTestId(this.test.TestId).then(function (groups) {
            _this.groups = groups != null ? groups.filter(function (g) { return g != null; }) : [];
            setTimeout(_this.jQueryMarkup, 1);
            if (_this.groups.length > 0) {
                _this.selectedGroupName = _this.groups[0].Name;
                _this.testQuestionService.GetQuestionResults(_this.test.TestId, _this.groups[0].Id)
                    .then(function (results) {
                    _this.questionResults = results;
                    setTimeout(_this.jQueryMarkup, 1);
                });
                _this.feedbackService.getFeedbackById(_this.test.TestId, _this.groups[0].Id)
                    .then(function (fb) {
                    if (fb == null) {
                        _this.fbExists = false;
                        _this.feedback = new Feedback_1.Feedback();
                    }
                    else {
                        _this.fbExists = true;
                        _this.feedback = fb;
                    }
                    setTimeout(_this.jQueryMarkup, 1);
                });
            }
        });
    };
    ReportComponent.prototype.groupChanged = function (groupName) {
        var _this = this;
        setTimeout(this.jQueryMarkup, 1);
        var group = this.groups.find(function (g) { return g.Name == groupName; });
        if (group == null) {
            throw new Error("invalid selected group name");
        }
        this.testQuestionService.GetQuestionResults(this.test.TestId, group.Id)
            .then(function (results) {
            _this.questionResults = results;
        });
        this.feedbackService.getFeedbackById(this.test.TestId, this.getSelectedGroup(groupName).Id)
            .then(function (fb) {
            if (fb == null) {
                _this.fbExists = false;
                _this.feedback = new Feedback_1.Feedback();
            }
            else {
                _this.fbExists = true;
                _this.feedback = fb;
            }
        });
    };
    ReportComponent.prototype.SendFeedback = function () {
        var _this = this;
        this.feedback.TestId = this.test.TestId;
        this.feedback.GroupId = this.getSelectedGroupId();
        if (this.fbExists) {
            this.feedbackService.updateFeedback(this.feedback).then(function (res) {
                if (!res.IsSuccess) {
                    ErrorHandler_1.ErrorHandler.Error("Invalid user data.");
                }
                else {
                    _this.fbExists = true;
                }
            });
        }
        else {
            this.feedbackService.createFeedback(this.feedback).then(function (res) {
                if (!res.IsSuccess) {
                    ErrorHandler_1.ErrorHandler.Error("Creating feedback is not available. Please try later.");
                }
                else {
                    _this.fbExists = true;
                }
            });
        }
    };
    ReportComponent = __decorate([
        core_1.Component({
            selector: 'report',
            templateUrl: '/app/Views/report.component.html',
            providers: [test_question_service_1.TestQuestionService, test_group_service_1.TestGroupService, test_passing_service_1.TestPassingService, feedback_service_1.FeedbackService]
        }), 
        __metadata('design:paramtypes', [router_1.Router, test_question_service_1.TestQuestionService, test_group_service_1.TestGroupService, test_passing_service_1.TestPassingService, feedback_service_1.FeedbackService])
    ], ReportComponent);
    return ReportComponent;
}());
exports.ReportComponent = ReportComponent;
var QuestionReportViewModel = (function () {
    function QuestionReportViewModel(question) {
        this.QuestionText = null;
        this.Percentages = [];
        this.Answers = [];
        this.QuestionText = question;
    }
    return QuestionReportViewModel;
}());
//# sourceMappingURL=report.component.js.map