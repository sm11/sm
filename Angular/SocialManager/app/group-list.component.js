"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var GroupSelecter_1 = require("./Library/GroupSelecter");
var GroupListComponent = (function () {
    function GroupListComponent() {
        this.selectedGroup = GroupSelecter_1.GroupSelector.Group;
    }
    GroupListComponent.prototype.selectGroup = function (item) {
        this.selectedGroup = item;
        GroupSelecter_1.GroupSelector.Group = item;
    };
    GroupListComponent.prototype.hasSubGroups = function (item) {
        return this.Groups.findIndex(function (value) { return value.parentId == item.id; }) != -1;
    };
    GroupListComponent.prototype.getSubGroups = function (item) {
        return this.Groups.filter(function (group) { return group.parentId == item.id; });
    };
    GroupListComponent.prototype.jQueryMarkup = function () {
        //$(".ul-dropfree").find("li:has(ul):has(li)").prepend('<div class="drop"></div>');
        $(".ul-dropfree div.drop").unbind('click');
        $(".ul-dropfree div.drop").click(function (e) {
            if ($(this).nextAll('div.group').nextAll("div").children('groups').children('ul').css('display') == 'none') {
                $(this).nextAll('div.group').nextAll("div").children('groups').children('ul').slideDown(400);
                $(this).css({ 'background-position': "-11px 0" });
            }
            else {
                $(this).nextAll('div.group').nextAll("div").children('groups').children('ul').slideUp(400);
                $(this).css({ 'background-position': "0 0" });
            }
        });
        $(".ul-dropfree").find("ul").slideUp(400).parents("li").children("div.drop").css({ 'background-position': "0 0" });
    };
    GroupListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.levelGroups = this.Groups.filter(function (group) { return group.parentId == _this.parentId; });
        GroupSelecter_1.GroupSelector.RegisterCallback(function (group) { return _this.selectedGroup = group; });
        GroupSelecter_1.GroupSelector.RegisterGroupArrCallback(function (groups) {
            return _this.levelGroups = _this.Groups.filter(function (group) { return group.parentId == _this.parentId; });
        });
        setTimeout(this.jQueryMarkup, 1);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], GroupListComponent.prototype, "Groups", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], GroupListComponent.prototype, "parentId", void 0);
    GroupListComponent = __decorate([
        core_1.Component({
            selector: 'groups',
            templateUrl: '/app/Views/group.list.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], GroupListComponent);
    return GroupListComponent;
}());
exports.GroupListComponent = GroupListComponent;
//# sourceMappingURL=group-list.component.js.map