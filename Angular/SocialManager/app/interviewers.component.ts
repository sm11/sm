import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { ErrorHandler } from "./Library/ErrorHandler";
import { Session } from "./Library/Session";

// models
import { Test } from "./Models/Test";
import { User } from "./Models/User";

// services
import { TestService } from "./Services/test.service";
import { EditTestService } from "./Services/edit.test.service";

@Component({
    selector: "<interviewer-page></interviewer-page>",
    templateUrl: "/app/Views/interviewers.component.html",
    providers: [TestService, EditTestService]
})
export class InterviewersComponent implements OnInit {

    private tests: Test[];
    private user: User;
    private deleteTestId: number;

    public constructor(private testService: TestService, private editTestService: EditTestService, private router: Router) {
        const ROLE = "interviewer";

        this.user = Session.AuthenticatedUser;
        
        if (this.user == null || this.user.role != ROLE) {
            this.router.navigate(['/login']);
        }
    }


    ngOnInit() {
        this.getTests();
    }

    public getTests () {
        this.testService.getTestsByInterviewer(this.user.id).then(tests => {
            this.tests = tests;
        });
    }

    public moveToAssignTest(test: Test) {

        if (test == null) {
            console.warn('try to assign group to null test');
            return;
        }

        localStorage.setItem('selectedTest', JSON.stringify(test));
        this.router.navigate(['assign-test']);
    }

    public moveToReport(test: Test) {
        if (test == null) {
            console.warn('try to assign group to null test');
            return;
        }

        localStorage.setItem('selectedTest', JSON.stringify(test));
        this.router.navigate(['report']);
    }

    public moveToUpdateTest(test: Test) {
        sessionStorage.setItem('selectedTest', JSON.stringify(test));
        this.router.navigate([ "/createTest" ]);
    }

    public moveToCreateTest() {
        this.router.navigate([ "/createTest" ]);
    }

    public deleteTest(testId: number) {
        this.editTestService.deleteTest(testId).then(model => {
                    if (model.IsSuccess) {
                        ErrorHandler.Error("Test successfully deleted!");
                    } else {
                        ErrorHandler.Error(model.Message);
                    }
                });
        this.deleteTestId = null;
        this.tests.splice(this.tests.findIndex(t => t.TestId == testId), 1);
    }

    public getTestById (testId: number) : Test{
        return this.tests.find(t => t.TestId == testId);
    }
}