import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// components
import { CreateOrgComponent } from "./create.organization.component";
import { LoginComponent } from "./login.component";
import { RegisterComponent } from "./register.component";
import { TestPassingComponent } from "./test.passing.component";
import { CreateTestComponent } from "./create.test.component";

import { InterviewersComponent } from "./interviewers.component"; 
import { AssignTestComponent } from "./assign.test.component";

import { RespondentComponent } from "./respondent.component";
import { ReportComponent } from "./report.component";

import { ProfileEditingComponent } from "./profile.editing.component";
import { HelpComponent } from "./help.component"

const routes : Routes = [
    { 
        path: '',
        redirectTo: "/login",
        pathMatch: 'full'
    },
    {
        path: 'createOrg',
        component: CreateOrgComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'register',
        component: RegisterComponent
    },
    {
        path: 'testPassing',
        component: TestPassingComponent
    },
    {
        path: 'interviewer',
        component: InterviewersComponent
    },
    {
        path: 'assign-test',
        component : AssignTestComponent
    },
    {
        path: 'respondent',
        component: RespondentComponent
    },
    {
        path: 'report',
        component: ReportComponent
    },
    {
        path: 'profile-editing',
        component: ProfileEditingComponent
    },
    {
        path: 'createTest',
        component: CreateTestComponent
    },
    {
        path: 'help',
        component: HelpComponent
    }
]

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }