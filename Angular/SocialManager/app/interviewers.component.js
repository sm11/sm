"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var ErrorHandler_1 = require("./Library/ErrorHandler");
var Session_1 = require("./Library/Session");
// services
var test_service_1 = require("./Services/test.service");
var edit_test_service_1 = require("./Services/edit.test.service");
var InterviewersComponent = (function () {
    function InterviewersComponent(testService, editTestService, router) {
        this.testService = testService;
        this.editTestService = editTestService;
        this.router = router;
        var ROLE = "interviewer";
        this.user = Session_1.Session.AuthenticatedUser;
        if (this.user == null || this.user.role != ROLE) {
            this.router.navigate(['/login']);
        }
    }
    InterviewersComponent.prototype.ngOnInit = function () {
        this.getTests();
    };
    InterviewersComponent.prototype.getTests = function () {
        var _this = this;
        this.testService.getTestsByInterviewer(this.user.id).then(function (tests) {
            _this.tests = tests;
        });
    };
    InterviewersComponent.prototype.moveToAssignTest = function (test) {
        if (test == null) {
            console.warn('try to assign group to null test');
            return;
        }
        localStorage.setItem('selectedTest', JSON.stringify(test));
        this.router.navigate(['assign-test']);
    };
    InterviewersComponent.prototype.moveToReport = function (test) {
        if (test == null) {
            console.warn('try to assign group to null test');
            return;
        }
        localStorage.setItem('selectedTest', JSON.stringify(test));
        this.router.navigate(['report']);
    };
    InterviewersComponent.prototype.moveToUpdateTest = function (test) {
        sessionStorage.setItem('selectedTest', JSON.stringify(test));
        this.router.navigate(["/createTest"]);
    };
    InterviewersComponent.prototype.moveToCreateTest = function () {
        this.router.navigate(["/createTest"]);
    };
    InterviewersComponent.prototype.deleteTest = function (testId) {
        this.editTestService.deleteTest(testId).then(function (model) {
            if (model.IsSuccess) {
                ErrorHandler_1.ErrorHandler.Error("Test successfully deleted!");
            }
            else {
                ErrorHandler_1.ErrorHandler.Error(model.Message);
            }
        });
        this.deleteTestId = null;
        this.tests.splice(this.tests.findIndex(function (t) { return t.TestId == testId; }), 1);
    };
    InterviewersComponent.prototype.getTestById = function (testId) {
        return this.tests.find(function (t) { return t.TestId == testId; });
    };
    InterviewersComponent = __decorate([
        core_1.Component({
            selector: "<interviewer-page></interviewer-page>",
            templateUrl: "/app/Views/interviewers.component.html",
            providers: [test_service_1.TestService, edit_test_service_1.EditTestService]
        }), 
        __metadata('design:paramtypes', [test_service_1.TestService, edit_test_service_1.EditTestService, router_1.Router])
    ], InterviewersComponent);
    return InterviewersComponent;
}());
exports.InterviewersComponent = InterviewersComponent;
//# sourceMappingURL=interviewers.component.js.map