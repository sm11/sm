import { Component, OnInit } from "@angular/core";
import { UserService } from "./Services/user.service";
import { LoginViewModel } from "./Models/LoginViewModel";
import { User } from "./Models/User";
import { Router } from "@angular/router";

import { Session } from "./Library/Session";
import { ErrorHandler } from "./Library/ErrorHandler";

@Component({
    templateUrl: "/app/views/login.component.html",
    providers: [UserService]
})
export class LoginComponent implements OnInit {
    private Login: string;
    private Password: string;

    public constructor(private userService: UserService, private router: Router) {
    }

    private validate() {
        var loginForm = <HTMLFormElement>document.getElementsByName('loginForm')[0];
        if (loginForm.checkValidity()) {
            return true;
        }
        return false;
    }

    private getRouterNavigate(user: User): string[] {
        if (user.role == 'creator') {
            return ["/createOrg"];
        }
        else if (user.role == 'interviewer') {
            return ['/interviewer'];
        }
        else if (user.role == 'respondent') {
            return ['/respondent'];
        }
    }

    ngOnInit() {
        let user = Session.AuthenticatedUser;

        if (user != null) {
            this.router.navigate(this.getRouterNavigate(user));
        }
    }

    public login() {
        if (!this.validate()) {
            return false;
        }
        console.log(this.Login);
        this.userService.Login(this.Login, this.Password).then(model => {
            if (model != null) {
                if (model.User.role == 'creator') {
                    this.router.navigate(["/createOrg"]);
                }
                else if (model.User.role == 'interviewer') {
                    this.router.navigate(['/interviewer']);
                }
                else if (model.User.role == 'respondent') {
                    this.router.navigate(['/respondent']);
                }
                Session.AuthenticatedUser = model.User;
            }
            else {
                ErrorHandler.Error("Invalid login or password");
            }
        });

    }

}