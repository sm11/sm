"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var ErrorHandler_1 = require("./Library/ErrorHandler");
var Session_1 = require("./Library/Session");
var test_passing_service_1 = require("./Services/test.passing.service");
var TestViewModel_1 = require("./Models/TestViewModel");
var UserResultViewModel_1 = require("./Models/UserResultViewModel");
var User_1 = require("./Models/User");
var QuestionViewModel_1 = require("./Models/QuestionViewModel");
var AnswerViewModel_1 = require("./Models/AnswerViewModel");
var TestPassingComponent = (function () {
    function TestPassingComponent(testPassingService, router) {
        this.testPassingService = testPassingService;
        this.router = router;
        this.currentUserResultViewModel = new UserResultViewModel_1.UserResultViewModel();
        this.result = [];
        this.testViewModel = new TestViewModel_1.TestViewModel();
        this.user = new User_1.User();
    }
    TestPassingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = new User_1.User();
        this.user = Session_1.Session.AuthenticatedUser;
        this.result = new Array();
        if (this.user == null) {
            this.router.navigate(["/login"]);
        }
        this.currentUserResultViewModel = new UserResultViewModel_1.UserResultViewModel();
        this.currentUserResultViewModel.userId = this.user.id;
        var parsedTest = JSON.parse(sessionStorage.getItem('selectedTest'));
        this.testPassingService.getTest(parsedTest.testId).then(function (model) {
            Session_1.Session.AuthenticatedUser = _this.user;
            _this.testViewModel = new TestViewModel_1.TestViewModel(model.TestId, model.Name, model.InterviewerId, model.Questions.map(function (q) { return new QuestionViewModel_1.QuestionViewModel(q.QuestionId, q.Text, q.TestId, q.Type, q.Answers.map(function (a) {
                return new AnswerViewModel_1.AnswerViewModel(a.AnswerId, a.Text);
            })); }));
        });
    };
    TestPassingComponent.prototype.saveSingleAns = function (questionId, answerId) {
        for (var i = 0; i < this.result.length; i++) {
            if (this.result[i].questionId == questionId) {
                this.result[i].answerId = answerId;
                return;
            }
        }
        this.currentUserResultViewModel = new UserResultViewModel_1.UserResultViewModel(); //
        this.currentUserResultViewModel.userId = this.user.id;
        this.currentUserResultViewModel.answerId = answerId;
        this.currentUserResultViewModel.questionId = questionId;
        this.result.push(this.currentUserResultViewModel);
    };
    TestPassingComponent.prototype.saveMultipleAns = function (questionId, answerId) {
        this.currentUserResultViewModel = new UserResultViewModel_1.UserResultViewModel(); //
        this.currentUserResultViewModel.userId = this.user.id;
        this.currentUserResultViewModel.answerId = answerId;
        this.currentUserResultViewModel.questionId = questionId;
        if (this.result.indexOf(this.currentUserResultViewModel) == -1) {
            this.result.push(this.currentUserResultViewModel);
        }
    };
    TestPassingComponent.prototype.isInResult = function (questionId) {
        for (var _i = 0, _a = this.result; _i < _a.length; _i++) {
            var ans = _a[_i];
            if (ans.questionId == questionId) {
                return true;
            }
        }
        return false;
    };
    TestPassingComponent.prototype.validate = function () {
        var Form = document.getElementsByName('testPassing')[0];
        for (var _i = 0, _a = this.testViewModel.questions; _i < _a.length; _i++) {
            var question = _a[_i];
            if (!this.isInResult(question.questionId)) {
                ErrorHandler_1.ErrorHandler.Error("You forgot to answer the question \"" + question.text + "\"");
                return false;
            }
        }
        return Form.checkValidity();
    };
    TestPassingComponent.prototype.saveTest = function () {
        var _this = this;
        if (!this.validate()) {
            ErrorHandler_1.ErrorHandler.Error("There are something wrong in this form");
            return;
        }
        this.testPassingService.saveTest(this.result).then(function (model) {
            if (model.IsSuccess) {
                _this.router.navigate(["/respondent"]);
            }
            else {
                ErrorHandler_1.ErrorHandler.Error(model.Message);
            }
        });
    };
    TestPassingComponent.prototype.back = function () {
        history.back();
    };
    TestPassingComponent = __decorate([
        core_1.Component({
            selector: "<testPassing-page></testPassing-page>",
            templateUrl: "app/Views/test.passing.component.html",
            providers: [test_passing_service_1.TestPassingService]
        }), 
        __metadata('design:paramtypes', [test_passing_service_1.TestPassingService, router_1.Router])
    ], TestPassingComponent);
    return TestPassingComponent;
}());
exports.TestPassingComponent = TestPassingComponent;
//# sourceMappingURL=test.passing.component.js.map