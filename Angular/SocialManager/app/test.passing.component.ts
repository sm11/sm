import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { ErrorHandler } from "./Library/ErrorHandler";
import { Session } from "./Library/Session";
import { TestPassingService } from "./Services/test.passing.service"

import { TestViewModel} from "./Models/TestViewModel"
import { UserResultViewModel } from "./Models/UserResultViewModel";
import { User } from "./Models/User";
import { Test } from "./Models/Test";

import { QuestionViewModel} from "./Models/QuestionViewModel";
import { AnswerViewModel} from "./Models/AnswerViewModel";

@Component({
selector:"<testPassing-page></testPassing-page>",
templateUrl: "app/Views/test.passing.component.html",
providers: [TestPassingService]
})

export class TestPassingComponent implements  OnInit{
    private currentUserResultViewModel: UserResultViewModel = new UserResultViewModel();
    private result: UserResultViewModel[] = [];
    private testViewModel : TestViewModel= new TestViewModel();
    private user: User = new User();
    private errorMessage: string;


    public constructor(private testPassingService : TestPassingService, private router: Router) {
    }

    ngOnInit() {
        this.user = new User();
        this.user = Session.AuthenticatedUser;
        this.result = new Array <UserResultViewModel>();
        if (this.user == null) {
            this.router.navigate([ "/login" ]);
        }
        this.currentUserResultViewModel = new UserResultViewModel();
        this.currentUserResultViewModel.userId = this.user.id;
        
        let parsedTest = JSON.parse(sessionStorage.getItem('selectedTest'));

        this.testPassingService.getTest(parsedTest.testId).then(model => {
            Session.AuthenticatedUser = this.user;
            this.testViewModel = new TestViewModel(model.TestId, model.Name, model.InterviewerId,
                model.Questions.map(q => new QuestionViewModel(q.QuestionId, q.Text, q.TestId, q.Type, q.Answers.map(a =>
                    new AnswerViewModel(a.AnswerId, a.Text)))));
        });
    }

    public saveSingleAns(questionId: number, answerId: number) {
        for (var i = 0; i < this.result.length; i++) {
            if (this.result[i].questionId == questionId) {
                this.result[i].answerId = answerId;
                return;
            }
        }
        this.currentUserResultViewModel = new UserResultViewModel(); //
        this.currentUserResultViewModel.userId = this.user.id;
        this.currentUserResultViewModel.answerId = answerId;
        this.currentUserResultViewModel.questionId = questionId;
        this.result.push(this.currentUserResultViewModel);
    }

    public saveMultipleAns(questionId: number, answerId: number) {
        this.currentUserResultViewModel = new UserResultViewModel(); //
        this.currentUserResultViewModel.userId = this.user.id;
        this.currentUserResultViewModel.answerId = answerId;
        this.currentUserResultViewModel.questionId = questionId;
        if (this.result.indexOf(this.currentUserResultViewModel) == -1) {
            this.result.push(this.currentUserResultViewModel);
        }
    }

    public isInResult(questionId: number): boolean{
        for (let ans of this.result) {
            if (ans.questionId == questionId) {
                return true;
            }
        }
        return false;
    }

    private validate() {
        var Form = <HTMLFormElement>document.getElementsByName('testPassing')[0];
        for (let question of this.testViewModel.questions) {
            if (!this.isInResult(question.questionId)) {
                ErrorHandler.Error("You forgot to answer the question \"" + question.text + "\"");
                return false;
            }
        }
        return Form.checkValidity();
    }

    public saveTest() {
         if (!this.validate()) {
             ErrorHandler.Error("There are something wrong in this form");
            return;
        }
        this.testPassingService.saveTest(this.result).then(model => {
            if (model.IsSuccess) {
                this.router.navigate(["/respondent"]);
            } else {
                ErrorHandler.Error(model.Message);
            }
        });
    }

    public back() {
        history.back();
    }
        
}