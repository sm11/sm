"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
// components
var create_organization_component_1 = require("./create.organization.component");
var login_component_1 = require("./login.component");
var register_component_1 = require("./register.component");
var test_passing_component_1 = require("./test.passing.component");
var create_test_component_1 = require("./create.test.component");
var interviewers_component_1 = require("./interviewers.component");
var assign_test_component_1 = require("./assign.test.component");
var respondent_component_1 = require("./respondent.component");
var report_component_1 = require("./report.component");
var profile_editing_component_1 = require("./profile.editing.component");
var help_component_1 = require("./help.component");
var routes = [
    {
        path: '',
        redirectTo: "/login",
        pathMatch: 'full'
    },
    {
        path: 'createOrg',
        component: create_organization_component_1.CreateOrgComponent
    },
    {
        path: 'login',
        component: login_component_1.LoginComponent
    },
    {
        path: 'register',
        component: register_component_1.RegisterComponent
    },
    {
        path: 'testPassing',
        component: test_passing_component_1.TestPassingComponent
    },
    {
        path: 'interviewer',
        component: interviewers_component_1.InterviewersComponent
    },
    {
        path: 'assign-test',
        component: assign_test_component_1.AssignTestComponent
    },
    {
        path: 'respondent',
        component: respondent_component_1.RespondentComponent
    },
    {
        path: 'report',
        component: report_component_1.ReportComponent
    },
    {
        path: 'profile-editing',
        component: profile_editing_component_1.ProfileEditingComponent
    },
    {
        path: 'createTest',
        component: create_test_component_1.CreateTestComponent
    },
    {
        path: 'help',
        component: help_component_1.HelpComponent
    }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule]
        }), 
        __metadata('design:paramtypes', [])
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map