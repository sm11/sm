"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var Session_1 = require("./Library/Session");
var ErrorHandler_1 = require("./Library/ErrorHandler");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
// services
var user_service_1 = require("./Services/user.service");
var ProfileEditingComponent = (function () {
    function ProfileEditingComponent(userService, router) {
        this.userService = userService;
        this.router = router;
    }
    ProfileEditingComponent.prototype.getRouterNavigate = function (user) {
        if (user.role == 'creator') {
            return ["/createOrg"];
        }
        else if (user.role == 'interviewer') {
            return ['/interviewer'];
        }
        else if (user.role == 'respondent') {
            return ['/respondent'];
        }
    };
    ProfileEditingComponent.prototype.ngOnInit = function () {
        this.user = Session_1.Session.AuthenticatedUser;
        if (this.user == null) {
            this.router.navigate(["/login"]);
        }
    };
    ProfileEditingComponent.prototype.validate = function () {
        var registerForm = document.getElementsByName('registerForm')[0];
        return registerForm.checkValidity();
    };
    ProfileEditingComponent.prototype.Save = function () {
        var _this = this;
        if (!this.validate()) {
            return true;
        }
        this.userService.UpdateUser(this.user).then(function (model) {
            if (model.IsSuccess) {
                Session_1.Session.AuthenticatedUser = _this.user;
                _this.router.navigate(_this.getRouterNavigate(_this.user));
            }
            else {
                ErrorHandler_1.ErrorHandler.Error(model.Message);
            }
        });
    };
    ProfileEditingComponent.prototype.back = function () {
        history.back();
    };
    ProfileEditingComponent = __decorate([
        core_1.Component({
            selector: "<interviewer-page></interviewer-page>",
            templateUrl: "app/Views/profile.editing.component.html",
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
    ], ProfileEditingComponent);
    return ProfileEditingComponent;
}());
exports.ProfileEditingComponent = ProfileEditingComponent;
//# sourceMappingURL=profile.editing.component.js.map