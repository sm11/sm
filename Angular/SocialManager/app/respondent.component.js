"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var Session_1 = require("./Library/Session");
// services 
var test_service_1 = require("./Services/test.service");
var RespondentComponent = (function () {
    function RespondentComponent(testService, router) {
        this.testService = testService;
        this.router = router;
        this.shownTest = null;
        this.SUGGESTED_MODE = 'suggested';
        this.PASSED_MODE = "passed";
        this.mode = this.SUGGESTED_MODE;
        var ROLE = "respondent";
        this.user = Session_1.Session.AuthenticatedUser;
        if (this.user == null || this.user.role != ROLE) {
            this.router.navigate(["/login"]);
        }
    }
    RespondentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.testService.GetTestsByRespondent(this.user.id).then(function (tests) {
            console.log(tests);
            _this.passedTests = tests.filter(function (test) { return test.IsPassed; });
            _this.suggestedTests = tests.filter(function (test) { return !test.IsPassed; });
        });
    };
    RespondentComponent.prototype.moveToPassTest = function (test) {
        if (test == null) {
            console.warn('try to assign group to null test');
            return;
        }
        sessionStorage.setItem('selectedTest', JSON.stringify(test));
        this.router.navigate(["/testPassing"]);
    };
    RespondentComponent.prototype.toggleFeedback = function (test) {
        console.log(test);
        if (this.shownTest != null && this.shownTest.TestId == test.TestId) {
            this.shownTest = null;
        }
        else {
            this.shownTest = test;
        }
    };
    RespondentComponent = __decorate([
        core_1.Component({
            selector: "<respondent-page></respondent-page>",
            templateUrl: "/app/Views/respondent.component.html",
            providers: [test_service_1.TestService]
        }), 
        __metadata('design:paramtypes', [test_service_1.TestService, router_1.Router])
    ], RespondentComponent);
    return RespondentComponent;
}());
exports.RespondentComponent = RespondentComponent;
//# sourceMappingURL=respondent.component.js.map