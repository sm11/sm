"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var user_service_1 = require("./Services/user.service");
var router_1 = require("@angular/router");
var Session_1 = require("./Library/Session");
var ErrorHandler_1 = require("./Library/ErrorHandler");
var LoginComponent = (function () {
    function LoginComponent(userService, router) {
        this.userService = userService;
        this.router = router;
    }
    LoginComponent.prototype.validate = function () {
        var loginForm = document.getElementsByName('loginForm')[0];
        if (loginForm.checkValidity()) {
            return true;
        }
        return false;
    };
    LoginComponent.prototype.getRouterNavigate = function (user) {
        if (user.role == 'creator') {
            return ["/createOrg"];
        }
        else if (user.role == 'interviewer') {
            return ['/interviewer'];
        }
        else if (user.role == 'respondent') {
            return ['/respondent'];
        }
    };
    LoginComponent.prototype.ngOnInit = function () {
        var user = Session_1.Session.AuthenticatedUser;
        if (user != null) {
            this.router.navigate(this.getRouterNavigate(user));
        }
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        if (!this.validate()) {
            return false;
        }
        console.log(this.Login);
        this.userService.Login(this.Login, this.Password).then(function (model) {
            if (model != null) {
                if (model.User.role == 'creator') {
                    _this.router.navigate(["/createOrg"]);
                }
                else if (model.User.role == 'interviewer') {
                    _this.router.navigate(['/interviewer']);
                }
                else if (model.User.role == 'respondent') {
                    _this.router.navigate(['/respondent']);
                }
                Session_1.Session.AuthenticatedUser = model.User;
            }
            else {
                ErrorHandler_1.ErrorHandler.Error("Invalid login or password");
            }
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            templateUrl: "/app/views/login.component.html",
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map