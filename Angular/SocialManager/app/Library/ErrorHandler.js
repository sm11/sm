"use strict";
var ErrorHandler = (function () {
    function ErrorHandler() {
    }
    ErrorHandler.RegisterCallback = function (callback) {
        this.errorLayoutCallback = callback;
    };
    ErrorHandler.RegisterHideCallback = function (callback) {
        this.hideErrorCallback = callback;
    };
    ErrorHandler.Error = function (message) {
        this.errorLayoutCallback(message);
        setTimeout(this.hideErrorCallback, 5 * 1000);
    };
    return ErrorHandler;
}());
exports.ErrorHandler = ErrorHandler;
//# sourceMappingURL=ErrorHandler.js.map