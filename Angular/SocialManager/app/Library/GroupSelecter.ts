import { Group } from "./../Models/Group";

export class GroupSelector {
    private static group : Group = null;
    private static callbacks : ((group: Group) => void)[] = [];
    private static groupCallbacks : ((groups: Group[]) => void)[] = [];

    public static get Group() { return GroupSelector.group; }
    public static set Group(value: Group) { 
        GroupSelector.group = value; 
        GroupSelector.callbacks.forEach(cb => cb(value));
    }

    public static RegisterCallback(callBack: (group: Group) => void) {
        GroupSelector.callbacks.push(callBack);
    }

    public static RegisterGroupArrCallback(callBack: (group: Group[]) => void) {
        GroupSelector.groupCallbacks.push(callBack);
    } 

    public static GroupArrChanged(value: Group[]) {
        GroupSelector.groupCallbacks.forEach(cb => cb(value));
    }
}