"use strict";
var GroupSelector = (function () {
    function GroupSelector() {
    }
    Object.defineProperty(GroupSelector, "Group", {
        get: function () { return GroupSelector.group; },
        set: function (value) {
            GroupSelector.group = value;
            GroupSelector.callbacks.forEach(function (cb) { return cb(value); });
        },
        enumerable: true,
        configurable: true
    });
    GroupSelector.RegisterCallback = function (callBack) {
        GroupSelector.callbacks.push(callBack);
    };
    GroupSelector.RegisterGroupArrCallback = function (callBack) {
        GroupSelector.groupCallbacks.push(callBack);
    };
    GroupSelector.GroupArrChanged = function (value) {
        GroupSelector.groupCallbacks.forEach(function (cb) { return cb(value); });
    };
    GroupSelector.group = null;
    GroupSelector.callbacks = [];
    GroupSelector.groupCallbacks = [];
    return GroupSelector;
}());
exports.GroupSelector = GroupSelector;
//# sourceMappingURL=GroupSelecter.js.map