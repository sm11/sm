export class ErrorHandler {
    private static errorLayoutCallback: (message: string) => void; 
    private static hideErrorCallback: () => void;

    public static RegisterCallback(callback: (message: string) => void) {
        this.errorLayoutCallback = callback;
    }

    public static RegisterHideCallback(callback: () => void) {
        this.hideErrorCallback = callback;
    }

    public static Error(message: string) {
        this.errorLayoutCallback(message);
        setTimeout(this.hideErrorCallback, 5 * 1000);
    }
}