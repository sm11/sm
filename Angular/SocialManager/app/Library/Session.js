"use strict";
var User_1 = require("./../Models/User");
var Session = (function () {
    function Session() {
    }
    Object.defineProperty(Session, "AuthenticatedUser", {
        get: function () {
            if (Session.user != null) {
                return Session.user;
            }
            return Session.restoreUser();
        },
        set: function (value) {
            Session.user = value;
            Session.saveUser();
            this.callback(value);
        },
        enumerable: true,
        configurable: true
    });
    Session.saveCallback = function (callback) {
        this.callback = callback;
    };
    Session.restoreUser = function () {
        var stData = localStorage.getItem(Session.key);
        var parsedData = JSON.parse(stData);
        if (parsedData != null) {
            return new User_1.User(parsedData.id, parsedData.email, parsedData.login, parsedData.password, parsedData.surname, parsedData.phone, parsedData.groupId, parsedData.role);
        }
        else {
            return null;
        }
    };
    Session.saveUser = function () {
        localStorage.setItem(Session.key, JSON.stringify(Session.user));
    };
    Session.user = null;
    Session.callback = null;
    Session.key = "sm-auth-key";
    return Session;
}());
exports.Session = Session;
//# sourceMappingURL=Session.js.map