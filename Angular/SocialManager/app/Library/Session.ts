import { User } from "./../Models/User";

export class Session {
    private static user = null;
    private static callback: (user:User) => void = null;

    public static get AuthenticatedUser() { 
        if (Session.user != null) {
            return Session.user; 
        }
        return Session.restoreUser();
    }

    public static set AuthenticatedUser(value :User) { 
        Session.user = value; 
        Session.saveUser();
        this.callback(value);
    }

    public static saveCallback(callback : (user:User) => void) {
        this.callback = callback;
    }

    private static key:string = "sm-auth-key";

    private static restoreUser() : User {
        let stData = localStorage.getItem(Session.key);
        let parsedData = JSON.parse(stData);

        if (parsedData != null) {
            return new User(parsedData.id, parsedData.email, parsedData.login, parsedData.password, 
                parsedData.surname, parsedData.phone, parsedData.groupId, parsedData.role);
        }
        else {
            return null;
        }
    }

    private static saveUser() {
        localStorage.setItem(Session.key, JSON.stringify(Session.user));
    }

}