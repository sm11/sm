import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector: '<help></help>',
    templateUrl: '/app/views/help.component.html',
})
export class HelpComponent implements OnInit {
    public interviewerVisible: boolean;
    public respondentVisible: boolean;
    public creatorVisible: boolean;
    ngOnInit() {
        this.interviewerVisible = false;
        this.respondentVisible = false;
        this.creatorVisible = false;
    }

    public changeVisible(user: string) {
        if (user == "interviewer") {
            this.interviewerVisible = !this.interviewerVisible;
        } else if (user == "respondent") {
            this.respondentVisible = !this.respondentVisible;
        } else if (user == "creator") {
            this.creatorVisible = !this.creatorVisible;
        }
    }

    public back() {
        history.back();
    }

}