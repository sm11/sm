import { Component, Input, OnInit } from "@angular/core";

//Models
import { Group } from "./Models/Group";

import { GroupSelector } from "./Library/GroupSelecter";

@Component({
    selector: 'groups',
    templateUrl: '/app/Views/group.list.component.html'
})
export class GroupListComponent implements OnInit {
    @Input() Groups: Group[];
    @Input() parentId:number;

    private levelGroups: Group[];

    private selectedGroup: Group = GroupSelector.Group;

    selectGroup(item:Group) {
        this.selectedGroup = item;
        GroupSelector.Group = item;
    }

    hasSubGroups(item: Group) {
        return this.Groups.findIndex(value => value.parentId == item.id) != -1;
    }

    getSubGroups(item: Group) {
        return this.Groups.filter(group => group.parentId == item.id);
    }

    private jQueryMarkup() {
        //$(".ul-dropfree").find("li:has(ul):has(li)").prepend('<div class="drop"></div>');
        $(".ul-dropfree div.drop").unbind('click');
        $(".ul-dropfree div.drop").click(function (e) {

            if ($(this).nextAll('div.group').nextAll("div").children('groups').children('ul').css('display') == 'none') {
                $(this).nextAll('div.group').nextAll("div").children('groups').children('ul').slideDown(400);
                $(this).css({ 'background-position': "-11px 0" });
            } else {
                $(this).nextAll('div.group').nextAll("div").children('groups').children('ul').slideUp(400);
                $(this).css({ 'background-position': "0 0" });
            }
        });

        $(".ul-dropfree").find("ul").slideUp(400).parents("li").children("div.drop").css({ 'background-position': "0 0" });
    }

    ngOnInit() {
        this.levelGroups = this.Groups.filter(group => group.parentId == this.parentId);
        GroupSelector.RegisterCallback(group => this.selectedGroup = group);
        GroupSelector.RegisterGroupArrCallback(groups =>
                 this.levelGroups = this.Groups.filter(group => group.parentId == this.parentId));
        setTimeout(this.jQueryMarkup, 1);

    }
}