import { Session } from "./Library/Session";
import { ErrorHandler } from "./Library/ErrorHandler";

import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

// models
import { User } from "./Models/User";

// services
import { UserService } from "./Services/user.service";

@Component({
    selector: "<interviewer-page></interviewer-page>",
    templateUrl: "app/Views/profile.editing.component.html",
    providers: [UserService]
})
export class ProfileEditingComponent implements  OnInit{
    private user: User;
    private errorMessage: string;
    public constructor(private userService: UserService, private router: Router) {  }

    private getRouterNavigate(user: User): string[] {
        if (user.role == 'creator') {
            return ["/createOrg"];
        }
        else if (user.role == 'interviewer') {
            return ['/interviewer'];
        }
        else if (user.role == 'respondent') {
            return ['/respondent'];
        }
    }

    ngOnInit() {
        this.user = Session.AuthenticatedUser;
        
        if (this.user == null) {
            this.router.navigate([ "/login" ]);
        }   
    }

    private validate() {
        var registerForm = <HTMLFormElement>document.getElementsByName('registerForm')[0];
        return registerForm.checkValidity();
    }

    public Save() {
        
        if (!this.validate()) {
            return true;
        }

        this.userService.UpdateUser(this.user).then(model => {
            if (model.IsSuccess) {
                Session.AuthenticatedUser=this.user;
                this.router.navigate(this.getRouterNavigate(this.user));
            } else {
                ErrorHandler.Error(model.Message);
            }
        });
    }

    public back() {
        history.back();
    }
}
