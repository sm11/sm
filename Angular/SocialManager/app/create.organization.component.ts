﻿import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

// models
import { User } from "./Models/User";
import { Group } from "./Models/Group";

import { Session } from "./Library/Session";

//services
import { GroupService } from "./Services/group.service";

//Library
import { ErrorHandler } from "./Library/ErrorHandler";
import { GroupSelector } from "./Library/GroupSelecter";

@Component({
    selector: 'create-org',
    templateUrl: "/app/Views/create.organization.html",
    providers: [GroupService]
})
export class CreateOrgComponent implements OnInit {
    addingGroup: Group = null;
    deletingGroup: Group = null;
    editingGroup: Group = null;
    editingGroupName: string = null;

    groups: Group[] = [];
    user: User = null;
    selectedGroup: Group = null;

    // error validating
    private addError: string = null;
    private editError: string = null;
    private deleteError: string = null;

    // constants
    private MAX_GROUP_LENGTH = 25;

    private isLoaded = false;

    private excelExtensions = ['.xls', '.xlsx', '.xlsm'];

    private excelPath: string = "http://sm.somee.com/Files/test.xlsx";

    public constructor(private groupService: GroupService, private router: Router) { }

    private jQueryMarkup() {
        //$(".ul-dropfree").find("li:has(ul):has(li)").prepend('<div class="drop"></div>');

        $(".ul-dropfree div.drop").click(function (e) {

            if ($(this).nextAll("ul").css('display') == 'none') {
                $(this).nextAll("ul").slideDown(400);
                $(this).css({ 'background-position': "-11px 0" });
            } else {
                $(this).nextAll("ul").slideUp(400);
                $(this).css({ 'background-position': "0 0" });
            }
        });

        $(".ul-dropfree").find("ul").slideUp(400).parents("li").children("div.drop").css({ 'background-position': "0 0" });

        $(".usersTable tr.person").click(function () {
            $(".usersTable").find("tr.person").css({ 'background-color': "white" });
            $(this).css({ 'background-color': "#dff0b3" });
        });
    }

    private getRootGroups() {
        return this.groups.filter(group => group.parentId == null);
    }

    private loadGroups() {

        this.groupService.GetGroupsByInterviewer(this.user.id).then(groups => {
            if (groups == null) {
                this.groups = null;
                return;
            }

            this.groups = groups.map(gr => {

                let intervArr = gr.users.filter(u => u.role == 'interviewer');
                if (intervArr.length > 0) {
                    gr.interviewer = intervArr[0];
                }

                gr.users = gr.users.filter(user => user.role == 'respondent');
                return gr;
            });

            this.isLoaded = true;

            console.log('loading groups');
            console.log(this.groups);
        });

        GroupSelector.RegisterCallback(group => this.selectedGroup = group);
    }

    ngOnInit() {
        this.user = Session.AuthenticatedUser;

        if (this.user.role != 'creator') {
            if (this.user.role == 'interviewer') {
                this.router.navigate(['/interviewer']);
            }
            else if (this.user.role == 'respondent') {
                this.router.navigate(['/respondent']);
            }
        }

        this.loadGroups();

        GroupSelector.RegisterCallback(group => this.selectedGroup = group);
        GroupSelector.Group = null;
    }

    checkGroup(group: Group): void {
        this.selectedGroup = group;
    }

    createGroup(): void {
        console.log(this.selectedGroup);

        if (this.addingGroup.name.length > this.MAX_GROUP_LENGTH) {
            ErrorHandler.Error('Too long name for the group');
            return;
        }

        this.addingGroup.parentId = this.selectedGroup != null ? this.selectedGroup.id : null;
        this.addingGroup.creatorId = this.user.id;

        let groupy = this.groups.filter(group => (group.name==this.addingGroup.name)&&(group.parentId==this.addingGroup.parentId));
        if(groupy.length>0){
            console.warn('Group with this name already exists');
            ErrorHandler.Error("Group with this name already exists");
            this.addingGroup=null;
        }  

        this.groupService.CreateGroup(this.addingGroup).then(group => {
            if (group == null) {
                ErrorHandler.Error("Internal server error");
            }
            else {
                this.groups.push(group);
                GroupSelector.GroupArrChanged(this.groups);
                this.addingGroup = null;
                setTimeout(() => {
                    $(".ul-dropfree").find("ul").slideUp(400).parents("li").children("div.drop")
                        .css({ 'background-position': "0 0" });
                    //$(".ul-dropfree").find("li:has(li)").prepend('<div class="drop"></div>');

                    $(".ul-dropfree div.drop").unbind('click');
                    $(".ul-dropfree div.drop").css({ 'background-position': "0 0" });

                    $(".ul-dropfree div.drop").click(function (e) {

                        if ($(this).nextAll('div.group').nextAll("div").children('groups').children('ul').css('display') == 'none') {
                            $(this).nextAll('div.group').nextAll("div").children('groups').children('ul').slideDown(400);
                            $(this).css({ 'background-position': "-11px 0" });
                        } else {
                            $(this).nextAll('div.group').nextAll("div").children('groups').children('ul').slideUp(400);
                            $(this).css({ 'background-position': "0 0" });
                        }
                    });

                }, 1);
            }
        });
    }

    addGroup(): void {

        if (this.selectedGroup == null && this.groups != null && this.groups.length > 0) {
            console.warn('cannot add group');
            return;
        }
        this.addingGroup = new Group();
    }

    updateGroup() {
        if (this.editingGroupName.length > this.MAX_GROUP_LENGTH) {
            ErrorHandler.Error('Too long name for the group');
            return;
        }

        this.editingGroup.creatorId = this.user.id;
        this.editingGroup.name = this.editingGroupName;

        this.groupService.UpdateGroup(this.editingGroup).then(res => {
            if (!res.IsSuccess) {
                ErrorHandler.Error(res.Message);
            }
            else {
                //this.groups.push(this.addingGroup);
                this.editingGroup = null;
            }
        });
    }

    deleteGroup(): void {
        this.groupService.DeleteGroup(this.deletingGroup.id).then(res => {

            if (!res) {
                ErrorHandler.Error("Not found entity. Possibly it's already deleted.");
            }
            else {
                this.deleteGroupFromArr(this.deletingGroup);
                this.deletingGroup = null;
                GroupSelector.GroupArrChanged(this.groups);
            }
        });
    }

    deleteGroupFromArr(group: Group) {
        this.groups.filter(group => group.parentId == group.id).forEach(group => this.deleteGroupFromArr(group));
        let index = this.groups.findIndex(gr => gr.id == this.deletingGroup.id);
        this.groups.splice(index, 1);
    }

    uploadFile() {
        $("#fileInput").click();

        $("#fileInput").change(ev => {
            
            let file = (<HTMLInputElement>document.getElementById('fileInput')).files[0];

            if (this.excelExtensions.findIndex(ext => file.name.endsWith(ext)) == -1) {
                ErrorHandler.Error('Invalid file extension');
                return;
            }

            this.isLoaded = false;

            this.groupService.UploadFile(this.selectedGroup.id, file).then(response => {
                if (!response.IsSuccess) {
                    ErrorHandler.Error(response.Message);
                    this.isLoaded = true;
                }
                else {
                    GroupSelector.Group = null;
                    this.loadGroups();
                }
            });

        });
    }

    downloadFile() {
        window.open(this.excelPath);
    }

    openEditModal(group: Group) {
        this.editingGroup = group;
        this.editingGroupName = group.name;
    }
}