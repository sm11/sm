"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var User_1 = require("./Models/User");
var user_service_1 = require("./Services/user.service");
var router_1 = require("@angular/router");
var Session_1 = require("./Library/Session");
var ErrorHandler_1 = require("./Library/ErrorHandler");
var RegisterComponent = (function () {
    function RegisterComponent(userService, router) {
        this.userService = userService;
        this.router = router;
        this.user = new User_1.User();
    }
    RegisterComponent.prototype.getRouterNavigate = function (user) {
        if (user.role == 'creator') {
            return ["/createOrg"];
        }
        else if (user.role == 'interviewer') {
            return ['/interviewer'];
        }
        else if (user.role == 'respondent') {
            return ['/respondent'];
        }
    };
    RegisterComponent.prototype.ngOnInit = function () {
        var user = Session_1.Session.AuthenticatedUser;
        if (user != null) {
            this.router.navigate(this.getRouterNavigate(user));
        }
    };
    RegisterComponent.prototype.validate = function () {
        var registerForm = document.getElementsByName('registerForm')[0];
        return registerForm.checkValidity();
    };
    RegisterComponent.prototype.register = function () {
        var _this = this;
        if (!this.validate()) {
            return false;
        }
        this.user.role = 'creator';
        this.userService.Register(this.user).then(function (model) {
            console.log(model);
            if (model.CUDResult.IsSuccess) {
                Session_1.Session.AuthenticatedUser = model.User;
                // at the moment register is available just for creators
                _this.router.navigate(["/createOrg"]);
            }
            else {
                ErrorHandler_1.ErrorHandler.Error(model.CUDResult.Message);
            }
        });
    };
    RegisterComponent = __decorate([
        core_1.Component({
            selector: "register",
            templateUrl: "/app/views/register.component.html",
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
    ], RegisterComponent);
    return RegisterComponent;
}());
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=register.component.js.map