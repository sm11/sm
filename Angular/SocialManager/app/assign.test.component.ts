import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

//models
import { Test } from "./Models/Test";
import { Group } from "./Models/Group";
import { TestGroup } from "./Models/TestGroup";

//service
import { TestService } from "./Services/test.service";
import { GroupService } from "./Services/group.service";
import { TestGroupService } from "./Services/test.group.service";

// library
import { ErrorHandler } from "./Library/ErrorHandler";
import { GroupSelector } from "./Library/GroupSelecter";

@Component({
    selector: '<assign-test> </assign-test>',
    templateUrl: '/app/views/assign.test.component.html',
    providers: [TestService, GroupService, TestGroupService]
})
export class AssignTestComponent implements OnInit {

    private test: Test = new Test();

    private groups: Group[];
    private testGroups: TestGroup[];
    private selectedGroup: TestGroup = null;
    private isDateEdited: boolean = false;

    // loading
    private isLoaded: boolean = false;
    private isGroupsLoaded: boolean = false;
    private isTestGroupsLoaded: boolean = false;

    public constructor(private testService: TestService, private router: Router, private groupService: GroupService,
        private testGroupService: TestGroupService) { }

    private jQueryMarkup() {
        $(".ul-dropfree").find("li:has(li)").prepend('<div class="drop"></div>');

        $(".ul-dropfree div.drop").click(function () {
            if ($(this).nextAll("ul").css('display') == 'none') {
                $(this).nextAll("ul").slideDown(400);
                $(this).css({ 'background-position': "-11px 0" });
            } else {
                $(this).nextAll("ul").slideUp(400);
                $(this).css({ 'background-position': "0 0" });
            }
        });

        $(".ul-dropfree").find("ul").slideUp(400).parents("li").children("div.drop").css({ 'background-position': "0 0" });

        $(".header div.dropdown").click(function () {
            if ($(".header div.logoutbox").css('display') == 'none') {
                $(".logoutbox").css({ 'display': "block" });
            }
            else {
                $(".logoutbox").css({ 'display': "none" });
            }
        });

        $(window).resize(function () {
            if ($(".header div.dropdown").css('display') == 'none') {
                $(".logoutbox").css({ 'display': "block" });
            }
            else {
                $(".logoutbox").css({ 'display': "none" });
            }
        });

        $(".usersTable tr.person").click(function () {
            $(".usersTable").find("tr.person").css({ 'background-color': "white" });
            $(this).css({ 'background-color': "#dff0b3" });
        });

        $("td.q").click(function () {
            $(".resultTable").find("td.q").css({ 'background': "none" });
            $(this).css({ 'background': "linear-gradient(to right, #dbf29d, #fff)" });
        });
    }

    // date binding 
    set Expires(e:string) {
        
        if (e == "") {
            this.selectedGroup.Expires = null;
            return;
        }

        let date = e.split('-');
        let d = new Date(Date.UTC(+date[0], +date[1] - 1, +date[2]));
        if (this.selectedGroup.Expires != null) {
            this.selectedGroup.Expires.setFullYear(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate());
        }
        else {
            this.selectedGroup.Expires = new Date(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate());
        }
    }
    get Expires() {
        if (this.selectedGroup == null || this.selectedGroup.Expires == null) {
            return "";
        }
        return this.selectedGroup.Expires.toISOString().substring(0, 10);
    }

    ngOnInit() {
        let jsonTest = localStorage.getItem("selectedTest");

        let parsedTest = JSON.parse(jsonTest);
        this.test = new Test(parsedTest.testId, parsedTest.name, parsedTest.interviewerId)

        if (this.test == null) {
            this.router.navigate(['/interviewers']);
        }

        this.groupService.GetGroups().then(groups => {
            this.groups = groups;
            this.isGroupsLoaded = true;

            if (this.isTestGroupsLoaded) {
                this.isLoaded = true;
                setTimeout(this.jQueryMarkup, 1);
            } 
        });

        this.testGroupService.getByTestId(this.test.TestId).then(tGroups => {
            this.testGroups = tGroups;
            this.isTestGroupsLoaded = true;

            if (this.isGroupsLoaded) {
                this.isLoaded = true;
                setTimeout(this.jQueryMarkup, 1);
            } 
        });

        GroupSelector.RegisterCallback(group => {
            
            let tGroup = this.testGroups.find(tg => tg.GroupId == group.id);
            
            if (tGroup != null) {
                this.selectedGroup = tGroup;
            }
            else {
                let date = new Date();
                date.setMonth(date.getMonth() + 1);
                this.selectedGroup = new TestGroup(this.test.TestId, group.id, date, this.test.Name, group.name);
            }
        });

    }

    public goBack() {
        history.back();
    }

    public containsTestGroup(groupId: number) {
        let index = this.testGroups.findIndex(val => val.GroupId == groupId);
        return index != -1;
    }

    public getTestGroup(testId: number, groupId:number) {
        return this.testGroups.find(val => val.GroupId == groupId && val.TestId == testId);
    }

    public checkGroup(group: Group) {
        let date = new Date();
        date.setMonth(date.getMonth() + 1);

        let curTest = this.getTestGroup(this.test.TestId, group.id);
        date = curTest != undefined ? curTest.Expires : date;
        let testGroup = new TestGroup(this.test.TestId, group.id, date, this.test.Name, group.name);

        if (this.selectedGroup != null && this.selectedGroup.GroupId == testGroup.GroupId
            && this.selectedGroup.TestId == testGroup.TestId) {

            this.selectedGroup = null;
            return;
        }

        this.selectedGroup = testGroup;
        this.isDateEdited = false;

    }

    public selectTGroup(tGroup: TestGroup) {
        this.selectedGroup = tGroup;
    }

    public assignTest() {
        
        if (this.selectedGroup == null) {
            return;
        }

        if (this.selectedGroup.Expires == null) {
            ErrorHandler.Error("Specify final date of test passing");
            return;
        }

        let index = this.testGroups.findIndex(tg => tg.GroupId == this.selectedGroup.GroupId
            && tg.TestId == this.test.TestId);

        if (index == -1) {
            this.testGroupService.createTestGroup(this.selectedGroup).then(res => {
                if (res.IsSuccess) {
                    this.testGroups.push(this.selectedGroup);
                }
                else {
                    ErrorHandler.Error(res.Message);
                }
            });
        }
        else if (this.isDateEdited) {
            this.testGroupService.updateTestGroup(this.selectedGroup).then(res => {
                if (res.IsSuccess) {
                }
                else {
                    ErrorHandler.Error(res.Message);
                }
            });
            
        }
        else {
            this.testGroupService.deleteTestGroup(this.selectedGroup).then(res => {
                if (res.IsSuccess) {
                    this.testGroups.splice(index, 1);
                }
                else {
                    ErrorHandler.Error(res.Message);
                }
            });
        }

    }
}