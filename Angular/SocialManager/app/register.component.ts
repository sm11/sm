import { Component, OnInit } from "@angular/core";

import { User } from "./Models/User";

import { UserService } from "./Services/user.service";

import { Router } from "@angular/router";
import { Session } from "./Library/Session";
import { ErrorHandler } from "./Library/ErrorHandler";

@Component({
    selector: "register",
    templateUrl:"/app/views/register.component.html",
    providers: [ UserService ]
})
export class RegisterComponent implements OnInit {

    private user: User = new User();

    public constructor(private userService: UserService, private router: Router) {
    }

    private getRouterNavigate(user: User): string[] {
        if (user.role == 'creator') {
            return ["/createOrg"];
        }
        else if (user.role == 'interviewer') {
            return ['/interviewer'];
        }
        else if (user.role == 'respondent') {
            return ['/respondent'];
        }
    }

    ngOnInit() {
        let user = Session.AuthenticatedUser;

        if (user != null) {
            this.router.navigate(this.getRouterNavigate(user));
        }
    }

    private validate() {
        var registerForm = <HTMLFormElement>document.getElementsByName('registerForm')[0];

        return registerForm.checkValidity();
    }

    public register() {

        if (!this.validate()) {
            return false;
        }
        
        this.user.role = 'creator';

        this.userService.Register(this.user).then(model => {
            console.log(model);
            if (model.CUDResult.IsSuccess) {
                Session.AuthenticatedUser = model.User;
                // at the moment register is available just for creators
                this.router.navigate(["/createOrg"]);
            }
            else {
                ErrorHandler.Error(model.CUDResult.Message);
            }
        });
    }
}