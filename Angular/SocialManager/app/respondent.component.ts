import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { Session } from "./Library/Session";

// models
import { Test } from "./Models/Test";
import { RespondentTest } from "./Models/RespondentTest";
import { User } from "./Models/User";

// services 
import { TestService } from "./Services/test.service";

@Component({
    selector:"<respondent-page></respondent-page>",
    templateUrl: "/app/Views/respondent.component.html",
    providers: [ TestService ]
})
export class RespondentComponent implements OnInit {
    
    private tests: Test[];
    private user: User;

    private passedTests : Test[];
    private suggestedTests: Test[];
    private shownTest: Test = null;

    private SUGGESTED_MODE = 'suggested';
    private PASSED_MODE = "passed";

    private mode = this.SUGGESTED_MODE;

    public constructor(private testService: TestService, private router : Router) { 
        const ROLE = "respondent";

        this.user = Session.AuthenticatedUser;

        if (this.user == null || this.user.role != ROLE) {
            this.router.navigate([ "/login" ]);
        }
    }


    ngOnInit() {
        this.testService.GetTestsByRespondent(this.user.id).then(tests => {
            console.log(tests);
            this.passedTests = tests.filter(test => test.IsPassed);
            this.suggestedTests = tests.filter(test => !test.IsPassed);
        });
    }

    public moveToPassTest(test: Test) {
        
        if (test == null) {
            console.warn('try to assign group to null test');
            return;
        }

        sessionStorage.setItem('selectedTest', JSON.stringify(test));
        this.router.navigate([ "/testPassing" ]);
    }

    public toggleFeedback(test: Test) {
        console.log(test);

        if (this.shownTest != null && this.shownTest.TestId == test.TestId) {
            this.shownTest = null;
        }
        else {
            this.shownTest = test;
        }
    }
}