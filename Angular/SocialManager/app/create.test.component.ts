import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { ErrorHandler } from "./Library/ErrorHandler";
import { Session } from "./Library/Session";

//models
import { User } from "./Models/User"
import { Test } from "./Models/Test"
import { TestViewModel } from "./Models/TestViewModel"
import { QuestionViewModel } from "./Models/QuestionViewModel";
import { AnswerViewModel } from "./Models/AnswerViewModel";
import { QuestionType } from "./Models/Enums";

//services
import { EditTestService } from "./Services/edit.test.service"

@Component({
    selector: "<create-test-page></create-test-page>",
    templateUrl: "app/Views/create.test.component.html",
    providers: [EditTestService]
})

export class CreateTestComponent implements OnInit {

    //Pay attention!!!
    //New questions and answers have negative ids!!!

    private user: User;
    private testViewModel: TestViewModel;
    private currentQuestionId: number;
    private deletingQID: number;//for detect when pop-up window is open
    private mutq: boolean;//for check access to critical zone
    private muta: boolean;//for check access to critical zone

    public constructor(private editTestService: EditTestService, private router: Router) {

    }

    ngOnInit() {
        this.user = new User();
        this.user = Session.AuthenticatedUser;
        this.testViewModel = new TestViewModel();
        this.deletingQID = null;
        this.mutq = false;
        this.muta = false;

        if (sessionStorage.getItem('selectedTest') != null) {
            let parsedTest = JSON.parse(sessionStorage.getItem('selectedTest'));
            sessionStorage.removeItem("selectedTest");
            this.editTestService.getTest(parsedTest.testId).then(model => {
                this.testViewModel = new TestViewModel(model.TestId, model.Name, model.InterviewerId,
                    model.Questions.map(q => new QuestionViewModel(q.QuestionId, q.Text, q.TestId, q.Type, q.Answers.map(a =>
                        new AnswerViewModel(a.AnswerId, a.Text)))));
            });
            this.currentQuestionId = -1;
        } else {
            this.testViewModel.testId = -1;
            this.testViewModel.interviewerId = this.user.id;
            this.currentQuestionId = -3;

            //add two blank questions for template
            this.testViewModel.questions.push(new QuestionViewModel(-1, "", this.testViewModel.testId, QuestionType.SingleAnswer, new Array<AnswerViewModel>()));
            this.testViewModel.questions[0].answers.push(new AnswerViewModel(-1, ""));
            this.testViewModel.questions[0].answers.push(new AnswerViewModel(-2, ""));

            this.testViewModel.questions.push(new QuestionViewModel(-2, "", this.testViewModel.testId, QuestionType.SingleAnswer, new Array<AnswerViewModel>()));
            this.testViewModel.questions[1].answers.push(new AnswerViewModel(-1, ""));
            this.testViewModel.questions[1].answers.push(new AnswerViewModel(-2, ""));
        }
    }

    public addNewQuestion() {
        this.testViewModel.questions.push(new QuestionViewModel(this.currentQuestionId, "", this.testViewModel.testId,
            QuestionType.SingleAnswer, new Array<AnswerViewModel>()));
        this.testViewModel.questions[this.testViewModel.questions.length - 1].answers.push(new AnswerViewModel(-1, ""));
        this.testViewModel.questions[this.testViewModel.questions.length - 1].answers.push(new AnswerViewModel(-2, ""));
        this.currentQuestionId--;
    }

    public getQuestionById(Qid: number): QuestionViewModel {
        for (var i = 0; i < this.testViewModel.questions.length; i++) {
            if (this.testViewModel.questions[i].questionId == Qid) {
                return this.testViewModel.questions[i];
            }
        }
        return null;
    }

    public getAnswerById(Qid: number, Aid: number): AnswerViewModel {
        for (var j = 0; j < this.getQuestionById(Qid).answers.length; j++) {
            if (this.getQuestionById(Qid).answers[j].answerId == Aid) {
                return this.getQuestionById(Qid).answers[j];
            }
        }
        return null;
    }

    public addNewAnswer(Qid: number) {
        this.getQuestionById(Qid).answers.push(
            new AnswerViewModel(-this.getQuestionById(Qid).answers.length - 1, ""));
    }

    public saveQText(Qid: number, text: string) {
        this.getQuestionById(Qid).text = text;
    }

    public saveQAnswer(Qid: number, Aid: number, text: string) {
        if (this.getAnswerById(Qid, Aid) != null) {
            this.getAnswerById(Qid, Aid).text = text;
        } else {
            this.getQuestionById(Qid).answers.push(new AnswerViewModel(-1, text));
        }
    }

    public saveQType(Qid: number, type: string) {
        if (type == "radio") {
            this.getQuestionById(Qid).type = QuestionType.SingleAnswer;
        } else {
            this.getQuestionById(Qid).type = QuestionType.MultipleAnswer;
        }
    }

    public deleteQuestion() {
        this.testViewModel.questions.splice(
            this.testViewModel.questions.indexOf(
                this.getQuestionById(this.deletingQID)), 1);
        this.deletingQID = null;
    }

    public deleteAnswer(Qid: number, Aid: number) {
        this.getQuestionById(Qid).answers.splice(
            this.getQuestionById(Qid).answers.indexOf(
                this.getAnswerById(Qid, Aid)), 1);
    }

    public isUniqueQuestion(question: QuestionViewModel): boolean {
        this.mutq = false;
        console.log(this.testViewModel);
        for (let q of this.testViewModel.questions) {
            //console.log(q.text);
            //console.log(question.text);
            if (question.text === q.text) {
                console.log("into if");
                console.log(this.mutq);
                if (this.mutq) {
                    ErrorHandler.Error("Question  \"" + q.text + "\" is duplicated.");
                    this.mutq = false;
                    return false;
                }
                this.mutq = true;
            }
            if (!this.isUniqueAnswers(q)) {
                return false;
            }
        }
        return true;
    }

    public isUniqueAnswers(question: QuestionViewModel): boolean {
        this.muta = false;
        for (let a1 of question.answers) {
            for (let a2 of question.answers) {
                if (a1.text === a2.text) {
                    if (this.muta) {
                        ErrorHandler.Error("Answer  \"" + a1.text + "\" is duplicated.");
                        this.muta = false;
                        return false;
                    }
                    this.muta = true;
                }
            }
            this.muta = false;
            return true;
        }
    }

    public isValidate(): boolean {
        if (this.testViewModel.questions.length < 2) {
            ErrorHandler.Error("Test must consists at least 2 questions");
            return false;
        }
        for (let q of this.testViewModel.questions) {
            if (q.answers.length < 2) {
                ErrorHandler.Error("Question  \"" + q.text + "\" must consists at least 2 answers");
                return false;
            }
            if (!this.isUniqueQuestion(q)) {
                return false;
            }
        }

        return true;
    }

    public save() {
        if (this.isValidate()) {
            if (this.testViewModel.testId == -1) {
                this.editTestService.saveTest(this.testViewModel).then(model => {
                    if (model.IsSuccess) {
                        this.router.navigate(["/interviewer"]);
                    } else {
                        ErrorHandler.Error(model.Message);
                    }
                });
            }
            else {
                this.editTestService.updateTest(this.testViewModel).then(model => {
                    if (model.IsSuccess) {
                        this.router.navigate(["/interviewer"]);
                    } else {
                        ErrorHandler.Error(model.Message);
                    }
                });
            }
        }
    }

    public confirmDelete(Qid: number) {
        this.deletingQID = Qid;
    }

    public back() {
        history.back();
    }

}
