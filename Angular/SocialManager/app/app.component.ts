import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

//models
import { User } from "./Models/User";

// services
import { UserService } from "./Services/user.service";

//library
import { Session } from "./Library/Session";
import { ErrorHandler } from "./Library/ErrorHandler";

@Component({
    selector: 'sm-app',
    templateUrl: `/app/Views/layout.html`,
    providers: [UserService]
})
export class AppComponent implements OnInit {
    private user: User = null;

    private errorMessage: string = null;

    constructor(private router: Router) { }

    private jQueryMarkUp() {
        $(".header div.dropdown").click(function () {
            if ($(".header div.logoutbox").css('display') == 'none') {
                $(".logoutbox").css({ 'display': "block" });
            }
            else {
                $(".logoutbox").css({ 'display': "none" });
            }
        });
    }

    ngOnInit() {
        this.user = Session.AuthenticatedUser;
        Session.saveCallback(user => this.user = user);

        ErrorHandler.RegisterCallback(message => this.errorMessage = message);
        ErrorHandler.RegisterHideCallback(() => this.errorMessage = null);

        this.jQueryMarkUp();
    }

    LogOut() {
        Session.AuthenticatedUser = null;
        this.router.navigate(['/login']);
    }

    ProfEdit() {
        this.router.navigate(['/profile-editing']);
    }

    help() {
        this.router.navigate(['/help']);
    }

    isHelpPage() : boolean {
        return (window.location.toString() == "http://localhost:3000/help");
    }
};