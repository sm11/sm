import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { Test } from "./Models/Test";
import { QuestionResultViewModel } from "./Models/QuestionResultViewModel";
import { GroupBaseModel } from "./Models/GroupBaseModel";
import { TestViewModel } from "./Models/TestViewModel";
import { QuestionViewModel } from "./Models/QuestionViewModel";
import { Feedback } from "./Models/Feedback";

import { TestQuestionService } from "./Services/test.question.service";
import { TestGroupService } from "./Services/test.group.service";
import { TestPassingService } from "./Services/test.passing.service";
import { FeedbackService } from "./Services/feedback.service";

import { ErrorHandler } from "./Library/ErrorHandler";

@Component({
    selector: 'report',
    templateUrl: '/app/Views/report.component.html',
    providers: [TestQuestionService, TestGroupService, TestPassingService, FeedbackService]
})
export class ReportComponent {

    private test: Test;
    private testModel: TestViewModel = null;

    private questionResults: QuestionResultViewModel[] = null;
    private selectedQuestion: QuestionViewModel = null;

    private groups: GroupBaseModel[] = [];
    private selectedGroupName: string;

    private maxAnswers: number = 0;

    private feedback: Feedback = new Feedback();
    private fbExists: boolean = true;

    constructor(private router: Router, private testQuestionService: TestQuestionService,
        private testGroupService: TestGroupService, private TestPassingService: TestPassingService,
        private feedbackService: FeedbackService) { }

    getPercent(question: string, answer: string) {
        if (this.questionResults == null || this.selectedQuestion == null || this.selectedQuestion.text != question) {
            return "";
        }
        let qResult = this.questionResults.find(res => res.QuestionText == question && res.AnswerText == answer);
        return qResult != null ? qResult.Percent.toString() : '-';
    }

    getArray(length: number) {
        if (length <= 0)  { return []; }
        
        let arr = [];
        arr.length = length;
        return arr;
    }

    getSelectedGroupId() {
        let group = this.groups.filter(g => g.Name == this.selectedGroupName)[0];

        if (group != null) {
            return group.Id;
        }

        return -1;
    }

    getSelectedGroup(groupName) {
        return this.groups.filter(g => g.Name == groupName)[0];
    }

    private jQueryMarkup() {
        console.log("$ markup");
        $(".recall").click(function () {
            if ($(this).css('height') == '65px') {
                $(this).css({ 'height': "auto" });
            }
            else {
                $(this).css({ 'height': "65px" });
            }
        });

        $("td.q").click(function () {
            $(".resultTable").find("td.q").css({ 'background': "none" });
            $(this).css({ 'background': "linear-gradient(to right, #dbf29d, #fff)" });
        });

    }

    ngOnInit() {
        let jsonTest = localStorage.getItem("selectedTest");

        let parsedTest = JSON.parse(jsonTest);
        this.test = new Test(parsedTest.testId, parsedTest.name, parsedTest.interviewerId)

        if (this.test == null) {
            this.router.navigate(['/interviewers']);
        }

        this.TestPassingService.getTestParsed(this.test.TestId).then(test => {
            this.testModel = test;
            let questions = this.testModel.questions;
            let maxAnswers = 0;

            for (let i = 0; i < questions.length; i++) {
                if (maxAnswers < questions[0].answers.length) {
                    maxAnswers = questions[0].answers.length;
                }
            }

            this.maxAnswers = maxAnswers;
        });

        this.testGroupService.getFinalGroupsByTestId(this.test.TestId).then(groups => {
            this.groups = groups != null ? groups.filter(g => g != null) : [];
            setTimeout(this.jQueryMarkup, 1);
            if (this.groups.length > 0) {
                this.selectedGroupName = this.groups[0].Name;
                this.testQuestionService.GetQuestionResults(this.test.TestId, this.groups[0].Id)
                    .then(results => {
                        this.questionResults = results;
                        setTimeout(this.jQueryMarkup, 1);
                    });

                this.feedbackService.getFeedbackById(this.test.TestId, this.groups[0].Id)
                    .then(fb => {
                        if (fb == null) {
                            this.fbExists = false;
                            this.feedback = new Feedback();
                        }
                        else {
                            this.fbExists = true;
                            this.feedback = fb;
                        }
                        setTimeout(this.jQueryMarkup, 1);
                    });
            }
        });
    }

    groupChanged(groupName: string) {

        setTimeout(this.jQueryMarkup, 1);

        let group = this.groups.find(g => g.Name == groupName);

        if (group == null) {
            throw new Error("invalid selected group name");
        }

        this.testQuestionService.GetQuestionResults(this.test.TestId, group.Id)
            .then(results => {
                this.questionResults = results;
            });

        this.feedbackService.getFeedbackById(this.test.TestId, this.getSelectedGroup(groupName).Id)
            .then(fb => {
                if (fb == null) {
                    this.fbExists = false;
                    this.feedback = new Feedback();
                }
                else {
                    this.fbExists = true;
                    this.feedback = fb;
                }
            });
    }

    SendFeedback() {
        this.feedback.TestId = this.test.TestId;
        this.feedback.GroupId = this.getSelectedGroupId();

        if (this.fbExists) {
            this.feedbackService.updateFeedback(this.feedback).then(res => {
                if (!res.IsSuccess) {
                    ErrorHandler.Error("Invalid user data.");
                }
                else {
                    this.fbExists = true;
                }
            });
        }
        else {
            this.feedbackService.createFeedback(this.feedback).then(res => {
                if (!res.IsSuccess) {
                    ErrorHandler.Error("Creating feedback is not available. Please try later.");
                }
                else {
                    this.fbExists = true;
                }
            })
        }
    }


}

class QuestionReportViewModel {
    public QuestionText: string = null;

    public Percentages: number[] = [];
    public Answers: string[] = [];

    constructor(question: string) {
        this.QuestionText = question;
    }
}