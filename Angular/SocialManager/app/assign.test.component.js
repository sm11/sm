"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
//models
var Test_1 = require("./Models/Test");
var TestGroup_1 = require("./Models/TestGroup");
//service
var test_service_1 = require("./Services/test.service");
var group_service_1 = require("./Services/group.service");
var test_group_service_1 = require("./Services/test.group.service");
// library
var ErrorHandler_1 = require("./Library/ErrorHandler");
var GroupSelecter_1 = require("./Library/GroupSelecter");
var AssignTestComponent = (function () {
    function AssignTestComponent(testService, router, groupService, testGroupService) {
        this.testService = testService;
        this.router = router;
        this.groupService = groupService;
        this.testGroupService = testGroupService;
        this.test = new Test_1.Test();
        this.selectedGroup = null;
        this.isDateEdited = false;
        // loading
        this.isLoaded = false;
        this.isGroupsLoaded = false;
        this.isTestGroupsLoaded = false;
    }
    AssignTestComponent.prototype.jQueryMarkup = function () {
        $(".ul-dropfree").find("li:has(li)").prepend('<div class="drop"></div>');
        $(".ul-dropfree div.drop").click(function () {
            if ($(this).nextAll("ul").css('display') == 'none') {
                $(this).nextAll("ul").slideDown(400);
                $(this).css({ 'background-position': "-11px 0" });
            }
            else {
                $(this).nextAll("ul").slideUp(400);
                $(this).css({ 'background-position': "0 0" });
            }
        });
        $(".ul-dropfree").find("ul").slideUp(400).parents("li").children("div.drop").css({ 'background-position': "0 0" });
        $(".header div.dropdown").click(function () {
            if ($(".header div.logoutbox").css('display') == 'none') {
                $(".logoutbox").css({ 'display': "block" });
            }
            else {
                $(".logoutbox").css({ 'display': "none" });
            }
        });
        $(window).resize(function () {
            if ($(".header div.dropdown").css('display') == 'none') {
                $(".logoutbox").css({ 'display': "block" });
            }
            else {
                $(".logoutbox").css({ 'display': "none" });
            }
        });
        $(".usersTable tr.person").click(function () {
            $(".usersTable").find("tr.person").css({ 'background-color': "white" });
            $(this).css({ 'background-color': "#dff0b3" });
        });
        $("td.q").click(function () {
            $(".resultTable").find("td.q").css({ 'background': "none" });
            $(this).css({ 'background': "linear-gradient(to right, #dbf29d, #fff)" });
        });
    };
    Object.defineProperty(AssignTestComponent.prototype, "Expires", {
        get: function () {
            if (this.selectedGroup == null || this.selectedGroup.Expires == null) {
                return "";
            }
            return this.selectedGroup.Expires.toISOString().substring(0, 10);
        },
        // date binding 
        set: function (e) {
            if (e == "") {
                this.selectedGroup.Expires = null;
                return;
            }
            var date = e.split('-');
            var d = new Date(Date.UTC(+date[0], +date[1] - 1, +date[2]));
            if (this.selectedGroup.Expires != null) {
                this.selectedGroup.Expires.setFullYear(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate());
            }
            else {
                this.selectedGroup.Expires = new Date(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate());
            }
        },
        enumerable: true,
        configurable: true
    });
    AssignTestComponent.prototype.ngOnInit = function () {
        var _this = this;
        var jsonTest = localStorage.getItem("selectedTest");
        var parsedTest = JSON.parse(jsonTest);
        this.test = new Test_1.Test(parsedTest.testId, parsedTest.name, parsedTest.interviewerId);
        if (this.test == null) {
            this.router.navigate(['/interviewers']);
        }
        this.groupService.GetGroups().then(function (groups) {
            _this.groups = groups;
            _this.isGroupsLoaded = true;
            if (_this.isTestGroupsLoaded) {
                _this.isLoaded = true;
                setTimeout(_this.jQueryMarkup, 1);
            }
        });
        this.testGroupService.getByTestId(this.test.TestId).then(function (tGroups) {
            _this.testGroups = tGroups;
            _this.isTestGroupsLoaded = true;
            if (_this.isGroupsLoaded) {
                _this.isLoaded = true;
                setTimeout(_this.jQueryMarkup, 1);
            }
        });
        GroupSelecter_1.GroupSelector.RegisterCallback(function (group) {
            var tGroup = _this.testGroups.find(function (tg) { return tg.GroupId == group.id; });
            if (tGroup != null) {
                _this.selectedGroup = tGroup;
            }
            else {
                var date = new Date();
                date.setMonth(date.getMonth() + 1);
                _this.selectedGroup = new TestGroup_1.TestGroup(_this.test.TestId, group.id, date, _this.test.Name, group.name);
            }
        });
    };
    AssignTestComponent.prototype.goBack = function () {
        history.back();
    };
    AssignTestComponent.prototype.containsTestGroup = function (groupId) {
        var index = this.testGroups.findIndex(function (val) { return val.GroupId == groupId; });
        return index != -1;
    };
    AssignTestComponent.prototype.getTestGroup = function (testId, groupId) {
        return this.testGroups.find(function (val) { return val.GroupId == groupId && val.TestId == testId; });
    };
    AssignTestComponent.prototype.checkGroup = function (group) {
        var date = new Date();
        date.setMonth(date.getMonth() + 1);
        var curTest = this.getTestGroup(this.test.TestId, group.id);
        date = curTest != undefined ? curTest.Expires : date;
        var testGroup = new TestGroup_1.TestGroup(this.test.TestId, group.id, date, this.test.Name, group.name);
        if (this.selectedGroup != null && this.selectedGroup.GroupId == testGroup.GroupId
            && this.selectedGroup.TestId == testGroup.TestId) {
            this.selectedGroup = null;
            return;
        }
        this.selectedGroup = testGroup;
        this.isDateEdited = false;
    };
    AssignTestComponent.prototype.selectTGroup = function (tGroup) {
        this.selectedGroup = tGroup;
    };
    AssignTestComponent.prototype.assignTest = function () {
        var _this = this;
        if (this.selectedGroup == null) {
            return;
        }
        if (this.selectedGroup.Expires == null) {
            ErrorHandler_1.ErrorHandler.Error("Specify final date of test passing");
            return;
        }
        var index = this.testGroups.findIndex(function (tg) { return tg.GroupId == _this.selectedGroup.GroupId
            && tg.TestId == _this.test.TestId; });
        if (index == -1) {
            this.testGroupService.createTestGroup(this.selectedGroup).then(function (res) {
                if (res.IsSuccess) {
                    _this.testGroups.push(_this.selectedGroup);
                }
                else {
                    ErrorHandler_1.ErrorHandler.Error(res.Message);
                }
            });
        }
        else if (this.isDateEdited) {
            this.testGroupService.updateTestGroup(this.selectedGroup).then(function (res) {
                if (res.IsSuccess) {
                }
                else {
                    ErrorHandler_1.ErrorHandler.Error(res.Message);
                }
            });
        }
        else {
            this.testGroupService.deleteTestGroup(this.selectedGroup).then(function (res) {
                if (res.IsSuccess) {
                    _this.testGroups.splice(index, 1);
                }
                else {
                    ErrorHandler_1.ErrorHandler.Error(res.Message);
                }
            });
        }
    };
    AssignTestComponent = __decorate([
        core_1.Component({
            selector: '<assign-test> </assign-test>',
            templateUrl: '/app/views/assign.test.component.html',
            providers: [test_service_1.TestService, group_service_1.GroupService, test_group_service_1.TestGroupService]
        }), 
        __metadata('design:paramtypes', [test_service_1.TestService, router_1.Router, group_service_1.GroupService, test_group_service_1.TestGroupService])
    ], AssignTestComponent);
    return AssignTestComponent;
}());
exports.AssignTestComponent = AssignTestComponent;
//# sourceMappingURL=assign.test.component.js.map