"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var ErrorHandler_1 = require("./Library/ErrorHandler");
var Session_1 = require("./Library/Session");
//models
var User_1 = require("./Models/User");
var TestViewModel_1 = require("./Models/TestViewModel");
var QuestionViewModel_1 = require("./Models/QuestionViewModel");
var AnswerViewModel_1 = require("./Models/AnswerViewModel");
var Enums_1 = require("./Models/Enums");
//services
var edit_test_service_1 = require("./Services/edit.test.service");
var CreateTestComponent = (function () {
    function CreateTestComponent(editTestService, router) {
        this.editTestService = editTestService;
        this.router = router;
    }
    CreateTestComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = new User_1.User();
        this.user = Session_1.Session.AuthenticatedUser;
        this.testViewModel = new TestViewModel_1.TestViewModel();
        this.deletingQID = null;
        this.mutq = false;
        this.muta = false;
        if (sessionStorage.getItem('selectedTest') != null) {
            var parsedTest = JSON.parse(sessionStorage.getItem('selectedTest'));
            sessionStorage.removeItem("selectedTest");
            this.editTestService.getTest(parsedTest.testId).then(function (model) {
                _this.testViewModel = new TestViewModel_1.TestViewModel(model.TestId, model.Name, model.InterviewerId, model.Questions.map(function (q) { return new QuestionViewModel_1.QuestionViewModel(q.QuestionId, q.Text, q.TestId, q.Type, q.Answers.map(function (a) {
                    return new AnswerViewModel_1.AnswerViewModel(a.AnswerId, a.Text);
                })); }));
            });
            this.currentQuestionId = -1;
        }
        else {
            this.testViewModel.testId = -1;
            this.testViewModel.interviewerId = this.user.id;
            this.currentQuestionId = -3;
            //add two blank questions for template
            this.testViewModel.questions.push(new QuestionViewModel_1.QuestionViewModel(-1, "", this.testViewModel.testId, Enums_1.QuestionType.SingleAnswer, new Array()));
            this.testViewModel.questions[0].answers.push(new AnswerViewModel_1.AnswerViewModel(-1, ""));
            this.testViewModel.questions[0].answers.push(new AnswerViewModel_1.AnswerViewModel(-2, ""));
            this.testViewModel.questions.push(new QuestionViewModel_1.QuestionViewModel(-2, "", this.testViewModel.testId, Enums_1.QuestionType.SingleAnswer, new Array()));
            this.testViewModel.questions[1].answers.push(new AnswerViewModel_1.AnswerViewModel(-1, ""));
            this.testViewModel.questions[1].answers.push(new AnswerViewModel_1.AnswerViewModel(-2, ""));
        }
    };
    CreateTestComponent.prototype.addNewQuestion = function () {
        this.testViewModel.questions.push(new QuestionViewModel_1.QuestionViewModel(this.currentQuestionId, "", this.testViewModel.testId, Enums_1.QuestionType.SingleAnswer, new Array()));
        this.testViewModel.questions[this.testViewModel.questions.length - 1].answers.push(new AnswerViewModel_1.AnswerViewModel(-1, ""));
        this.testViewModel.questions[this.testViewModel.questions.length - 1].answers.push(new AnswerViewModel_1.AnswerViewModel(-2, ""));
        this.currentQuestionId--;
    };
    CreateTestComponent.prototype.getQuestionById = function (Qid) {
        for (var i = 0; i < this.testViewModel.questions.length; i++) {
            if (this.testViewModel.questions[i].questionId == Qid) {
                return this.testViewModel.questions[i];
            }
        }
        return null;
    };
    CreateTestComponent.prototype.getAnswerById = function (Qid, Aid) {
        for (var j = 0; j < this.getQuestionById(Qid).answers.length; j++) {
            if (this.getQuestionById(Qid).answers[j].answerId == Aid) {
                return this.getQuestionById(Qid).answers[j];
            }
        }
        return null;
    };
    CreateTestComponent.prototype.addNewAnswer = function (Qid) {
        this.getQuestionById(Qid).answers.push(new AnswerViewModel_1.AnswerViewModel(-this.getQuestionById(Qid).answers.length - 1, ""));
    };
    CreateTestComponent.prototype.saveQText = function (Qid, text) {
        this.getQuestionById(Qid).text = text;
    };
    CreateTestComponent.prototype.saveQAnswer = function (Qid, Aid, text) {
        if (this.getAnswerById(Qid, Aid) != null) {
            this.getAnswerById(Qid, Aid).text = text;
        }
        else {
            this.getQuestionById(Qid).answers.push(new AnswerViewModel_1.AnswerViewModel(-1, text));
        }
    };
    CreateTestComponent.prototype.saveQType = function (Qid, type) {
        if (type == "radio") {
            this.getQuestionById(Qid).type = Enums_1.QuestionType.SingleAnswer;
        }
        else {
            this.getQuestionById(Qid).type = Enums_1.QuestionType.MultipleAnswer;
        }
    };
    CreateTestComponent.prototype.deleteQuestion = function () {
        this.testViewModel.questions.splice(this.testViewModel.questions.indexOf(this.getQuestionById(this.deletingQID)), 1);
        this.deletingQID = null;
    };
    CreateTestComponent.prototype.deleteAnswer = function (Qid, Aid) {
        this.getQuestionById(Qid).answers.splice(this.getQuestionById(Qid).answers.indexOf(this.getAnswerById(Qid, Aid)), 1);
    };
    CreateTestComponent.prototype.isUniqueQuestion = function (question) {
        this.mutq = false;
        console.log(this.testViewModel);
        for (var _i = 0, _a = this.testViewModel.questions; _i < _a.length; _i++) {
            var q = _a[_i];
            //console.log(q.text);
            //console.log(question.text);
            if (question.text === q.text) {
                console.log("into if");
                console.log(this.mutq);
                if (this.mutq) {
                    ErrorHandler_1.ErrorHandler.Error("Question  \"" + q.text + "\" is duplicated.");
                    this.mutq = false;
                    return false;
                }
                this.mutq = true;
            }
            if (!this.isUniqueAnswers(q)) {
                return false;
            }
        }
        return true;
    };
    CreateTestComponent.prototype.isUniqueAnswers = function (question) {
        this.muta = false;
        for (var _i = 0, _a = question.answers; _i < _a.length; _i++) {
            var a1 = _a[_i];
            for (var _b = 0, _c = question.answers; _b < _c.length; _b++) {
                var a2 = _c[_b];
                if (a1.text === a2.text) {
                    if (this.muta) {
                        ErrorHandler_1.ErrorHandler.Error("Answer  \"" + a1.text + "\" is duplicated.");
                        this.muta = false;
                        return false;
                    }
                    this.muta = true;
                }
            }
            this.muta = false;
            return true;
        }
    };
    CreateTestComponent.prototype.isValidate = function () {
        if (this.testViewModel.questions.length < 2) {
            ErrorHandler_1.ErrorHandler.Error("Test must consists at least 2 questions");
            return false;
        }
        for (var _i = 0, _a = this.testViewModel.questions; _i < _a.length; _i++) {
            var q = _a[_i];
            if (q.answers.length < 2) {
                ErrorHandler_1.ErrorHandler.Error("Question  \"" + q.text + "\" must consists at least 2 answers");
                return false;
            }
            if (!this.isUniqueQuestion(q)) {
                return false;
            }
        }
        return true;
    };
    CreateTestComponent.prototype.save = function () {
        var _this = this;
        if (this.isValidate()) {
            if (this.testViewModel.testId == -1) {
                this.editTestService.saveTest(this.testViewModel).then(function (model) {
                    if (model.IsSuccess) {
                        _this.router.navigate(["/interviewer"]);
                    }
                    else {
                        ErrorHandler_1.ErrorHandler.Error(model.Message);
                    }
                });
            }
            else {
                this.editTestService.updateTest(this.testViewModel).then(function (model) {
                    if (model.IsSuccess) {
                        _this.router.navigate(["/interviewer"]);
                    }
                    else {
                        ErrorHandler_1.ErrorHandler.Error(model.Message);
                    }
                });
            }
        }
    };
    CreateTestComponent.prototype.confirmDelete = function (Qid) {
        this.deletingQID = Qid;
    };
    CreateTestComponent.prototype.back = function () {
        history.back();
    };
    CreateTestComponent = __decorate([
        core_1.Component({
            selector: "<create-test-page></create-test-page>",
            templateUrl: "app/Views/create.test.component.html",
            providers: [edit_test_service_1.EditTestService]
        }), 
        __metadata('design:paramtypes', [edit_test_service_1.EditTestService, router_1.Router])
    ], CreateTestComponent);
    return CreateTestComponent;
}());
exports.CreateTestComponent = CreateTestComponent;
//# sourceMappingURL=create.test.component.js.map