import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

//models
import { TestViewModel } from "./../Models/TestViewModel";
import { CUDResponseModel } from "./../Models/CUDResponseModel";
import { UserResultViewModel } from "./../Models/UserResultViewModel";
import { QuestionViewModel } from "./../Models/QuestionViewModel";
import { AnswerViewModel } from "./../Models/AnswerViewModel";
 
@Injectable()
export class EditTestService {

    private testViewModel : TestViewModel= new TestViewModel();
    
    private path:string = 'http://sm.somee.com/';

    public constructor(private http : Http) {
    }

    public getTest(id: number) : Promise <any>{
        return this.http.get(this.path + "api/test/get?id=" + id).toPromise()
        .then(rs => {
            var res = rs.json();
            if (res != null) {
                return res;
            }
            else {
                return null;
            }   
        });
    }

    public saveTest (testViewModel: TestViewModel) : Promise<CUDResponseModel> {
        return this.http.post(this.path + "api/test/add", testViewModel)
        .toPromise()
            .then(rs => {
                var res = rs.json();
                if (res != null) {
                    return new CUDResponseModel(res.IsSuccess, res.ErrorMessage);
                }
                else {
                    return null;
                }   
        });
    }

    public updateTest (testViewModel: TestViewModel) : Promise<CUDResponseModel> {
        return this.http.put(this.path + "api/test/update", testViewModel)
        .toPromise()
            .then(rs => {
                var res = rs.json();
                if (res != null) {
                    return new CUDResponseModel(res.IsSuccess, res.ErrorMessage);
                }
                else {
                    return null;
                }   
        });
    }

    public deleteTest(id: number): Promise<CUDResponseModel> {
        return this.http.delete(this.path + "api/test/delete" + "?id=" + id).toPromise()
            .then(rs => {
                var res = rs.json();
                if (res != null) {
                    return new CUDResponseModel(res.IsSuccess, res.ErrorMessage);
                }
                else {
                    return null;
                }
            });
    }
}