import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import { RespondentTest } from "./../Models/RespondentTest";
import { Test } from "./../Models/Test";

@Injectable()
export class TestService {

    private path = "http://sm.somee.com/api/test";

    public constructor(private http: Http) { }

    private transformToRespondentTestArray(res: any): RespondentTest[] {
        var jsParsed = res.json();
        return jsParsed.map(test =>
            new RespondentTest(test.TestId, test.Name, test.InterviewerId, test.IsPassed, test.FeedbackMessage));
    }

    private transformToTestArray(res: any) : Test[] {
        var jsParsed = res.json();
        return jsParsed.map(test => new Test(test.TestId, test.Name, test.InterviewerId));
    }

    private transformToTest(res: any) : Test {
        var jsParsed = res.json();
        return new Test(jsParsed.TestId, jsParsed.Name, jsParsed.InterviewerId);
    }

    public GetTestsByRespondent(respondentId: number) {
        return this.http.get(this.path + "/getRespondentTests?userId=" + respondentId).toPromise()
            .then(res => this.transformToRespondentTestArray(res));
    }

    public getTestsByInterviewer(interviewerId: number) {
        return this.http.get(this.path + "/getInterviewerTests?userId=" + interviewerId).toPromise()
            .then(res => this.transformToTestArray(res));
    }

    public getTestById(id: number) {
        return this.http.get(this.path).toPromise().then(res => this.transformToTest(res));
    }

}