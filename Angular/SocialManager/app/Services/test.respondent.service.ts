import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import { RespondentTest } from "./../Models/RespondentTest";

@Injectable() 
export class TestRespondentService {

    private path = "http://sm.somee.com/api/test";

    public constructor(private http: Http) { } 

    private transformToTestArray(res: any) : RespondentTest[] {
        var jsParsed = res.json();
        return jsParsed.map(test =>
             new RespondentTest(test.TestId, test.Name, test.InterviewerId, test.IsPassed, test.FeedbackMessage));
    }

    public GetTestsByRespondent(respondentId: number) {
        return this.http.get(this.path + "/getRespondentTests?userId=" + respondentId).toPromise()
            .then(res => this.transformToTestArray(res));
    } 

}