import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import { QuestionResultViewModel } from "./../Models/QuestionResultViewModel";

@Injectable()
export class TestQuestionService {

    private url:string = "http://sm.somee.com/api/result";

    constructor(private http: Http) { }

    private transformToResultArray(res:any) {
        res = res.json();
        return res.map(result => new QuestionResultViewModel(result.QAnswerId, result.Percent, 
                result.QuestionText, result.AnswerText));
    }

    public GetQuestionResults(testId:number, groupId:number ) {
        return this.http.get(this.url + "/get?testId=" + testId + "&groupId=" + groupId).toPromise()
            .then(res => this.transformToResultArray(res));
    }
}