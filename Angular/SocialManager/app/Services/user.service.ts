import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions  } from "@angular/http";

// models
import { User } from "./../Models/User";
import { LoginViewModel } from "./../Models/LoginViewModel";
import { CUDResponseModel } from "./../Models/CUDResponseModel";
import { RegisterResponseModel } from "./../Models/RegisterResponseModel";

import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {
    
    private path:string = 'http://sm.somee.com/api/user/';

    constructor(private http : Http) {
    } 

    private transformToRegisterResponse(res: any) {
        let cudResponse = new CUDResponseModel(res.CUDResult.IsSuccess, res.CUDResult.ErrorMessage);
        let user = res.User != null ? 
            new User(res.User.UserId, res.User.Email, res.User.Login, res.User.Passsword,  res.User.FullName, 
                res.User.PhoneNumber, res.User.GroupId, res.User.Role) 
            : null;
        
        return new RegisterResponseModel(cudResponse, user);
    }

    public Login(login:string, password:string) : Promise<LoginViewModel> {
        return this.http.get(this.path + "login?Login=" + login + "&Password="+ password+"&RememberMe=true").toPromise()
        .then(rs => {
            var res = rs.json();

            if (res != null) {
                return new LoginViewModel(res.Token, new User(res.User.UserId, res.User.Email, res.User.Login, 
                res.User.Passsword,  res.User.FullName, res.User.PhoneNumber, res.User.GroupId, res.User.Role));
            }
            else {
                return null;
            }   
        });
    }

    public Register(user: User) : Promise<RegisterResponseModel> {
        let headers = new Headers({ 'content-type' : 'application/json' });
        let options = new RequestOptions( { headers: headers } );

        let postObject = { Email: user.email, Login: user.login, Password : user.password, FullName: user.surname,
                            PhoneNumber : user.phone, Role : 'creator' };

        return this.http.post(this.path + "register", postObject , options).toPromise()
        .then(rs => {
            var res = rs.json();
            if (res != null) {
                return this.transformToRegisterResponse(res);
            }
            else {
                return null;
            }   
        });
    }

    public UpdateUser(user: User) : Promise<CUDResponseModel> {
        return this.http.put(this.path, 
                            {
                                "UserId" : user.id,
                                "Login" : user.login,
                                "Password" : user.password,
                                "Email" : user.email,
                                "GroupId" : user.groupId,
                                "Role" : user.role,
                                "FullName" : user.surname,
                                "PhoneNumber" : user.phone
                            }).toPromise()
                            .then(rs => {
                                var res = rs.json();
                                if (res != null) {
                                    return new CUDResponseModel(res.IsSuccess, res.ErrorMessage);
                                }
                                else {
                                    return null;
                                }
                            });
    }

    public DeleteUser(id: number) : Promise<boolean> {

        return this.http.delete(this.path + "?id=" + id).toPromise()
        .then(rs => {
            var res = rs.json();
            if (res != null) {
                return true;
            }
            else {
                return false;
            }
        });
    }
}