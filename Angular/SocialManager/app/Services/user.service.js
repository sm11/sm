"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
// models
var User_1 = require("./../Models/User");
var LoginViewModel_1 = require("./../Models/LoginViewModel");
var CUDResponseModel_1 = require("./../Models/CUDResponseModel");
var RegisterResponseModel_1 = require("./../Models/RegisterResponseModel");
require('rxjs/add/operator/toPromise');
var UserService = (function () {
    function UserService(http) {
        this.http = http;
        this.path = 'http://sm.somee.com/api/user/';
    }
    UserService.prototype.transformToRegisterResponse = function (res) {
        var cudResponse = new CUDResponseModel_1.CUDResponseModel(res.CUDResult.IsSuccess, res.CUDResult.ErrorMessage);
        var user = res.User != null ?
            new User_1.User(res.User.UserId, res.User.Email, res.User.Login, res.User.Passsword, res.User.FullName, res.User.PhoneNumber, res.User.GroupId, res.User.Role)
            : null;
        return new RegisterResponseModel_1.RegisterResponseModel(cudResponse, user);
    };
    UserService.prototype.Login = function (login, password) {
        return this.http.get(this.path + "login?Login=" + login + "&Password=" + password + "&RememberMe=true").toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return new LoginViewModel_1.LoginViewModel(res.Token, new User_1.User(res.User.UserId, res.User.Email, res.User.Login, res.User.Passsword, res.User.FullName, res.User.PhoneNumber, res.User.GroupId, res.User.Role));
            }
            else {
                return null;
            }
        });
    };
    UserService.prototype.Register = function (user) {
        var _this = this;
        var headers = new http_1.Headers({ 'content-type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var postObject = { Email: user.email, Login: user.login, Password: user.password, FullName: user.surname,
            PhoneNumber: user.phone, Role: 'creator' };
        return this.http.post(this.path + "register", postObject, options).toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return _this.transformToRegisterResponse(res);
            }
            else {
                return null;
            }
        });
    };
    UserService.prototype.UpdateUser = function (user) {
        return this.http.put(this.path, {
            "UserId": user.id,
            "Login": user.login,
            "Password": user.password,
            "Email": user.email,
            "GroupId": user.groupId,
            "Role": user.role,
            "FullName": user.surname,
            "PhoneNumber": user.phone
        }).toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return new CUDResponseModel_1.CUDResponseModel(res.IsSuccess, res.ErrorMessage);
            }
            else {
                return null;
            }
        });
    };
    UserService.prototype.DeleteUser = function (id) {
        return this.http.delete(this.path + "?id=" + id).toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return true;
            }
            else {
                return false;
            }
        });
    };
    UserService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map