import { Injectable } from "@angular/core";
import { Http } from '@angular/http';

// models
import { Feedback } from "./../Models/Feedback";
import { CUDResponseModel } from "./../Models/CUDResponseModel";

@Injectable()
export class FeedbackService {
    private url: string = "http://sm.somee.com/";

    constructor(private http: Http) { }

    private transformToFeedback(res: any) {
        res = res.json();
        if (res != null) {
            return new Feedback(res.TestId, res.GroupId, res.Text);
        }
        else {
            return null;
        }
    }

    private transformToCUDResponse(res: any) {
        let rs = res.json();
        return new CUDResponseModel(rs.IsSuccess, rs.ErrorMessage);
    }

    public getFeedbackById(testId: number, groupId: number) {
        return this.http.get(this.url + "api/feedback/get?testId=" + testId + "&groupId=" + groupId).toPromise()
            .then(res => this.transformToFeedback(res));
    }

    public createFeedback(fb: Feedback) {
        return this.http.post(this.url + "api/feedback", fb).toPromise()
            .then(res => this.transformToCUDResponse(res));
    }

    public updateFeedback(fb: Feedback) {
        return this.http.put(this.url + "api/feedback", fb).toPromise()
            .then(res => this.transformToCUDResponse(res));
    }
}