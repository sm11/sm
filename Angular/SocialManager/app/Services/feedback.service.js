"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require('@angular/http');
// models
var Feedback_1 = require("./../Models/Feedback");
var CUDResponseModel_1 = require("./../Models/CUDResponseModel");
var FeedbackService = (function () {
    function FeedbackService(http) {
        this.http = http;
        this.url = "http://sm.somee.com/";
    }
    FeedbackService.prototype.transformToFeedback = function (res) {
        res = res.json();
        if (res != null) {
            return new Feedback_1.Feedback(res.TestId, res.GroupId, res.Text);
        }
        else {
            return null;
        }
    };
    FeedbackService.prototype.transformToCUDResponse = function (res) {
        var rs = res.json();
        return new CUDResponseModel_1.CUDResponseModel(rs.IsSuccess, rs.ErrorMessage);
    };
    FeedbackService.prototype.getFeedbackById = function (testId, groupId) {
        var _this = this;
        return this.http.get(this.url + "api/feedback/get?testId=" + testId + "&groupId=" + groupId).toPromise()
            .then(function (res) { return _this.transformToFeedback(res); });
    };
    FeedbackService.prototype.createFeedback = function (fb) {
        var _this = this;
        return this.http.post(this.url + "api/feedback", fb).toPromise()
            .then(function (res) { return _this.transformToCUDResponse(res); });
    };
    FeedbackService.prototype.updateFeedback = function (fb) {
        var _this = this;
        return this.http.put(this.url + "api/feedback", fb).toPromise()
            .then(function (res) { return _this.transformToCUDResponse(res); });
    };
    FeedbackService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], FeedbackService);
    return FeedbackService;
}());
exports.FeedbackService = FeedbackService;
//# sourceMappingURL=feedback.service.js.map