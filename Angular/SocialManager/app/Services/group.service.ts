import { Injectable } from "@angular/core";
import { Http, RequestOptions, Headers } from "@angular/http";

// models
import { Group } from "./../Models/Group";
import { CUDResponseModel } from "./../Models/CUDResponseModel";
import { User } from "./../Models/User";

@Injectable()
export class GroupService {

    private path: string = 'http://sm.somee.com/';

    public constructor(private http: Http) {
    }

    public GetGroup(id:number): Promise<Group> {
        return this.http.get(this.path + "api/group/get?id=" + id).toPromise()
            .then(rs => {
                var res = rs.json();
                if (res != null) {
                    return res;
                }
                else {
                    return null;
                }
            });
    }

    private transformToUser(user: any): User {
        if (user != null) {
            return new User(user.UserId, user.Email, user.Login,
                user.Passsword, user.FullName, user.PhoneNumber, user.GroupId, user.Role);
        }
        return null;
    }

    private transformToGroup(res: any): Group {
        if (res != null) {
            return new Group(res.GroupId, res.GroupName, res.Subgroups != null ? res.Subgroups.map(g => {
                if (g != null) {
                    return this.transformToGroup(g);
                }
            }) : null, res.Users != null ? res.Users.map(user => {
                if (user != null) {
                    return this.transformToUser(user);
                }
            }) : null, this.transformToUser(res.Interviewer), this.transformToGroup(res.Parent), res.ParentId);
        }
        return null;
    }

    private transformToGroupArray(res: any): Group[] {
        return res.map(group => this.transformToGroup(group));
    }

    public GetGroups(): Promise<Group[]> {
        return this.http.get(this.path + "api/group/getall").toPromise()
            .then(rs => {
                var res = rs.json();
                if (res != null) {
                    return this.transformToGroupArray(res);
                }
                else {
                    return null;
                }
            });
    }

    public GetGroupsByInterviewer(userId :number): Promise<Group[]> {
        return this.http.get(this.path + "api/group/getcreatortests?userId=" + userId).toPromise()
            .then(rs => {
                var res = rs.json();
                if (res != null) {
                    return this.transformToGroupArray(res);
                }
                else {
                    return null;
                }
            });
    }

    public CreateGroup(group: Group): Promise<Group> {
        return this.http.post(this.path + "api/group/add", //group
            {
                "GroupId": group.id,
                "GroupName": group.name,
                "ParentId": group.parentId,
                "CreatorId": group.creatorId
            }
        ).toPromise()
            .then(rs => {
                var res = rs.json();
                if (res != null) {
                    return this.transformToGroup(res);
                }
                else {
                    return null;
                }
            });
    }

    public UpdateGroup(group: Group): Promise<CUDResponseModel> {
        return this.http.put(this.path + "api/group",
            {
                "GroupId": group.id,
                "GroupName": group.name,
                "ParentId": group.parentId,
                "CreatorId": group.creatorId
            }).toPromise()
            .then(rs => {
                var res = rs.json();
                if (res != null) {
                    return new CUDResponseModel(res.IsSuccess, res.ErrorMessage);
                }
                else {
                    return null;
                }
            });
    }

    public DeleteGroup(id: number) {
        return this.http.delete(this.path + "api/group" + "?id=" + id).toPromise()
            .then(rs => {
                var res = rs.json();
                if (res != null) {
                    return true;
                }
                else {
                    return false;
                }
            });
    }

    public UploadFile(groupId: number, file:File) {
        let formData = new FormData();

        formData.append('File', file);

        let headers = new Headers({ 'enctype': 'multipart/form-data' })
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.path + "api/group/uploadFile?GroupId=" + groupId, formData, options).toPromise().then(rs => {
            var res = rs.json();
            if (res != null) {
                return new CUDResponseModel(res.IsSuccess, res.ErrorMessage);
            }
            else {
                return null;
            }
        });
    }
}