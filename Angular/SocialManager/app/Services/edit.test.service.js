"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
//models
var TestViewModel_1 = require("./../Models/TestViewModel");
var CUDResponseModel_1 = require("./../Models/CUDResponseModel");
var EditTestService = (function () {
    function EditTestService(http) {
        this.http = http;
        this.testViewModel = new TestViewModel_1.TestViewModel();
        this.path = 'http://sm.somee.com/';
    }
    EditTestService.prototype.getTest = function (id) {
        return this.http.get(this.path + "api/test/get?id=" + id).toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return res;
            }
            else {
                return null;
            }
        });
    };
    EditTestService.prototype.saveTest = function (testViewModel) {
        return this.http.post(this.path + "api/test/add", testViewModel)
            .toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return new CUDResponseModel_1.CUDResponseModel(res.IsSuccess, res.ErrorMessage);
            }
            else {
                return null;
            }
        });
    };
    EditTestService.prototype.updateTest = function (testViewModel) {
        return this.http.put(this.path + "api/test/update", testViewModel)
            .toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return new CUDResponseModel_1.CUDResponseModel(res.IsSuccess, res.ErrorMessage);
            }
            else {
                return null;
            }
        });
    };
    EditTestService.prototype.deleteTest = function (id) {
        return this.http.delete(this.path + "api/test/delete" + "?id=" + id).toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return new CUDResponseModel_1.CUDResponseModel(res.IsSuccess, res.ErrorMessage);
            }
            else {
                return null;
            }
        });
    };
    EditTestService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], EditTestService);
    return EditTestService;
}());
exports.EditTestService = EditTestService;
//# sourceMappingURL=edit.test.service.js.map