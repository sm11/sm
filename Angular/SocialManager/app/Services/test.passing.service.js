"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
//models
var TestViewModel_1 = require("./../Models/TestViewModel");
var CUDResponseModel_1 = require("./../Models/CUDResponseModel");
var QuestionViewModel_1 = require("./../Models/QuestionViewModel");
var AnswerViewModel_1 = require("./../Models/AnswerViewModel");
var TestPassingService = (function () {
    function TestPassingService(http) {
        this.http = http;
        this.path = 'http://sm.somee.com/';
    }
    TestPassingService.prototype.transformToTestModel = function (model) {
        return new TestViewModel_1.TestViewModel(model.TestId, model.Name, model.InterviewerId, model.Questions.map(function (q) { return new QuestionViewModel_1.QuestionViewModel(q.QuestionId, q.Text, q.TestId, q.Type, q.Answers.map(function (a) {
            return new AnswerViewModel_1.AnswerViewModel(a.AnswerId, a.Text);
        })); }));
    };
    TestPassingService.prototype.getTest = function (id) {
        return this.http.get(this.path + "api/test/get?id=" + id).toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return res;
            }
            else {
                return null;
            }
        });
    };
    TestPassingService.prototype.getTestParsed = function (id) {
        var _this = this;
        return this.http.get(this.path + "api/test/get?id=" + id).toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return _this.transformToTestModel(res);
            }
            else {
                return null;
            }
        });
    };
    TestPassingService.prototype.saveTest = function (userResultViewModels) {
        return this.http.post(this.path + "api/result/add", userResultViewModels)
            .toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return new CUDResponseModel_1.CUDResponseModel(res.IsSuccess, res.ErrorMessage);
            }
            else {
                return null;
            }
        });
    };
    TestPassingService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], TestPassingService);
    return TestPassingService;
}());
exports.TestPassingService = TestPassingService;
//# sourceMappingURL=test.passing.service.js.map