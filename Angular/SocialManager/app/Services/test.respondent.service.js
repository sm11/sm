"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var RespondentTest_1 = require("./../Models/RespondentTest");
var TestRespondentService = (function () {
    function TestRespondentService(http) {
        this.http = http;
        this.path = "http://sm.somee.com/api/test";
    }
    TestRespondentService.prototype.transformToTestArray = function (res) {
        var jsParsed = res.json();
        return jsParsed.map(function (test) {
            return new RespondentTest_1.RespondentTest(test.TestId, test.Name, test.InterviewerId, test.IsPassed, test.FeedbackMessage);
        });
    };
    TestRespondentService.prototype.GetTestsByRespondent = function (respondentId) {
        var _this = this;
        return this.http.get(this.path + "/getRespondentTests?userId=" + respondentId).toPromise()
            .then(function (res) { return _this.transformToTestArray(res); });
    };
    TestRespondentService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], TestRespondentService);
    return TestRespondentService;
}());
exports.TestRespondentService = TestRespondentService;
//# sourceMappingURL=test.respondent.service.js.map