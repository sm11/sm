import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import { TestGroup } from "./../Models/TestGroup";
import { CUDResponseModel } from "./../Models/CUDResponseModel";
import { GroupBaseModel } from "./../Models/GroupBaseModel"; 

@Injectable()
export class TestGroupService {
    private path = "http://sm.somee.com/api/testgroup";

    public constructor(private http : Http) {}

    private transformToTestGroupArray(res : any) {
        let arr = res.json();
        return arr.map(tg => new TestGroup(tg.TestId, tg.GroupId, new Date(tg.Expires), tg.TestName, tg.GroupName));
    }

    private transformToGroupArray(res : any) {
        let arr = res.json();
        console.log(arr);
        if (arr != null) {
            return arr.map(g => g != null ? new GroupBaseModel(g.GroupId, g.GroupName, g.ParentId) : null);
        }
        else {
            return [];
        }
    }

    private transformToCUDResponse(res: any) {
        let rs = res.json();
        return new CUDResponseModel(rs.IsSuccess, rs.ErrorMessage);
    } 

    public GetAll() : Promise<TestGroup[]> {
        return this.http.get(this.path).toPromise().then(res => this.transformToTestGroupArray(res));
    }
    
    public getByGroupId(groupId : number) : Promise<TestGroup[]> {
        return this.http.get(this.path + "/get_tests_by_groupid?groupId=" + groupId).toPromise()
                .then(res => this.transformToTestGroupArray(res));
    }

    public getByTestId(testId : number) {
        return this.http.get(this.path + "/get_tests_by_testid?testId=" + testId).toPromise()
                .then(res => this.transformToTestGroupArray(res));
    }

    public getFinalGroupsByTestId(testId:number) {
        console.log(this.path + "/get_groups_by_testid?testId=" + testId);
        return this.http.get(this.path + "/get_groups_by_testid?testId=" + testId).toPromise()
                .then(res => this.transformToGroupArray(res));
    }

    public createTestGroup(testGroup : TestGroup) {
        return this.http.post(this.path + "/add", testGroup).toPromise().then(res =>this.transformToCUDResponse(res));
    }

    public updateTestGroup(testGroup: TestGroup) {
        return this.http.put(this.path + "/update", testGroup).toPromise().then(res =>this.transformToCUDResponse(res));
    }

    public deleteTestGroup(testGroup : TestGroup) {
        return this.http.delete(this.path + "/delete?testId=" + testGroup.TestId + "&groupId=" + testGroup.GroupId)
                .toPromise().then(res =>this.transformToCUDResponse(res));
    }
}