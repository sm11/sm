"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var RespondentTest_1 = require("./../Models/RespondentTest");
var Test_1 = require("./../Models/Test");
var TestService = (function () {
    function TestService(http) {
        this.http = http;
        this.path = "http://sm.somee.com/api/test";
    }
    TestService.prototype.transformToRespondentTestArray = function (res) {
        var jsParsed = res.json();
        return jsParsed.map(function (test) {
            return new RespondentTest_1.RespondentTest(test.TestId, test.Name, test.InterviewerId, test.IsPassed, test.FeedbackMessage);
        });
    };
    TestService.prototype.transformToTestArray = function (res) {
        var jsParsed = res.json();
        return jsParsed.map(function (test) { return new Test_1.Test(test.TestId, test.Name, test.InterviewerId); });
    };
    TestService.prototype.transformToTest = function (res) {
        var jsParsed = res.json();
        return new Test_1.Test(jsParsed.TestId, jsParsed.Name, jsParsed.InterviewerId);
    };
    TestService.prototype.GetTestsByRespondent = function (respondentId) {
        var _this = this;
        return this.http.get(this.path + "/getRespondentTests?userId=" + respondentId).toPromise()
            .then(function (res) { return _this.transformToRespondentTestArray(res); });
    };
    TestService.prototype.getTestsByInterviewer = function (interviewerId) {
        var _this = this;
        return this.http.get(this.path + "/getInterviewerTests?userId=" + interviewerId).toPromise()
            .then(function (res) { return _this.transformToTestArray(res); });
    };
    TestService.prototype.getTestById = function (id) {
        var _this = this;
        return this.http.get(this.path).toPromise().then(function (res) { return _this.transformToTest(res); });
    };
    TestService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], TestService);
    return TestService;
}());
exports.TestService = TestService;
//# sourceMappingURL=test.service.js.map