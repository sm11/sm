"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
// models
var Group_1 = require("./../Models/Group");
var CUDResponseModel_1 = require("./../Models/CUDResponseModel");
var User_1 = require("./../Models/User");
var GroupService = (function () {
    function GroupService(http) {
        this.http = http;
        this.path = 'http://sm.somee.com/';
    }
    GroupService.prototype.GetGroup = function (id) {
        return this.http.get(this.path + "api/group/get?id=" + id).toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return res;
            }
            else {
                return null;
            }
        });
    };
    GroupService.prototype.transformToUser = function (user) {
        if (user != null) {
            return new User_1.User(user.UserId, user.Email, user.Login, user.Passsword, user.FullName, user.PhoneNumber, user.GroupId, user.Role);
        }
        return null;
    };
    GroupService.prototype.transformToGroup = function (res) {
        var _this = this;
        if (res != null) {
            return new Group_1.Group(res.GroupId, res.GroupName, res.Subgroups != null ? res.Subgroups.map(function (g) {
                if (g != null) {
                    return _this.transformToGroup(g);
                }
            }) : null, res.Users != null ? res.Users.map(function (user) {
                if (user != null) {
                    return _this.transformToUser(user);
                }
            }) : null, this.transformToUser(res.Interviewer), this.transformToGroup(res.Parent), res.ParentId);
        }
        return null;
    };
    GroupService.prototype.transformToGroupArray = function (res) {
        var _this = this;
        return res.map(function (group) { return _this.transformToGroup(group); });
    };
    GroupService.prototype.GetGroups = function () {
        var _this = this;
        return this.http.get(this.path + "api/group/getall").toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return _this.transformToGroupArray(res);
            }
            else {
                return null;
            }
        });
    };
    GroupService.prototype.GetGroupsByInterviewer = function (userId) {
        var _this = this;
        return this.http.get(this.path + "api/group/getcreatortests?userId=" + userId).toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return _this.transformToGroupArray(res);
            }
            else {
                return null;
            }
        });
    };
    GroupService.prototype.CreateGroup = function (group) {
        var _this = this;
        return this.http.post(this.path + "api/group/add", //group
        {
            "GroupId": group.id,
            "GroupName": group.name,
            "ParentId": group.parentId,
            "CreatorId": group.creatorId
        }).toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return _this.transformToGroup(res);
            }
            else {
                return null;
            }
        });
    };
    GroupService.prototype.UpdateGroup = function (group) {
        return this.http.put(this.path + "api/group", {
            "GroupId": group.id,
            "GroupName": group.name,
            "ParentId": group.parentId,
            "CreatorId": group.creatorId
        }).toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return new CUDResponseModel_1.CUDResponseModel(res.IsSuccess, res.ErrorMessage);
            }
            else {
                return null;
            }
        });
    };
    GroupService.prototype.DeleteGroup = function (id) {
        return this.http.delete(this.path + "api/group" + "?id=" + id).toPromise()
            .then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return true;
            }
            else {
                return false;
            }
        });
    };
    GroupService.prototype.UploadFile = function (groupId, file) {
        var formData = new FormData();
        formData.append('File', file);
        var headers = new http_1.Headers({ 'enctype': 'multipart/form-data' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(this.path + "api/group/uploadFile?GroupId=" + groupId, formData, options).toPromise().then(function (rs) {
            var res = rs.json();
            if (res != null) {
                return new CUDResponseModel_1.CUDResponseModel(res.IsSuccess, res.ErrorMessage);
            }
            else {
                return null;
            }
        });
    };
    GroupService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], GroupService);
    return GroupService;
}());
exports.GroupService = GroupService;
//# sourceMappingURL=group.service.js.map