import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

//models
import { TestViewModel } from "./../Models/TestViewModel";
import { CUDResponseModel } from "./../Models/CUDResponseModel";
import { UserResultViewModel } from "./../Models/UserResultViewModel";
import { QuestionViewModel } from "./../Models/QuestionViewModel";
import { AnswerViewModel } from "./../Models/AnswerViewModel";
 
@Injectable()
export class TestPassingService {

    private path:string = 'http://sm.somee.com/';

    public constructor(private http : Http) {
    }

    private transformToTestModel(model:any) {
        
        return new TestViewModel(model.TestId, model.Name, model.InterviewerId,
                model.Questions.map(q => new QuestionViewModel(q.QuestionId, q.Text, q.TestId, q.Type, q.Answers.map(a =>
                    new AnswerViewModel(a.AnswerId, a.Text)))));
    }

    public getTest(id: number) : Promise <any>{
        return this.http.get(this.path + "api/test/get?id=" + id).toPromise()
        .then(rs => {
            var res = rs.json();
            if (res != null) {
                return res;
            }
            else {
                return null;
            }   
        });
    }

    public getTestParsed(id:number) : Promise<TestViewModel> {
        return this.http.get(this.path + "api/test/get?id=" + id).toPromise()
        .then(rs => {
            var res = rs.json();
            if (res != null) {
                return this.transformToTestModel(res);
            }
            else {
                return null;
            }   
        });
    }

    public saveTest (userResultViewModels: UserResultViewModel[]) : Promise<CUDResponseModel> {
        return this.http.post(this.path + "api/result/add", userResultViewModels)
        .toPromise()
            .then(rs => {
                var res = rs.json();
                if (res != null) {
                    return new CUDResponseModel(res.IsSuccess, res.ErrorMessage);
                }
                else {
                    return null;
                }   
        });
    }
}