"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var TestGroup_1 = require("./../Models/TestGroup");
var CUDResponseModel_1 = require("./../Models/CUDResponseModel");
var GroupBaseModel_1 = require("./../Models/GroupBaseModel");
var TestGroupService = (function () {
    function TestGroupService(http) {
        this.http = http;
        this.path = "http://sm.somee.com/api/testgroup";
    }
    TestGroupService.prototype.transformToTestGroupArray = function (res) {
        var arr = res.json();
        return arr.map(function (tg) { return new TestGroup_1.TestGroup(tg.TestId, tg.GroupId, new Date(tg.Expires), tg.TestName, tg.GroupName); });
    };
    TestGroupService.prototype.transformToGroupArray = function (res) {
        var arr = res.json();
        console.log(arr);
        if (arr != null) {
            return arr.map(function (g) { return g != null ? new GroupBaseModel_1.GroupBaseModel(g.GroupId, g.GroupName, g.ParentId) : null; });
        }
        else {
            return [];
        }
    };
    TestGroupService.prototype.transformToCUDResponse = function (res) {
        var rs = res.json();
        return new CUDResponseModel_1.CUDResponseModel(rs.IsSuccess, rs.ErrorMessage);
    };
    TestGroupService.prototype.GetAll = function () {
        var _this = this;
        return this.http.get(this.path).toPromise().then(function (res) { return _this.transformToTestGroupArray(res); });
    };
    TestGroupService.prototype.getByGroupId = function (groupId) {
        var _this = this;
        return this.http.get(this.path + "/get_tests_by_groupid?groupId=" + groupId).toPromise()
            .then(function (res) { return _this.transformToTestGroupArray(res); });
    };
    TestGroupService.prototype.getByTestId = function (testId) {
        var _this = this;
        return this.http.get(this.path + "/get_tests_by_testid?testId=" + testId).toPromise()
            .then(function (res) { return _this.transformToTestGroupArray(res); });
    };
    TestGroupService.prototype.getFinalGroupsByTestId = function (testId) {
        var _this = this;
        console.log(this.path + "/get_groups_by_testid?testId=" + testId);
        return this.http.get(this.path + "/get_groups_by_testid?testId=" + testId).toPromise()
            .then(function (res) { return _this.transformToGroupArray(res); });
    };
    TestGroupService.prototype.createTestGroup = function (testGroup) {
        var _this = this;
        return this.http.post(this.path + "/add", testGroup).toPromise().then(function (res) { return _this.transformToCUDResponse(res); });
    };
    TestGroupService.prototype.updateTestGroup = function (testGroup) {
        var _this = this;
        return this.http.put(this.path + "/update", testGroup).toPromise().then(function (res) { return _this.transformToCUDResponse(res); });
    };
    TestGroupService.prototype.deleteTestGroup = function (testGroup) {
        var _this = this;
        return this.http.delete(this.path + "/delete?testId=" + testGroup.TestId + "&groupId=" + testGroup.GroupId)
            .toPromise().then(function (res) { return _this.transformToCUDResponse(res); });
    };
    TestGroupService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], TestGroupService);
    return TestGroupService;
}());
exports.TestGroupService = TestGroupService;
//# sourceMappingURL=test.group.service.js.map