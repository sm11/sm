"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var Group_1 = require("./Models/Group");
var Session_1 = require("./Library/Session");
//services
var group_service_1 = require("./Services/group.service");
//Library
var ErrorHandler_1 = require("./Library/ErrorHandler");
var GroupSelecter_1 = require("./Library/GroupSelecter");
var CreateOrgComponent = (function () {
    function CreateOrgComponent(groupService, router) {
        this.groupService = groupService;
        this.router = router;
        this.addingGroup = null;
        this.deletingGroup = null;
        this.editingGroup = null;
        this.editingGroupName = null;
        this.groups = [];
        this.user = null;
        this.selectedGroup = null;
        // error validating
        this.addError = null;
        this.editError = null;
        this.deleteError = null;
        // constants
        this.MAX_GROUP_LENGTH = 25;
        this.isLoaded = false;
        this.excelExtensions = ['.xls', '.xlsx', '.xlsm'];
        this.excelPath = "http://sm.somee.com/Files/test.xlsx";
    }
    CreateOrgComponent.prototype.jQueryMarkup = function () {
        //$(".ul-dropfree").find("li:has(ul):has(li)").prepend('<div class="drop"></div>');
        $(".ul-dropfree div.drop").click(function (e) {
            if ($(this).nextAll("ul").css('display') == 'none') {
                $(this).nextAll("ul").slideDown(400);
                $(this).css({ 'background-position': "-11px 0" });
            }
            else {
                $(this).nextAll("ul").slideUp(400);
                $(this).css({ 'background-position': "0 0" });
            }
        });
        $(".ul-dropfree").find("ul").slideUp(400).parents("li").children("div.drop").css({ 'background-position': "0 0" });
        $(".usersTable tr.person").click(function () {
            $(".usersTable").find("tr.person").css({ 'background-color': "white" });
            $(this).css({ 'background-color': "#dff0b3" });
        });
    };
    CreateOrgComponent.prototype.getRootGroups = function () {
        return this.groups.filter(function (group) { return group.parentId == null; });
    };
    CreateOrgComponent.prototype.loadGroups = function () {
        var _this = this;
        this.groupService.GetGroupsByInterviewer(this.user.id).then(function (groups) {
            if (groups == null) {
                _this.groups = null;
                return;
            }
            _this.groups = groups.map(function (gr) {
                var intervArr = gr.users.filter(function (u) { return u.role == 'interviewer'; });
                if (intervArr.length > 0) {
                    gr.interviewer = intervArr[0];
                }
                gr.users = gr.users.filter(function (user) { return user.role == 'respondent'; });
                return gr;
            });
            _this.isLoaded = true;
            console.log('loading groups');
            console.log(_this.groups);
        });
        GroupSelecter_1.GroupSelector.RegisterCallback(function (group) { return _this.selectedGroup = group; });
    };
    CreateOrgComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = Session_1.Session.AuthenticatedUser;
        if (this.user.role != 'creator') {
            if (this.user.role == 'interviewer') {
                this.router.navigate(['/interviewer']);
            }
            else if (this.user.role == 'respondent') {
                this.router.navigate(['/respondent']);
            }
        }
        this.loadGroups();
        GroupSelecter_1.GroupSelector.RegisterCallback(function (group) { return _this.selectedGroup = group; });
        GroupSelecter_1.GroupSelector.Group = null;
    };
    CreateOrgComponent.prototype.checkGroup = function (group) {
        this.selectedGroup = group;
    };
    CreateOrgComponent.prototype.createGroup = function () {
        var _this = this;
        console.log(this.selectedGroup);
        if (this.addingGroup.name.length > this.MAX_GROUP_LENGTH) {
            ErrorHandler_1.ErrorHandler.Error('Too long name for the group');
            return;
        }
        this.addingGroup.parentId = this.selectedGroup != null ? this.selectedGroup.id : null;
        this.addingGroup.creatorId = this.user.id;
        var groupy = this.groups.filter(function (group) { return (group.name == _this.addingGroup.name) && (group.parentId == _this.addingGroup.parentId); });
        if (groupy.length > 0) {
            console.warn('Group with this name already exists');
            ErrorHandler_1.ErrorHandler.Error("Group with this name already exists");
            this.addingGroup = null;
        }
        this.groupService.CreateGroup(this.addingGroup).then(function (group) {
            if (group == null) {
                ErrorHandler_1.ErrorHandler.Error("Internal server error");
            }
            else {
                _this.groups.push(group);
                GroupSelecter_1.GroupSelector.GroupArrChanged(_this.groups);
                _this.addingGroup = null;
                setTimeout(function () {
                    $(".ul-dropfree").find("ul").slideUp(400).parents("li").children("div.drop")
                        .css({ 'background-position': "0 0" });
                    //$(".ul-dropfree").find("li:has(li)").prepend('<div class="drop"></div>');
                    $(".ul-dropfree div.drop").unbind('click');
                    $(".ul-dropfree div.drop").css({ 'background-position': "0 0" });
                    $(".ul-dropfree div.drop").click(function (e) {
                        if ($(this).nextAll('div.group').nextAll("div").children('groups').children('ul').css('display') == 'none') {
                            $(this).nextAll('div.group').nextAll("div").children('groups').children('ul').slideDown(400);
                            $(this).css({ 'background-position': "-11px 0" });
                        }
                        else {
                            $(this).nextAll('div.group').nextAll("div").children('groups').children('ul').slideUp(400);
                            $(this).css({ 'background-position': "0 0" });
                        }
                    });
                }, 1);
            }
        });
    };
    CreateOrgComponent.prototype.addGroup = function () {
        if (this.selectedGroup == null && this.groups != null && this.groups.length > 0) {
            console.warn('cannot add group');
            return;
        }
        this.addingGroup = new Group_1.Group();
    };
    CreateOrgComponent.prototype.updateGroup = function () {
        var _this = this;
        if (this.editingGroupName.length > this.MAX_GROUP_LENGTH) {
            ErrorHandler_1.ErrorHandler.Error('Too long name for the group');
            return;
        }
        this.editingGroup.creatorId = this.user.id;
        this.editingGroup.name = this.editingGroupName;
        this.groupService.UpdateGroup(this.editingGroup).then(function (res) {
            if (!res.IsSuccess) {
                ErrorHandler_1.ErrorHandler.Error(res.Message);
            }
            else {
                //this.groups.push(this.addingGroup);
                _this.editingGroup = null;
            }
        });
    };
    CreateOrgComponent.prototype.deleteGroup = function () {
        var _this = this;
        this.groupService.DeleteGroup(this.deletingGroup.id).then(function (res) {
            if (!res) {
                ErrorHandler_1.ErrorHandler.Error("Not found entity. Possibly it's already deleted.");
            }
            else {
                _this.deleteGroupFromArr(_this.deletingGroup);
                _this.deletingGroup = null;
                GroupSelecter_1.GroupSelector.GroupArrChanged(_this.groups);
            }
        });
    };
    CreateOrgComponent.prototype.deleteGroupFromArr = function (group) {
        var _this = this;
        this.groups.filter(function (group) { return group.parentId == group.id; }).forEach(function (group) { return _this.deleteGroupFromArr(group); });
        var index = this.groups.findIndex(function (gr) { return gr.id == _this.deletingGroup.id; });
        this.groups.splice(index, 1);
    };
    CreateOrgComponent.prototype.uploadFile = function () {
        var _this = this;
        $("#fileInput").click();
        $("#fileInput").change(function (ev) {
            var file = document.getElementById('fileInput').files[0];
            if (_this.excelExtensions.findIndex(function (ext) { return file.name.endsWith(ext); }) == -1) {
                ErrorHandler_1.ErrorHandler.Error('Invalid file extension');
                return;
            }
            _this.isLoaded = false;
            _this.groupService.UploadFile(_this.selectedGroup.id, file).then(function (response) {
                if (!response.IsSuccess) {
                    ErrorHandler_1.ErrorHandler.Error(response.Message);
                    _this.isLoaded = true;
                }
                else {
                    GroupSelecter_1.GroupSelector.Group = null;
                    _this.loadGroups();
                }
            });
        });
    };
    CreateOrgComponent.prototype.downloadFile = function () {
        window.open(this.excelPath);
    };
    CreateOrgComponent.prototype.openEditModal = function (group) {
        this.editingGroup = group;
        this.editingGroupName = group.name;
    };
    CreateOrgComponent = __decorate([
        core_1.Component({
            selector: 'create-org',
            templateUrl: "/app/Views/create.organization.html",
            providers: [group_service_1.GroupService]
        }), 
        __metadata('design:paramtypes', [group_service_1.GroupService, router_1.Router])
    ], CreateOrgComponent);
    return CreateOrgComponent;
}());
exports.CreateOrgComponent = CreateOrgComponent;
//# sourceMappingURL=create.organization.component.js.map