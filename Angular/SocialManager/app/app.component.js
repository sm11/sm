"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
// services
var user_service_1 = require("./Services/user.service");
//library
var Session_1 = require("./Library/Session");
var ErrorHandler_1 = require("./Library/ErrorHandler");
var AppComponent = (function () {
    function AppComponent(router) {
        this.router = router;
        this.user = null;
        this.errorMessage = null;
    }
    AppComponent.prototype.jQueryMarkUp = function () {
        $(".header div.dropdown").click(function () {
            if ($(".header div.logoutbox").css('display') == 'none') {
                $(".logoutbox").css({ 'display': "block" });
            }
            else {
                $(".logoutbox").css({ 'display': "none" });
            }
        });
    };
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = Session_1.Session.AuthenticatedUser;
        Session_1.Session.saveCallback(function (user) { return _this.user = user; });
        ErrorHandler_1.ErrorHandler.RegisterCallback(function (message) { return _this.errorMessage = message; });
        ErrorHandler_1.ErrorHandler.RegisterHideCallback(function () { return _this.errorMessage = null; });
        this.jQueryMarkUp();
    };
    AppComponent.prototype.LogOut = function () {
        Session_1.Session.AuthenticatedUser = null;
        this.router.navigate(['/login']);
    };
    AppComponent.prototype.ProfEdit = function () {
        this.router.navigate(['/profile-editing']);
    };
    AppComponent.prototype.help = function () {
        this.router.navigate(['/help']);
    };
    AppComponent.prototype.isHelpPage = function () {
        return (window.location.toString() == "http://localhost:3000/help");
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'sm-app',
            templateUrl: "/app/Views/layout.html",
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [router_1.Router])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
;
//# sourceMappingURL=app.component.js.map