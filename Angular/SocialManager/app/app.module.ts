import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from "@angular/http";

// components
import { AppComponent }   from './app.component';
import { CreateOrgComponent } from "./create.organization.component";
import { LoginComponent } from "./login.component";
import { RegisterComponent } from "./register.component";
import { TestPassingComponent } from "./test.passing.component";
import { CreateTestComponent } from "./create.test.component";

import { InterviewersComponent } from "./interviewers.component";
import { AssignTestComponent } from "./assign.test.component";

import { RespondentComponent } from "./respondent.component";
import { ReportComponent } from "./report.component";
import { ProfileEditingComponent } from "./profile.editing.component";
import { GroupListComponent } from "./group-list.component";
import { HelpComponent } from "./help.component";

//services
import { UserService } from "./services/user.service";
import { TestGroupService } from "./services/test.group.service";
import { TestService } from "./services/test.service";
import { TestRespondentService } from "./services/test.respondent.service";
import { GroupService } from "./services/group.service";
import { TestPassingService } from "./services/test.passing.service";
import { EditTestService } from "./services/edit.test.service";

// routing
import { AppRoutingModule } from "./app-routing.module"; 

@NgModule({
    imports:      [ BrowserModule, FormsModule, AppRoutingModule, HttpModule ],
    declarations: [ 
        AppComponent, CreateOrgComponent, LoginComponent, RegisterComponent, InterviewersComponent,
        AssignTestComponent, RespondentComponent, TestPassingComponent, ReportComponent, ProfileEditingComponent,
        GroupListComponent, CreateTestComponent, HelpComponent
    ],
    providers:    [ UserService, TestGroupService, TestService, TestRespondentService, GroupService, 
        TestPassingService, EditTestService 
    ],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }