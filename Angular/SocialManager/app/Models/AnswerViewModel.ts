export class AnswerViewModel {
    
    answerId: number;
    text: string;

    constructor (answerId: number = 0, text: string = "textquestion") {
        this.answerId = answerId;
        this.text = text;
    }
}