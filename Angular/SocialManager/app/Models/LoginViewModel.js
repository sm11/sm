"use strict";
var LoginViewModel = (function () {
    function LoginViewModel(token, user) {
        this.token = token;
        this.user = user;
    }
    Object.defineProperty(LoginViewModel.prototype, "Token", {
        get: function () { return this.token; },
        set: function (value) { this.token = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginViewModel.prototype, "User", {
        get: function () { return this.user; },
        set: function (value) { this.user = value; },
        enumerable: true,
        configurable: true
    });
    return LoginViewModel;
}());
exports.LoginViewModel = LoginViewModel;
//# sourceMappingURL=LoginViewModel.js.map