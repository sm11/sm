export class QuestionResultViewModel {
    private qAnswerId:number = 0;
    private percent: number = 0;
    private questionText: string = null;
    private answerText: string = null;

    public constructor(qAnswerId: number = 0, percent: number = 0, question: string =null, answer : string = null) {
        this.qAnswerId = qAnswerId;
        this.percent = percent;
        this.answerText = answer;
        this.questionText = question;
    }

    public get QAnswerId() { return this.qAnswerId; }
    public set QAnswerId(value: number) { this.qAnswerId = value; }

    public get Percent() { return this.percent; }
    public set Percent(value:number) { this.percent = value; }

    public get QuestionText() { return this.questionText; }
    public set QuestionText(value: string) { this.questionText = value; }

    public get AnswerText() { return this.answerText; }
    public set AnswerText(value: string) { this.answerText = value; }   
}