"use strict";
(function (QuestionType) {
    QuestionType[QuestionType["SingleAnswer"] = 0] = "SingleAnswer";
    QuestionType[QuestionType["MultipleAnswer"] = 1] = "MultipleAnswer";
})(exports.QuestionType || (exports.QuestionType = {}));
var QuestionType = exports.QuestionType;
//# sourceMappingURL=Enums.js.map