"use strict";
var TestGroup = (function () {
    function TestGroup(testId, groupId, expires, testName, groupName) {
        if (testId === void 0) { testId = 0; }
        if (groupId === void 0) { groupId = 0; }
        if (expires === void 0) { expires = null; }
        if (testName === void 0) { testName = null; }
        if (groupName === void 0) { groupName = null; }
        this.testId = testId;
        this.groupId = groupId;
        this.expires = expires;
        this.testName = testName;
        this.groupName = groupName;
    }
    Object.defineProperty(TestGroup.prototype, "TestId", {
        get: function () { return this.testId; },
        set: function (value) { this.testId = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TestGroup.prototype, "GroupId", {
        get: function () { return this.groupId; },
        set: function (value) { this.groupId = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TestGroup.prototype, "Expires", {
        get: function () { return this.expires; },
        set: function (value) { this.expires = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TestGroup.prototype, "TestName", {
        get: function () { return this.testName; },
        set: function (value) { this.testName = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TestGroup.prototype, "GroupName", {
        get: function () { return this.groupName; },
        set: function (value) { this.groupName = value; },
        enumerable: true,
        configurable: true
    });
    return TestGroup;
}());
exports.TestGroup = TestGroup;
//# sourceMappingURL=TestGroup.js.map