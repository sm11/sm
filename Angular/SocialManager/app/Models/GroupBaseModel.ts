export class GroupBaseModel {
    private id: number;
    private name: string;
    private parentId: number;
    
    constructor(id: number = 0, name: string = null, parentId:number = 0) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
    }

    public get Id() { return this.id; }
    public set Id(value:number) { this.id = value; }

    public get Name() { return this.name; }
    public set Name(value:string) { this.name = value; }

    public get ParentId() { return this.parentId; }
    public set ParentId(value: number) { this.parentId = value; }   
}