"use strict";
var Test = (function () {
    function Test(testId, name, interviewerId) {
        if (testId === void 0) { testId = 0; }
        if (name === void 0) { name = null; }
        if (interviewerId === void 0) { interviewerId = 0; }
        this.testId = testId;
        this.name = name;
        this.interviewerId = interviewerId;
    }
    Object.defineProperty(Test.prototype, "TestId", {
        get: function () { return this.testId; },
        set: function (value) { this.testId = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Test.prototype, "Name", {
        get: function () { return this.name; },
        set: function (value) { this.name = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Test.prototype, "InterviewerId", {
        get: function () { return this.interviewerId; },
        set: function (value) { this.interviewerId = value; },
        enumerable: true,
        configurable: true
    });
    return Test;
}());
exports.Test = Test;
//# sourceMappingURL=Test.js.map