"use strict";
var CUDResponseModel = (function () {
    function CUDResponseModel(isSuccess, message) {
        if (isSuccess === void 0) { isSuccess = true; }
        if (message === void 0) { message = null; }
        this.isSuccess = isSuccess;
        this.message = message;
    }
    Object.defineProperty(CUDResponseModel.prototype, "IsSuccess", {
        get: function () { return this.isSuccess; },
        set: function (value) { this.isSuccess = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CUDResponseModel.prototype, "Message", {
        get: function () { return this.message; },
        set: function (value) { this.message = value; },
        enumerable: true,
        configurable: true
    });
    return CUDResponseModel;
}());
exports.CUDResponseModel = CUDResponseModel;
//# sourceMappingURL=CUDResponseModel.js.map