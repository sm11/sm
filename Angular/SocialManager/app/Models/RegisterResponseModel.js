"use strict";
var RegisterResponseModel = (function () {
    function RegisterResponseModel(res, user) {
        this.result = res;
        this.user = user;
    }
    Object.defineProperty(RegisterResponseModel.prototype, "CUDResult", {
        get: function () { return this.result; },
        set: function (res) { this.result = res; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RegisterResponseModel.prototype, "User", {
        get: function () { return this.user; },
        set: function (user) { this.user = user; },
        enumerable: true,
        configurable: true
    });
    return RegisterResponseModel;
}());
exports.RegisterResponseModel = RegisterResponseModel;
//# sourceMappingURL=RegisterResponseModel.js.map