"use strict";
var User = (function () {
    function User(id, 
        //fullName: string = "",
        email, login, password, surname, phone, groupId, role) {
        if (id === void 0) { id = 0; }
        if (email === void 0) { email = ""; }
        if (login === void 0) { login = ""; }
        if (password === void 0) { password = ""; }
        if (surname === void 0) { surname = ""; }
        if (phone === void 0) { phone = ""; }
        if (groupId === void 0) { groupId = 0; }
        if (role === void 0) { role = ""; }
        this.id = id;
        this.surname = surname;
        this.email = email;
        this.login = login;
        this.password = password;
        this.phone = phone;
        this.groupId = groupId;
        this.role = role;
    }
    return User;
}());
exports.User = User;
//# sourceMappingURL=User.js.map