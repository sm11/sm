"use strict";
var GroupBaseModel = (function () {
    function GroupBaseModel(id, name, parentId) {
        if (id === void 0) { id = 0; }
        if (name === void 0) { name = null; }
        if (parentId === void 0) { parentId = 0; }
        this.id = id;
        this.name = name;
        this.parentId = parentId;
    }
    Object.defineProperty(GroupBaseModel.prototype, "Id", {
        get: function () { return this.id; },
        set: function (value) { this.id = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GroupBaseModel.prototype, "Name", {
        get: function () { return this.name; },
        set: function (value) { this.name = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GroupBaseModel.prototype, "ParentId", {
        get: function () { return this.parentId; },
        set: function (value) { this.parentId = value; },
        enumerable: true,
        configurable: true
    });
    return GroupBaseModel;
}());
exports.GroupBaseModel = GroupBaseModel;
//# sourceMappingURL=GroupBaseModel.js.map