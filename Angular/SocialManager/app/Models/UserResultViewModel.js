"use strict";
var UserResultViewModel = (function () {
    function UserResultViewModel(userId, answerId, questionId) {
        if (userId === void 0) { userId = 0; }
        if (answerId === void 0) { answerId = 0; }
        if (questionId === void 0) { questionId = 0; }
        this.userId = userId;
        this.answerId = answerId;
        this.questionId = questionId;
    }
    return UserResultViewModel;
}());
exports.UserResultViewModel = UserResultViewModel;
//# sourceMappingURL=UserResultViewModel.js.map