import { User } from  "./User";

export class Group {
    id: number;
    name: string;
    subgroups: Group[];
    parentGroup: Group;
    users: User[];
    interviewer: User;
    parentId: number;
    creatorId: number;
    
    constructor(id: number = 0, name: string = "", subgroups: Group[] = [null], users: User[] = [ ] , 
    interviewer: User = new User(), parentGroup: Group = null, parentId:number = 0, creatorId:number = 0) {
        this.id = id;
        this.name = name;
        this.subgroups = subgroups;
        this.users = users;
        this.interviewer = interviewer;
        this.parentGroup = parentGroup;
        this.parentId = parentId;
        this.creatorId = creatorId;
    }
}