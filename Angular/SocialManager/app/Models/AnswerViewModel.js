"use strict";
var AnswerViewModel = (function () {
    function AnswerViewModel(answerId, text) {
        if (answerId === void 0) { answerId = 0; }
        if (text === void 0) { text = "textquestion"; }
        this.answerId = answerId;
        this.text = text;
    }
    return AnswerViewModel;
}());
exports.AnswerViewModel = AnswerViewModel;
//# sourceMappingURL=AnswerViewModel.js.map