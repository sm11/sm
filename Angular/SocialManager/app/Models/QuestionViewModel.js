"use strict";
var Enums_1 = require("./Enums");
var QuestionViewModel = (function () {
    function QuestionViewModel(questionId, text, testId, type, answers) {
        if (questionId === void 0) { questionId = 0; }
        if (text === void 0) { text = ""; }
        if (testId === void 0) { testId = 0; }
        if (type === void 0) { type = Enums_1.QuestionType.SingleAnswer; }
        if (answers === void 0) { answers = []; }
        this.questionId = questionId;
        this.text = text;
        this.testId = testId;
        this.type = type;
        this.answers = answers;
    }
    return QuestionViewModel;
}());
exports.QuestionViewModel = QuestionViewModel;
//# sourceMappingURL=QuestionViewModel.js.map