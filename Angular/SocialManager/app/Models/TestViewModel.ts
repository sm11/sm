import { QuestionViewModel } from "./QuestionViewModel"

export class TestViewModel {
    testId: number;
    name: string;
    interviewerId: number;    
    questions: QuestionViewModel[];

    constructor (testId: number = 0, name: string = "", interviewerId: number = 0, questions: QuestionViewModel[] = []) {
        this.testId = testId;
        this.name = name;
        this.interviewerId = interviewerId;
        this.questions = questions;
    }
}