import { Test } from "./Test";

export class RespondentTest extends Test {
    private isPassed : boolean;
    private feedbackMesage :string;

    public constructor(testId: number = 0, name:string = null, interviewerId: number = 0, isPassed = false, feedbackMesage = null) {
        super(testId, name, interviewerId);
        this.isPassed = isPassed;
        this.feedbackMesage = feedbackMesage;
    }

    public get IsPassed() { return this.isPassed; }
    public set IsPassed(value: boolean) { this.isPassed = value; }

    public get FeedbackMessage() { return this.feedbackMesage; }
    public set FeedbackMessage(value: string) { this.feedbackMesage = value; }
     
}