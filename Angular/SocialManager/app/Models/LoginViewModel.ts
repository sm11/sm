import { User } from "./User";

export class LoginViewModel {
    private token: string;
    private user: User;

    public constructor(token:string, user: User) {
        this.token = token;
        this.user = user;
    }

    public get Token() { return this.token; }
    public set Token(value:string) { this.token = value; }

    public get User() { return this.user; }
    public set User(value: User) { this.user = value; }
    
}