export class Feedback {
    private testId: number;
    private groupId: number;
    private text: string;

    constructor(testId:number = 0, groupId: number = 0, text:string = null) {
        this.testId = testId;
        this.groupId = groupId;
        this.text = text;
    }

    public get TestId() { return this.testId; }
    public set TestId(value:number) { this.testId = value; }

    public get GroupId() { return this.groupId; }
    public set GroupId( value:number ) { this.groupId = value; }

    public get Text() { return this.text; }
    public set Text( value: string ) { this.text = value; }
}