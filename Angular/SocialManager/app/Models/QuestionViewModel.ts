import { AnswerViewModel } from "./AnswerViewModel"
import { QuestionType } from "./Enums"

export class QuestionViewModel {
    questionId: number;
    text: string;
    testId: number;
    type: QuestionType;
    answers: AnswerViewModel[]; 
    
    constructor (questionId: number = 0, text: string = "", testId: number = 0, 
    type: QuestionType = QuestionType.SingleAnswer, answers: AnswerViewModel[] = []) {
        this.questionId = questionId;
        this.text = text;
        this.testId = testId;
        this.type  = type;
        this.answers = answers;
    }
}