export class User {
    id: number;
    //fullName: string;
    email: string;
    login: string;
    password: string;
    surname: string;
    phone: string;
    groupId: number;
    role : string;

    constructor(
        id: number = 0,
        //fullName: string = "",
        email: string = "",
        login:string = "",
        password: string = "", 
        surname: string = "",
        phone: string = "",
        groupId: number = 0,
        role: string = "") {
        this.id = id;
        this.surname = surname;
        this.email = email;
        this.login = login;
        this.password = password;
        this.phone = phone;
        this.groupId = groupId;
        this.role = role;
    }
}