import { CUDResponseModel } from "./CUDResponseModel";
import { User } from "./User";

export class RegisterResponseModel {
    private result: CUDResponseModel;
    private user : User;

    public constructor(res: CUDResponseModel, user: User) {
        this.result = res;
        this.user = user;
    }

    public get CUDResult() { return this.result; }
    public set CUDResult(res: CUDResponseModel) { this.result = res; }

    public get User() { return this.user; }
    public set User(user:User) { this.user = user; }

}