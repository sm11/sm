export class TestGroup {

    public constructor(private testId:number = 0, private  groupId:number = 0, private expires:Date = null, 
                        private testName:string = null, private groupName:string = null ) { }

    public get TestId() { return this.testId; }
    public set TestId(value: number) { this.testId = value; }

    public get GroupId() { return this.groupId; }
    public set GroupId(value:number) { this.groupId = value; }

    public get Expires() { return this.expires; }
    public set Expires(value: Date) { this.expires = value; }

    public get TestName() { return this.testName; }
    public set TestName(value: string) { this.testName = value; }

    public get GroupName() { return this.groupName; }
    public set GroupName(value:string) { this.groupName = value; }
}