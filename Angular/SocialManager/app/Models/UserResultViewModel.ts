import { QuestionViewModel } from "./QuestionViewModel"

export class UserResultViewModel {
    userId: number;
    answerId: number;
    questionId: number;    

    constructor (userId: number = 0, answerId: number = 0, questionId: number = 0) {
        this.userId = userId;
        this.answerId = answerId;
        this.questionId = questionId;
    }
}