export class CUDResponseModel {
    private isSuccess: boolean;
    private message: string;

    public constructor(isSuccess: boolean = true, message:string = null ) {
        this.isSuccess = isSuccess;
        this.message = message;
    }

    public get IsSuccess() { return this.isSuccess; }
    public set IsSuccess(value: boolean) { this.isSuccess = value; }

    public get Message() { return this.message; }
    public set Message(value: string) { this.message = value; }
}