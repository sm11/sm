export class Test {
    private testId: number;
    private name: string;
    private interviewerId: number;

    public constructor(testId: number = 0, name:string = null, interviewerId: number = 0) {
        this.testId = testId;
        this.name = name;
        this.interviewerId = interviewerId;
    }

    public get TestId() { return this.testId; }
    public set TestId(value:number) { this.testId = value; }

    public get Name() { return this.name; }
    public set Name(value:string) { this.name = value; }

    public get InterviewerId() { return this.interviewerId; }
    public set InterviewerId(value:number) { this.interviewerId = value; }
}