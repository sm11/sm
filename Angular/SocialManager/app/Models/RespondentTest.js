"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Test_1 = require("./Test");
var RespondentTest = (function (_super) {
    __extends(RespondentTest, _super);
    function RespondentTest(testId, name, interviewerId, isPassed, feedbackMesage) {
        if (testId === void 0) { testId = 0; }
        if (name === void 0) { name = null; }
        if (interviewerId === void 0) { interviewerId = 0; }
        if (isPassed === void 0) { isPassed = false; }
        if (feedbackMesage === void 0) { feedbackMesage = null; }
        _super.call(this, testId, name, interviewerId);
        this.isPassed = isPassed;
        this.feedbackMesage = feedbackMesage;
    }
    Object.defineProperty(RespondentTest.prototype, "IsPassed", {
        get: function () { return this.isPassed; },
        set: function (value) { this.isPassed = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RespondentTest.prototype, "FeedbackMessage", {
        get: function () { return this.feedbackMesage; },
        set: function (value) { this.feedbackMesage = value; },
        enumerable: true,
        configurable: true
    });
    return RespondentTest;
}(Test_1.Test));
exports.RespondentTest = RespondentTest;
//# sourceMappingURL=RespondentTest.js.map