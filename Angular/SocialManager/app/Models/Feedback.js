"use strict";
var Feedback = (function () {
    function Feedback(testId, groupId, text) {
        if (testId === void 0) { testId = 0; }
        if (groupId === void 0) { groupId = 0; }
        if (text === void 0) { text = null; }
        this.testId = testId;
        this.groupId = groupId;
        this.text = text;
    }
    Object.defineProperty(Feedback.prototype, "TestId", {
        get: function () { return this.testId; },
        set: function (value) { this.testId = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Feedback.prototype, "GroupId", {
        get: function () { return this.groupId; },
        set: function (value) { this.groupId = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Feedback.prototype, "Text", {
        get: function () { return this.text; },
        set: function (value) { this.text = value; },
        enumerable: true,
        configurable: true
    });
    return Feedback;
}());
exports.Feedback = Feedback;
//# sourceMappingURL=Feedback.js.map