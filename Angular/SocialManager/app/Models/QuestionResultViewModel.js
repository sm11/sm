"use strict";
var QuestionResultViewModel = (function () {
    function QuestionResultViewModel(qAnswerId, percent, question, answer) {
        if (qAnswerId === void 0) { qAnswerId = 0; }
        if (percent === void 0) { percent = 0; }
        if (question === void 0) { question = null; }
        if (answer === void 0) { answer = null; }
        this.qAnswerId = 0;
        this.percent = 0;
        this.questionText = null;
        this.answerText = null;
        this.qAnswerId = qAnswerId;
        this.percent = percent;
        this.answerText = answer;
        this.questionText = question;
    }
    Object.defineProperty(QuestionResultViewModel.prototype, "QAnswerId", {
        get: function () { return this.qAnswerId; },
        set: function (value) { this.qAnswerId = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(QuestionResultViewModel.prototype, "Percent", {
        get: function () { return this.percent; },
        set: function (value) { this.percent = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(QuestionResultViewModel.prototype, "QuestionText", {
        get: function () { return this.questionText; },
        set: function (value) { this.questionText = value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(QuestionResultViewModel.prototype, "AnswerText", {
        get: function () { return this.answerText; },
        set: function (value) { this.answerText = value; },
        enumerable: true,
        configurable: true
    });
    return QuestionResultViewModel;
}());
exports.QuestionResultViewModel = QuestionResultViewModel;
//# sourceMappingURL=QuestionResultViewModel.js.map