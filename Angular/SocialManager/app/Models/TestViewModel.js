"use strict";
var TestViewModel = (function () {
    function TestViewModel(testId, name, interviewerId, questions) {
        if (testId === void 0) { testId = 0; }
        if (name === void 0) { name = ""; }
        if (interviewerId === void 0) { interviewerId = 0; }
        if (questions === void 0) { questions = []; }
        this.testId = testId;
        this.name = name;
        this.interviewerId = interviewerId;
        this.questions = questions;
    }
    return TestViewModel;
}());
exports.TestViewModel = TestViewModel;
//# sourceMappingURL=TestViewModel.js.map