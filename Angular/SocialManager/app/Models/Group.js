"use strict";
var User_1 = require("./User");
var Group = (function () {
    function Group(id, name, subgroups, users, interviewer, parentGroup, parentId, creatorId) {
        if (id === void 0) { id = 0; }
        if (name === void 0) { name = ""; }
        if (subgroups === void 0) { subgroups = [null]; }
        if (users === void 0) { users = []; }
        if (interviewer === void 0) { interviewer = new User_1.User(); }
        if (parentGroup === void 0) { parentGroup = null; }
        if (parentId === void 0) { parentId = 0; }
        if (creatorId === void 0) { creatorId = 0; }
        this.id = id;
        this.name = name;
        this.subgroups = subgroups;
        this.users = users;
        this.interviewer = interviewer;
        this.parentGroup = parentGroup;
        this.parentId = parentId;
        this.creatorId = creatorId;
    }
    return Group;
}());
exports.Group = Group;
//# sourceMappingURL=Group.js.map