Synopsis

Social Manager is the system that allows you to conduct surveys and testing of user groups.
The main purpose is to monitor the psychological climate in the team.
For the full functioning of the software product is developed with the possibility of
processing a large number of requests from interviewers and users of the product, 
as well as its reliability, openness for further improvements, security, 
and correctly elaborated further support its regime.
The interviewer’s role refers to the psychologist or other person who wants to improve colleagues’
relationships or, for example, check manager’s work. 
The respondent of a system is adult person, who works with the team or studies at the university.  
The creator is person that creates the group hierarchy and is responsible for group members.

---------------------------------
---------------------------------
package.json content
          
angular(compiler/core/forms/platform-brovser/upgrade) - 2.1.1,
angular router - 3.1.1,
angular-in-memory-web-api - 0.1.13, 
core-js - 2.4.1,
reflect-metadata - 0.1.8,    
rxjs - 5.0.0-beta.12, 
systemjs - 0.19.39,   
zone.js - 0.6.25,
@types/core-js - 0.9.34,    
@types/node - 6.0.45,      
concurrently - 3.0.0,       
lite-server - 2.2.2, 
typescript - 2.0.3.

---------------------------------

back-end techologies:

Autofac - 4.1.1,
BCrypt - 1.0.0.0.0,
EntityFramework - 6.1.3,
GemBox.Spreadsheet - 41.3.30.1033,
Microsoft AspNet Mvc - 5.2.3,
Microsoft AspNet WebApi Client - 5.2.3,
Newtonsoft Json - 9.0.1.

---------------------------------
---------------------------------

Motivation

There are three main components of psychological climate: relationship between the head and subordinates, the relationship between the colleagues, and how people perceive their work.
Our project is expected to run diagnostics on each of these dimensions.
There are some software products with the similar functions. But we suggest introducing some fundamentally new provisions.
Firstly, we will have a complex system with the automatically results processing, in opposite to existing sets of tests.
Secondly, we provide much more users convenience while completing the tests.

---------------------------------
---------------------------------

Installation

Firstly, you must have specific versions of the software on your computer to deploy the project.
We have not provided for a single method of installation. The easiest option to open the project is to run the sm/SMSolution/SMSolution.sln file in visual studio.
Next, you enter the command "npm start" in the command line, being in the folder sm/Angular/SocialManager.
This action will start the front-end part. The authentication page opens by the default. It is already possible to work with.

---------------------------------
---------------------------------

Tests

Testing was carried out manually. 

---------------------------------
---------------------------------

Contributors

Information about the project is not widespread. You can`t read about it in the blogs or social networks.
However, the topic is not too specific and does not require a deep dive into it.

---------------------------------
---------------------------------

Contacts

-Grinko Alina, grinko-alina1996@ukr.net, - (Scrum Master, Analyst)
-Rakova Alina, rakova1997@gmail.com, - (Designer, Front-end developer)
-Swirchkov Volodimir, swirchkov@gmail.com,- (Developer)
-Topchii Aleksandr, farexyg@gmail.com, - (Developer)
-Lutsenko Oksana,  ksuha_lutsenko@mail.ru, - (Developer)
-Rebenko Dmytro, dima.rebenko@gmail.com,- (Tester)